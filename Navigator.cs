﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;
using Loki.Game.Objects;

namespace GeneralCombatRoutine
{
    public interface INavigator
    {
        
    }

    public class Navigator : INavigator
    {
        private static readonly ILog Log = Logger.GetLoggerInstanceForType();
        private const string Name = "Navigator";

        // TODO: Standardize Vector names around "point" instead of "position"

        /// <summary>
        /// Find the optimal point within line of sight of the given <see cref="targetMonster"/>, and if that's too close
        /// to the monster for safety, find an alternative within a 180* arc and within the given <see cref="maxDistance"/> of the monster,
        /// so that ranged classes can move within line-of-sight while maintaining a comfortable range.
        /// </summary>
        /// <param name="startPoint"></param>
        /// <param name="targetMonster"></param>
        /// <param name="maxDistance"></param>
        /// <returns></returns>
	    public Vector2i GetSafestWalkablePointInRangeAndLineOfSightOfTarget(Vector2i startPoint, Monster targetMonster, int maxDistance)
        {
            // Adjust maximumRanged down a bit in order to prevent movement attempt spam due to being RIGHT ON or near maximum range
            maxDistance = (int)Math.Round(maxDistance * 0.9);

            var endPoint = targetMonster.Position;
            var pathToTarget = ExilePather.GetPointsOnSegment(startPoint, endPoint, true);

            var firstPositionInRangeAndSightOfTarget = pathToTarget
                .FirstOrDefault(v => v.Distance(endPoint) <= maxDistance && ExilePather.CanObjectSee(targetMonster, v, true));

            // If *no* position is within sight and range of the target, we have a problem.
            if (firstPositionInRangeAndSightOfTarget == default(Vector2i)) return endPoint;

            var distanceBetweenPointAndEndPoint = firstPositionInRangeAndSightOfTarget.Distance(endPoint);
            Log.Debug($"[{Name}:Pathfinding] Found position in sight and range of target {firstPositionInRangeAndSightOfTarget} ({distanceBetweenPointAndEndPoint}).");

            // TODO: Make minimum range configurable. For now, default to 1/2 or 1/3 of maximum range.
            var minimumDistanceToTarget = maxDistance / 2;

            var isPointNearAnyMonster = IsPointWithinDistanceOfAnyActiveMonster(firstPositionInRangeAndSightOfTarget, minimumDistanceToTarget);
            if (isPointNearAnyMonster)
            {
                Log.Debug($"[{Name}:Pathfinding] First acceptable point is within minimum distance {minimumDistanceToTarget} of a monster.");

                // 1) Normalize vector to 0,0
                // Well... I know how to normalize a line segment to 0,0... but how the hell does that apply to vectors
                // Looks like these Vector2i's store a point, but no direction. I can effectively treat them like points.

                var adjustedEndPoint = new Vector2i(endPoint.X - firstPositionInRangeAndSightOfTarget.X, endPoint.Y - firstPositionInRangeAndSightOfTarget.Y);
                var headingAngle = Math.Atan2(adjustedEndPoint.Y, adjustedEndPoint.X);
                // TODO: This can probably be significantly optimized

                // A heading angle of 0 would yield a maximum angle of 90 and a minimum angle of 270.
                // A heading angle of 90 would yield a maximum angle of 180 and a minimum angle of 0.
                // A heading angle of 180 would yield a maximum angle of 270 and a minimum angle of 90.
                // A heading angle of 270 would yield a maximum angle of 360 and a minimum angle of 180.
                var minimumAngle = headingAngle >= 90 ? headingAngle - 90 : (headingAngle + 360) - 90;

                var bestPosition = firstPositionInRangeAndSightOfTarget;
                var distanceFromBestPositionToEndPosition = firstPositionInRangeAndSightOfTarget.Distance(endPoint);
                var monsterCountNearBestPosition = MonsterCountWithinDistanceOfPosition(firstPositionInRangeAndSightOfTarget, minimumDistanceToTarget);

                // I can potentially just use the "minimum angle" as the starting point for my stepping and
                // rollover from there, handling the 360->0 translation
                const int degreesPerStep = 8;
                for (var index = 0; index < 20; index++)
                {
                    var currentHeadingDegrees = (float)(index == 0 ? minimumAngle : minimumAngle + (index * degreesPerStep));
                    if (currentHeadingDegrees > 360) currentHeadingDegrees -= 360;

                    var currentHeadingRadians = MathEx.ToRadians(currentHeadingDegrees);
                    //Log.Debug($"[{Name}:Pathfinding] Current rotation is {currentHeadingDegrees}* ({currentHeadingRadians}rad)");
                    // LERP From the projected maximum point on line segment down to the minimum, stepping by 2 units
                    for (var projectedDistanceToTarget = maxDistance; projectedDistanceToTarget >= minimumDistanceToTarget; projectedDistanceToTarget -= 2)
                    {
                        // What a chain of kludges
                        // Extracted from MathEx.GetPointAt in order to cut down on unnecessary work
                        var x = (int)(Math.Cos(currentHeadingRadians) * projectedDistanceToTarget);
                        var y = (int)(Math.Sin(currentHeadingRadians) * projectedDistanceToTarget);
                        var potentialPosition = firstPositionInRangeAndSightOfTarget + new Vector2i(x, y);

                        //Log.Debug($"[{Name}:Pathfinding] Potential position: {potentialPosition} ({firstPositionInRangeAndSightOfTarget} + ({x},{y} [from {currentHeadingRadians} & {projectedDistanceToTarget}]))");

                        // If we can't walk to the point there's really nothing more to do
                        var isPointWalkable = ExilePather.IsWalkable(potentialPosition);
                        if (!isPointWalkable) continue;

                        // Warning: The clamp from float to int may slightly alter distance.
                        // If a raycast fails due to collision, it's no good
                        var collisionPoint = Vector2i.Zero;
                        var isRaycastClear = ExilePather.Raycast(endPoint, potentialPosition, out collisionPoint, true);
                        if (!isRaycastClear)
                        {
                            //Log.Debug($"[{Name}:Pathfinding] Raycasting discovered a collision at position {collisionPoint}.");

                            // Minor optimization: I know how far this collision is from our target. I can forward this loop until it clears the collision.
                            var collisionDistanceToTarget = endPoint.Distance(collisionPoint);
                            if (projectedDistanceToTarget > collisionDistanceToTarget)
                            {
                                //  This will set us EXACTLY AT the position of the collision. We want one step PAST the position of the collision.
                                // Additionally, delta is unnecessary entirely.
                                var adjustedDistanceToTarget = collisionDistanceToTarget - 2;
                                if (adjustedDistanceToTarget % 2 != 0) adjustedDistanceToTarget -= 1; // Make sure we're adjusting by an even amount to match our stepping

                                projectedDistanceToTarget = adjustedDistanceToTarget;
                            }
                            continue;
                        }

                        var distanceFromPotentialPositionToEndPosition = potentialPosition.Distance(endPoint);
                        var monsterCountNearPotentialPosition = MonsterCountWithinDistanceOfPosition(potentialPosition, minimumDistanceToTarget);
                        //Log.Debug($"[{Name}:Pathfinding] Found walkable point {potentialPosition} in vision of target, at distance {distanceFromPotentialPositionToTarget}.");

                        if (monsterCountNearPotentialPosition < monsterCountNearBestPosition ||
                            (monsterCountNearPotentialPosition == monsterCountNearBestPosition && distanceFromPotentialPositionToEndPosition > distanceFromBestPositionToEndPosition))
                        {
                            //Log.Debug($"[{Name}:Pathfinding] Potential position {potentialPosition} ({distanceFromPotentialPositionToEndPosition} distance, {monsterCountNearPotentialPosition} monsters) is a better option than our previous {bestPosition} ({bestPosition.Distance(endPoint)} distance, {monsterCountNearBestPosition} monsters).");
                            bestPosition = potentialPosition;
                            monsterCountNearBestPosition = monsterCountNearPotentialPosition;
                            distanceFromBestPositionToEndPosition = distanceFromPotentialPositionToEndPosition;
                        }
                    }
                }

                if (bestPosition != default(Vector2i))
                {
                    Log.Debug($"[{Name}:Pathfinding] Using best position {bestPosition} ({bestPosition.Distance(endPoint)}).");
                    return bestPosition;
                }
            }

            Log.Debug($"[{Name}:Pathfinding] Could not find better position; falling back on position from existing path.");
            return firstPositionInRangeAndSightOfTarget;
        }

        /// <summary>
        /// Determine if the given point is within the given distance of any active monster.
        /// </summary>
        /// <param name="point"></param>
        /// <param name="minimumDistance"></param>
        /// <returns></returns>
	    private static bool IsPointWithinDistanceOfAnyActiveMonster(Vector2i point, int minimumDistance)
        {
            return LokiPoe.ObjectManager.Objects.OfType<Monster>()
                .Any(m => m.IsActive && !m.IsMonolithed && m.Position.Distance(point) <= minimumDistance);
        }

        /// <summary>
        /// Determine the number of active monsters within the given distance of the given position.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
	    private static int MonsterCountWithinDistanceOfPosition(Vector2i position, int distance)
        {
            return LokiPoe.ObjectManager.Objects.OfType<Monster>()
                .Count(m => m.IsActive && !m.IsMonolithed && m.Position.Distance(position) <= distance);
        }
    }
}
