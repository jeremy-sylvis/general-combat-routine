﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;
using Buddy.Coroutines;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace GeneralCombatRoutine
{
	/// <summary> </summary>
	public class Routine : IRoutine
	{
        private const int OrbOfStormsSkillId = 35848;

	    private const int DisciplineSkillId = 35972;
        private const string DisciplineAuraName = "Discipline Aura";

	    private const string ColdSnapSkillName = "Cold Snap";
	    private const string ContagionSkillName = "Contagion";
	    private const string WitherSkillName = "Wither";
	    private const string SummonSkeletonsSkillName = "Summon Skeletons";
	    private const string SummonRagingSpiritSkillName = "Summon Raging Spirit";
	    private const string RighteousFireSkillName = "Righteous Fire";
	    private const string BloodRageSkillName = "Blood Rage";
	    private const string MoltenShellSkillName = "Molten Shell";
	    private const string EnduringCrySkillName = "Enduring Cry";
	    private const string SummonChaosGolemSkillName = "Summon Chaos Golem";
	    private const string SummonIceGolemSkillName = "Summon Ice Golem";
	    private const string SummonFlameGolemSkillName = "Summon Flame Golem";
	    private const string SummonStoneGolemSkillName = "Summon Stone Golem";
	    private const string SummonLightningGolemSkillName = "Summon Lightning Golem";
	    private const string RaiseZombieSkillName = "Raise Zombie";
	    private const string RaiseSpectreSkillName = "Raise Spectre";
	    private const string FlameblastSkillName = "Flameblast";
	    private const string AnimateGuardianSkillName = "Animate Guardian";
	    private const string AnimateWeaponSkillName = "Animate Weapon";
	    private const string FrostWallSkillName = "Frost Wall";
	    private const string VaalDisciplineSkillName = "Vaal Discipline";

        #region Temp Compatibility 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="distanceFromPoint"></param>
        /// <param name="dontLeaveFrame">Should the current frame not be left?</param>
        /// <returns></returns>
        public static int NumberOfMobsBetween(NetworkObject start, NetworkObject end, int distanceFromPoint = 5,
			bool dontLeaveFrame = false)
		{
			var mobs = LokiPoe.ObjectManager.GetObjectsByType<Monster>().Where(d => d.IsActive).ToList();
			if (!mobs.Any())
				return 0;

			var path = ExilePather.GetPointsOnSegment(start.Position, end.Position, dontLeaveFrame);

			var count = 0;
			for (var i = 0; i < path.Count; i += 10)
			{
				foreach (var mob in mobs)
				{
					if (mob.Position.Distance(path[i]) <= distanceFromPoint)
					{
						++count;
					}
				}
			}

			return count;
		}

		/// <summary>
		/// Checks for a closed door between start and end.
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="distanceFromPoint">How far to check around each point for a door object.</param>
		/// <param name="stride">The distance between points to check in the path.</param>
		/// <param name="dontLeaveFrame">Should the current frame not be left?</param>
		/// <returns>true if there's a closed door and false otherwise.</returns>
		public static bool ClosedDoorBetween(NetworkObject start, NetworkObject end, int distanceFromPoint = 10,
			int stride = 10, bool dontLeaveFrame = false)
		{
			return ClosedDoorBetween(start.Position, end.Position, distanceFromPoint, stride, dontLeaveFrame);
		}

		/// <summary>
		/// Checks for a closed door between start and end.
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="distanceFromPoint">How far to check around each point for a door object.</param>
		/// <param name="stride">The distance between points to check in the path.</param>
		/// <param name="dontLeaveFrame">Should the current frame not be left?</param>
		/// <returns>true if there's a closed door and false otherwise.</returns>
		public static bool ClosedDoorBetween(NetworkObject start, Vector2i end, int distanceFromPoint = 10,
			int stride = 10, bool dontLeaveFrame = false)
		{
			return ClosedDoorBetween(start.Position, end, distanceFromPoint, stride, dontLeaveFrame);
		}

		/// <summary>
		/// Checks for a closed door between start and end.
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="distanceFromPoint">How far to check around each point for a door object.</param>
		/// <param name="stride">The distance between points to check in the path.</param>
		/// <param name="dontLeaveFrame">Should the current frame not be left?</param>
		/// <returns>true if there's a closed door and false otherwise.</returns>
		public static bool ClosedDoorBetween(Vector2i start, NetworkObject end, int distanceFromPoint = 10,
			int stride = 10, bool dontLeaveFrame = false)
		{
			return ClosedDoorBetween(start, end.Position, distanceFromPoint, stride, dontLeaveFrame);
		}

		/// <summary>
		/// Checks for a closed door between start and end.
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <param name="distanceFromPoint">How far to check around each point for a door object.</param>
		/// <param name="stride">The distance between points to check in the path.</param>
		/// <param name="dontLeaveFrame">Should the current frame not be left?</param>
		/// <returns>true if there's a closed door and false otherwise.</returns>
		public static bool ClosedDoorBetween(Vector2i start, Vector2i end, int distanceFromPoint = 10, int stride = 10,
			bool dontLeaveFrame = false)
		{
			var doors = LokiPoe.ObjectManager.Doors.Where(d => !d.IsOpened).ToList();

			if (!doors.Any())
				return false;

			var path = ExilePather.GetPointsOnSegment(start, end, dontLeaveFrame);

			for (var i = 0; i < path.Count; i += stride)
			{
				foreach (var door in doors)
				{
					if (door.Position.Distance(path[i]) <= distanceFromPoint)
					{
						return true;
					}
				}
			}

			return false;
		}

		/// <summary>
		/// Returns the number of mobs near a target.
		/// </summary>
		/// <param name="target"></param>
		/// <param name="distance"></param>
		/// <param name="dead"></param>
		/// <returns></returns>
		public static int NumberOfMobsNear(NetworkObject target, float distance, bool dead = false)
		{
			var mpos = target.Position;

			var curCount = 0;

			foreach (var mob in LokiPoe.ObjectManager.Objects.OfType<Monster>())
			{
				if (mob.Id == target.Id)
				{
					continue;
				}

				// If we're only checking for dead mobs... then... yeah...
				if (dead)
				{
					if (!mob.IsDead)
					{
						continue;
					}
				}
				else if (!mob.IsActive)
				{
					continue;
				}

				if (mob.Position.Distance(mpos) < distance)
				{
					curCount++;
				}
			}

			return curCount;
		}

		#endregion

		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private int _totalCursesAllowed;

		// Auto-set, you do not have to change these.
		private int _raiseZombieSlot = -1;
		private int _raiseSpectreSlot = -1;
		private int _animateWeaponSlot = -1;
		private int _animateGuardianSlot = -1;
		private int _flameblastSlot = -1;
		private int _enduringCrySlot = -1;
		private int _moltenShellSlot = -1;
		private int _bloodRageSlot = -1;
		private int _righteousFurySlot = -1;
		private readonly List<int> _detectedCurseSlotNumbers = new List<int>();
		private int _totemSlot = -1;
		private int _trapSlot = -1;
		private int _mineSlot = -1;
		private int _summonSkeletonsSlot = -1;
		private int _summonRagingSpiritSlot = -1;
		private int _coldSnapSlot = -1;
		private int _contagionSlot = -1;
		private int _witherSlot = -1;
	    private int _frostWallSlot = -1;
	    private int _orbOfStormsSkillSlot = -1;
	    private int _vaalDisciplineSkillSlot = -1;

	    private readonly IList<int> _detectedAuraSkillIds = new List<int>();
	    private readonly IList<int> _detectedGolemSkillIds = new List<int>();

        private int _skillIdOriginallyInSlotFour = -1;

		private bool _isContinuouslyCasting;
		private int _slotOfSkillBeingContinuouslyCast;

		private int _currentLeashRange = -1;

		private readonly Stopwatch _trapStopwatch = Stopwatch.StartNew();
		private readonly Stopwatch _totemStopwatch = Stopwatch.StartNew();
		private readonly Stopwatch _mineStopwatch = Stopwatch.StartNew();
		private readonly Stopwatch _animateWeaponStopwatch = Stopwatch.StartNew();
		private readonly Stopwatch _animateGuardianStopwatch = Stopwatch.StartNew();
		private readonly Stopwatch _moltenShellStopwatch = Stopwatch.StartNew();
		private readonly List<int> _animatedItemBlacklist = new List<int>();
		private readonly Stopwatch _vaalStopwatch = Stopwatch.StartNew();

        private int _summonSkeletonCount;
		private readonly Stopwatch _summonSkeletonsStopwatch = Stopwatch.StartNew();

		private readonly Stopwatch _summonGolemStopwatch = Stopwatch.StartNew();

		private int _summonRagingSpiritCount;
		private readonly Stopwatch _summonRagingSpiritStopwatch = Stopwatch.StartNew();

	    private const int MillisecondsBetweenCurseChecks = 5000;
	    private DateTime _timeOfNextCurseCheck = DateTime.Now;

		private bool _isCastingFlameBlast;
		private int _lastFlameblastCharges;
		private bool _shouldRefreshSkillInformation;

	    private IEnumerable<int> _blacklistedSkillIds;

	    private Dictionary<string, Func<Tuple<object, string>[], object>> _exposedSettings;

		// http://stackoverflow.com/a/824854
		private void RegisterExposedSettings()
		{
			if (_exposedSettings != null)
				return;

			_exposedSettings = new Dictionary<string, Func<Tuple<object,string>[], object>>();

			// Not a part of settings, so do it manually
			_exposedSettings.Add("SetLeash", param =>
			{
				_currentLeashRange = (int) param[0].Item1;
				return MessageResult.Processed;
			});

			_exposedSettings.Add("GetLeash", param => _currentLeashRange != -1 ? _currentLeashRange : RoutineSettings.Instance.CombatRange);

			// Automatically handle all settings

			var properties = typeof (RoutineSettings).GetProperties(BindingFlags.Public | BindingFlags.Instance);
			foreach (var propertyInfo in properties)
			{
				// Only work with ints
				if (propertyInfo.PropertyType != typeof (int) && propertyInfo.PropertyType != typeof (bool))
				{
					continue;
				}

				// If not writable then cannot null it; if not readable then cannot check it's value
				if (!propertyInfo.CanWrite || !propertyInfo.CanRead)
				{
					continue;
				}

				var propertyGetMethod = propertyInfo.GetGetMethod(false);
				var propertySetMethod = propertyInfo.GetSetMethod(false);

				// Get and set methods have to be public
				if (propertyGetMethod == null)
				{
					continue;
				}
				if (propertySetMethod == null)
				{
					continue;
				}

				Log.Info($"Name: {propertyInfo.Name} ({propertyInfo.PropertyType})");

				_exposedSettings.Add("Set" + propertyInfo.Name, param =>
				{
					propertyInfo.SetValue(RoutineSettings.Instance, param[0]);
					return null;
				});

				_exposedSettings.Add("Get" + propertyInfo.Name, param => propertyInfo.GetValue(RoutineSettings.Instance));
			}
		}

        // I'm not yet sure what the appropriate replacement for Targeting is
        // TODO: Migrate this to a more appropriate object
#pragma warning disable CS0612 // Type or member is obsolete
        public Targeting CombatTargeting { get; } = new Targeting();
#pragma warning restore CS0612 // Type or member is obsolete

        #region Targeting

        private readonly IEnumerable<string> DangerousRangedNpcNames = new List<string>
	    {
            "Arcane Construct",
	        "Tentacle Miscreation",
            "Glade Mamba",
            "Ancient Construct",
            "Serpentine Construct",
            "Spine Serpent",
            "Nightwane" // This guy is just a complete asshole
	    };

		private void CombatTargetingOnWeightCalculation(NetworkObject entity, ref float weight)
		{
            // Only deprioritize by distance if they're sufficiently far away.
            // in fact, we generally want things close-up to die right the fuck now.

            // We also want ranged attackers to die most fucking hurriedly.
            if (entity.Distance >= 30)
			    weight -= entity.Distance/5;

			var m = entity as Monster;
			if (m == null)
				return;

		    if (DangerousRangedNpcNames.Any(n => m.Name == n))
		        weight += 10;

			// If the monster is the source of Allies Cannot Die, we really want to kill it fast.
			if (m.HasAura("monster_aura_cannot_die"))
				weight += 30;

			if (m.IsTargetingMe)
			{
				weight += 10;
			}

            // All "Far Shot" monsters must die
            if (m.ExplicitAffixes.Any(ma => ma.Category == "FarShot"))
		    {
		        weight += 10;
		    }

			if (m.Rarity == Rarity.Magic)
			{
				weight += 5;
			}
			else if (m.Rarity == Rarity.Rare)
			{
				weight += 10;
			}
			else if (m.Rarity == Rarity.Unique)
			{
				weight += 15;
			}

			// Minions that get in the way.
			switch (m.Name)
			{
				case "Summoned Skeleton":
					weight -= 15;
					break;

				case "Raised Zombie":
					weight -= 15;
					break;
			}

			if (m.Rarity == Rarity.Normal && m.Type.Contains("/Totems/"))
			{
				weight -= 15;
			}

			// Necros
			if (m.ExplicitAffixes.Any(a => a.InternalName.Contains("RaisesUndead")) ||
				m.ImplicitAffixes.Any(a => a.InternalName.Contains("RaisesUndead")))
			{
				weight += 30;
			}

			// Ignore these mostly, as they just respawn.
			if (m.Type.Contains("TaniwhaTail"))
			{
				weight -= 30;
			}

			// Ignore mobs that expire and die
			if (m.Components.DiesAfterTimeComponent != null)
			{
				weight -= 15;
			}
		}

		private readonly string[] _aurasToIgnore = new[]
		{
			"shrine_godmode", // Ignore any mob near Divine Shrine
			"bloodlines_invulnerable", // Ignore Phylacteral Link
			"god_mode", // Ignore Animated Guardian
			"bloodlines_necrovigil",
		};

		private bool CombatTargetingOnInclusionCalcuation(NetworkObject entity)
		{
			try
			{
				var m = entity as Monster;
				if (m == null)
					return false;

				if (Blacklist.Contains(m))
					return false;

                // Ignore Monolith'd monsters
			    if (m.IsMonolithed)
			        return false;

				// Do not consider inactive/dead mobs.
				if (!m.IsActive)
					return false;

				// Ignore any mob that cannot die.
				if (m.CannotDie)
					return false;

				// Ignore mobs that are too far to care about.
				if (m.Distance > (_currentLeashRange != -1 ? _currentLeashRange : RoutineSettings.Instance.CombatRange))
					return false;

				// Ignore mobs with special aura/buffs
				if (m.HasAura(_aurasToIgnore))
					return false;

				// Ignore Voidspawn of Abaxoth: thanks ExVault!
				if (m.ExplicitAffixes.Any(a => a.DisplayName == "Voidspawn of Abaxoth"))
					return false;

				// Ignore these mobs when trying to transition in the dom fight.
				// Flag1 has been seen at 5 or 6 at times, so need to work out something more reliable.
				if (m.Name == "Miscreation")
				{
					var dom = LokiPoe.ObjectManager.GetObjectByName<Monster>("Dominus, High Templar");
					if (dom != null && !dom.IsDead &&
						(dom.Components.TransitionableComponent.Flag1 == 6 || dom.Components.TransitionableComponent.Flag1 == 5))
					{
						Blacklist.Add(m.Id, TimeSpan.FromHours(1), "Miscreation");
						return false;
					}
				}

				// Ignore Piety's portals.
				if (m.Name == "Chilling Portal" || m.Name == "Burning Portal")
				{
					Blacklist.Add(m.Id, TimeSpan.FromHours(1), "Piety portal");
					return false;
				}
			}
			catch (Exception ex)
			{
				Log.Error("[CombatOnInclusionCalcuation]", ex);
				return false;
			}
			return true;
		}

		#endregion

		#region Implementation of IBase

		/// <summary>Initializes this routine.</summary>
		public void Initialize()
		{
			Log.Debug($"[{Name}] Initialize");

            CombatTargeting.InclusionCalcuation += CombatTargetingOnInclusionCalcuation;
			CombatTargeting.WeightCalculation += CombatTargetingOnWeightCalculation;

			RegisterExposedSettings();
		}

		/// <summary> </summary>
		public void Deinitialize()
		{
            Log.Debug($"[{Name}] Deinitialize");
		}

		#endregion

		#region Implementation of IAuthored

		/// <summary>The name of the routine.</summary>
		public string Name => "General Combat Routine";

	    /// <summary>The description of the routine.</summary>
		public string Description => "A generalized combat routine effectively handle most standard combat scenarios and to serve as a starting point and example for customization.";

	    /// <summary>
		/// The author of this object.
		/// </summary>
		public string Author => "stealthy; based on the work of Bossland GmbH";

	    /// <summary>
		/// The version of this routone.
		/// </summary>
		public string Version => "0.0.1.6";

	    #endregion

		#region Implementation of IRunnable

		/// <summary> The routine start callback. Do any initialization here. </summary>
		public void Start()
		{
			Log.Debug($"[{Name}] Start");

            _shouldRefreshSkillInformation = true;

            // Stop getting ourselves fucking murdered trying to avoid retard fucking chests
            var getTaskManagerMessage = new Message("GetTaskManager", this);
            var messageResult = BotManager.CurrentBot.Message(getTaskManagerMessage);
            var taskManager = getTaskManagerMessage.GetOutput<TaskManager>();
		    taskManager.Remove("HandleBlockingChestsTask");

			if (RoutineSettings.Instance.SingleTargetMeleeSlot == -1 &&
				RoutineSettings.Instance.SingleTargetRangedSlot == -1 &&
				RoutineSettings.Instance.AoeMeleeSlot == -1 &&
				RoutineSettings.Instance.AoeRangedSlot == -1 &&
				RoutineSettings.Instance.FallbackSlot == -1
				)
			{
				Log.Error($"[{Name}] No skills were configured. Please configure the routine's settings (Settings -> Routine) before starting!");
                BotManager.Stop();
			}
		}

		/// <summary> 
		/// Performs any read-only information retrieval & preparation; state-altering operations *should not happen here*.
		/// This shouldn't do *anything* coroutine-related.
		/// </summary>
		public void Tick()
		{
            // If I'm not fully in-game, there's nothing to update.
			if (!LokiPoe.IsInGame) return;

            // Only do the work-intensive skill information refresh if necessary
		    if (!_shouldRefreshSkillInformation) return;

            _shouldRefreshSkillInformation = false;

            // Refresh the skill blacklist
            _blacklistedSkillIds = GetBlacklistedSkillIds();

            // Clear existing tracked skill collections
		    ClearKnownSkills();

		    using (LokiPoe.AcquireFrame())
		    {
                DiscoverSkills();
            }
		}

        /// <summary>
        /// Discover what skills we have available for use.
        /// </summary>
	    private void DiscoverSkills()
	    {
	        var skills = LokiPoe.InGameState.SkillBarHud.Skills
	            .Where(s => _blacklistedSkillIds.All(i => s.Id != i));

	        // Track the skill originally in slot #4 so we can restore it after temporarily using that slot.
	        var temporarySlotOriginalSkill = LokiPoe.InGameState.SkillBarHud.Slot(4);
	        if (temporarySlotOriginalSkill != null && temporarySlotOriginalSkill.IsValid)
	        {
	            _skillIdOriginallyInSlotFour = temporarySlotOriginalSkill.Id;
	        }

	        foreach (var skill in skills)
	        {
	            var skillName = skill.Name;
	            var skillSlot = skill.Slot;
	            var skillId = skill.Id;
	            var tags = skill.SkillTags;

	            // Register curse skills
	            if (tags.Contains("curse"))
	            {
	                if (skillSlot != -1 && skill.IsCastable && !skill.IsAurifiedCurse)
	                {
	                    _detectedCurseSlotNumbers.Add(skillSlot);
	                }
	            }

	            // Register aura skills
	            if (!_detectedAuraSkillIds.Contains(skill.Id))
	            {
	                if (IsSkillAnAura(skill) ||
	                    (RoutineSettings.Instance.EnableAurasFromItems && IsSkillNameAnAuraName(skillName)))
	                {
	                    // Keep track of the fact we actually have this particular aura skill
	                    _detectedAuraSkillIds.Add(skill.Id);
	                }
	            }

	            // Totem slot has to be a pure totem, and not a trapped or mined totem.
	            if (skill.IsTotem && !skill.IsTrap && !skill.IsMine && _totemSlot == -1)
	            {
	                _totemSlot = skillSlot;
	            }

	            if (skill.IsTrap && _trapSlot == -1)
	            {
	                _trapSlot = skillSlot;
	            }

	            if (skill.IsMine && _mineSlot == -1)
	            {
	                _mineSlot = skillSlot;
	            }

	            if (skillId == OrbOfStormsSkillId)
	                _orbOfStormsSkillSlot = skillSlot;

	            // All skill checking below this line is migrated from existing OldRoutine logic
	            if (!IsCastableHelper(skill))
	            {
	                Log.Debug($"[{Name}:Tick] Skill {skillName} ({skillId}) isn't castable; ignoring it.");
	                continue;
	            }

	            if (skillName == ColdSnapSkillName)
	                _coldSnapSlot = skillSlot;
	            else if (skillName == ContagionSkillName)
	                _contagionSlot = skillSlot;
	            else if (skillName == WitherSkillName)
	                _witherSlot = skillSlot;
	            else if (skillName == SummonSkeletonsSkillName)
	                _summonSkeletonsSlot = skillSlot;
	            else if (skillName == SummonRagingSpiritSkillName)
	                _summonRagingSpiritSlot = skillSlot;
	            else if (skillName == RighteousFireSkillName)
	                _righteousFurySlot = skillSlot;
	            else if (skillName == BloodRageSkillName)
	                _bloodRageSlot = skillSlot;
	            else if (skillName == MoltenShellSkillName)
	                _moltenShellSlot = skillSlot;
	            else if (skillName == EnduringCrySkillName)
	                _enduringCrySlot = skillSlot;
	            else if (skillName == SummonChaosGolemSkillName || skillName == SummonIceGolemSkillName ||
	                     skillName == SummonFlameGolemSkillName ||
	                     skillName == SummonStoneGolemSkillName || skillName == SummonLightningGolemSkillName)
	                _detectedGolemSkillIds.Add(skillId);
	            else if (skillName == RaiseZombieSkillName)
	                _raiseZombieSlot = skillSlot;
	            else if (skillName == RaiseSpectreSkillName)
	                _raiseSpectreSlot = skillSlot;
	            else if (skillName == FlameblastSkillName)
	                _flameblastSlot = skillSlot;
	            else if (skillName == AnimateGuardianSkillName)
	                _animateGuardianSlot = skillSlot;
	            else if (skillName == AnimateWeaponSkillName)
	                _animateWeaponSlot = skillSlot;
	            else if (skillName == FrostWallSkillName)
	                _frostWallSlot = skillSlot;
                else if (skillName == VaalDisciplineSkillName)
                    _vaalDisciplineSkillSlot = skillSlot;
	            else
	            {
	                Log.Debug($"[{Name}:Tick] Skill {skill.Name} ({skill.Id}) was unrecognized.");
	            }
	        }
	    }

        /// <summary>
        /// Clear all known information about available skills.
        /// </summary>
	    private void ClearKnownSkills()
	    {
	        _detectedGolemSkillIds.Clear();
	        _detectedAuraSkillIds.Clear();

	        _raiseZombieSlot = -1;
	        _raiseSpectreSlot = -1;
	        _animateWeaponSlot = -1;
	        _animateGuardianSlot = -1;
	        _flameblastSlot = -1;
	        _enduringCrySlot = -1;
	        _moltenShellSlot = -1;
	        _totemSlot = -1;
	        _trapSlot = -1;
	        _coldSnapSlot = -1;
	        _contagionSlot = -1;
	        _witherSlot = -1;
	        _bloodRageSlot = -1;
	        _righteousFurySlot = -1;
	        _summonSkeletonsSlot = -1;
	        _summonRagingSpiritSlot = -1;
	        _summonSkeletonCount = 0;
	        _summonRagingSpiritCount = 0;
	        _mineSlot = -1;
	        _detectedCurseSlotNumbers.Clear();
	        _totalCursesAllowed = LokiPoe.Me.TotalCursesAllowed;
	        _frostWallSlot = -1;
	        _orbOfStormsSkillSlot = -1;
            _vaalDisciplineSkillSlot = -1;
	    }

	    /// <summary> The routine stop callback. Do any pre-dispose cleanup here. </summary>
		public void Stop()
		{
			Log.Debug($"[{Name}] Stop");
		}

		#endregion

		#region Implementation of IConfigurable

		/// <summary> The bot's settings control. This will be added to the Exilebuddy Settings tab.</summary>
		public UserControl Control
		{
			get
			{

				using (
					var fs = new FileStream(Path.Combine(ThirdPartyLoader.GetInstance("GeneralCombatRoutine").ContentPath, "SettingsGui.xaml"),
						FileMode.Open))
				{
					var root = (UserControl) XamlReader.Load(fs);

					// Your settings binding here.

					if (
						!Wpf.SetupCheckBoxBinding(root, "SkipShrinesCheckBox", "SkipShrines",
							BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'SkipShrinesCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupCheckBoxBinding(root, "LeaveFrameCheckBox", "LeaveFrame",
							BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'LeaveFrameCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupCheckBoxBinding(root, "EnableAurasFromItemsCheckBox", "EnableAurasFromItems",
							BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'EnableAurasFromItemsCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupCheckBoxBinding(root, "AlwaysAttackInPlaceCheckBox", "AlwaysAttackInPlace",
							BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'AlwaysAttackInPlaceCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupCheckBoxBinding(root, "DebugAurasCheckBox", "DebugAuras",
							BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'DebugAurasCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupCheckBoxBinding(root, "AutoCastVaalSkillsCheckBox", "AutoCastVaalSkills",
							BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupCheckBoxBinding failed for 'AutoCastVaalSkillsCheckBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxItemsBinding(root, "SingleTargetMeleeSlotComboBox", "AllSkillSlots",
							BindingMode.OneWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxItemsBinding failed for 'SingleTargetMeleeSlotComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxSelectedItemBinding(root, "SingleTargetMeleeSlotComboBox",
							"SingleTargetMeleeSlot", BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxSelectedItemBinding failed for 'SingleTargetMeleeSlotComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxItemsBinding(root, "SingleTargetRangedSlotComboBox", "AllSkillSlots",
							BindingMode.OneWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxItemsBinding failed for 'SingleTargetRangedSlotComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxSelectedItemBinding(root, "SingleTargetRangedSlotComboBox",
							"SingleTargetRangedSlot", BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxSelectedItemBinding failed for 'SingleTargetRangedSlotComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxItemsBinding(root, "AoeMeleeSlotComboBox", "AllSkillSlots",
							BindingMode.OneWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxItemsBinding failed for 'AoeMeleeSlotComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxSelectedItemBinding(root, "AoeMeleeSlotComboBox",
							"AoeMeleeSlot", BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxSelectedItemBinding failed for 'AoeMeleeSlotComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxItemsBinding(root, "AoeRangedSlotComboBox", "AllSkillSlots",
							BindingMode.OneWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxItemsBinding failed for 'AoeRangedSlotComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxSelectedItemBinding(root, "AoeRangedSlotComboBox",
							"AoeRangedSlot", BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxSelectedItemBinding failed for 'AoeRangedSlotComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxItemsBinding(root, "FallbackSlotComboBox", "AllSkillSlots",
							BindingMode.OneWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxItemsBinding failed for 'FallbackSlotComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupComboBoxSelectedItemBinding(root, "FallbackSlotComboBox",
							"FallbackSlot", BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupComboBoxSelectedItemBinding failed for 'FallbackSlotComboBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "CombatRangeTextBox", "CombatRange",
						BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat("[SettingsControl] SetupTextBoxBinding failed for 'CombatRangeTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "MaxMeleeRangeTextBox", "MaxMeleeRange",
						BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat("[SettingsControl] SetupTextBoxBinding failed for 'MaxMeleeRangeTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "MaxRangeRangeTextBox", "MaxRangeRange",
						BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat("[SettingsControl] SetupTextBoxBinding failed for 'MaxRangeRangeTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "MaxFlameBlastChargesTextBox", "MaxFlameBlastCharges",
						BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'MaxFlameBlastChargesTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "MoltenShellDelayMsTextBox", "MoltenShellDelayMs",
						BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat("[SettingsControl] SetupTextBoxBinding failed for 'MoltenShellDelayMsTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "TotemDelayMsTextBox", "TotemDelayMs",
						BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'TotemDelayMsTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "TrapDelayMsTextBox", "TrapDelayMs",
						BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'TrapDelayMsTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupTextBoxBinding(root, "SummonRagingSpiritCountPerDelayTextBox",
							"SummonRagingSpiritCountPerDelay",
							BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'SummonRagingSpiritCountPerDelayTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "SummonRagingSpiritDelayMsTextBox", "SummonRagingSpiritDelayMs",
						BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'SummonRagingSpiritDelayMsTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (
						!Wpf.SetupTextBoxBinding(root, "SummonSkeletonCountPerDelayTextBox",
							"SummonSkeletonCountPerDelay",
							BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'SummonSkeletonCountPerDelayTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "SummonSkeletonDelayMsTextBox", "SummonSkeletonDelayMs",
						BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'SummonSkeletonDelayMsTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "MineDelayMsTextBox", "MineDelayMs",
						BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'MineDelayMsTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					if (!Wpf.SetupTextBoxBinding(root, "BlacklistedSkillIdsTextBox", "BlacklistedSkillIds",
						BindingMode.TwoWay, RoutineSettings.Instance))
					{
						Log.DebugFormat(
							"[SettingsControl] SetupTextBoxBinding failed for 'BlacklistedSkillIdsTextBox'.");
						throw new Exception("The SettingsControl could not be created.");
					}

					// Your settings event handlers here.

					return root;
				}
			}
		}

		/// <summary>The settings object. This will be registered in the current configuration.</summary>
		public JsonSettings Settings => RoutineSettings.Instance;

        #region Configuration Helpers

        /// <summary>
        /// Get the collection of skill IDs present on the configured skill ID blacklist.
        /// </summary>
        /// <returns></returns>
	    private static IEnumerable<int> GetBlacklistedSkillIds()
        {
            var skillIdTokens = RoutineSettings.Instance.BlacklistedSkillIds
                .Split(new[] { ' ', ',', ';', '-' }, StringSplitOptions.RemoveEmptyEntries);
            var skillIdStrings = skillIdTokens.Where(s => s.All(char.IsDigit));

            var skillIds = new HashSet<int>();
            foreach (var skillIdString in skillIdStrings)
            {
                int parseInt;
                if (int.TryParse(skillIdString, out parseInt))
                    skillIds.Add(parseInt);
            }

            return skillIds;
        }

        #endregion

        #endregion

        #region Implementation of ILogic

        private TimeSpan EnduranceChargesTimeLeft
		{
			get
			{
				Aura aura = LokiPoe.Me.EnduranceChargeAura;
				if (aura != null)
				{
					return aura.TimeLeft;
				}

				return TimeSpan.Zero;
			}
		}

        /// <summary>
        /// Implements the ability to handle a logic passed through the system; this executes outside the scope of a framelock.
        /// Framelocks should be used anywhere data may change from frame to frame but should never be used in conjunction with another blocking operation
        /// e.g. awaits / Thread.Sleep.
        /// 
        /// NOTE: Returning "true" indicates the TaskManager needs to restart Task iteration from the start of the list, whereas
        /// returning "false" indicates the TaskManager can continue to the next task. In essence, returning "true" is our way of
        /// indicating "this task is done for now but we've done enough interaction we need to take a break and start from the top";
        /// "false" indicates "this task is done for now and we didn't really do anything, so the next task can interact with the game".
        /// </summary>
        /// <param name="logic">The logic to be processed.</param>
        /// <returns>A LogicResult that describes the result..</returns>
        public async Task<LogicResult> Logic(Logic logic)
		{
            // "combat" is the only task logic for which we might need to restart iteration from the top.
            if (logic.Id == "hook_combat")
            {
                // Update targeting.
                var wasCombatLogicProvided = await ProcessCombatLogic();
                return wasCombatLogicProvided ? LogicResult.Provided : LogicResult.Unprovided;
            }

			return LogicResult.Unprovided;
		}

        /// <summary>
        /// Process the combat-focused usage of skills against potential targets.
        /// 
        /// NOTES:
        /// So far, this is significantly faster to respond than the stock Routine.
        /// There's definitely still room for improvement, but most of that seems to be in terminating other tasks more quickly when a combat situation is detected,
        ///     e.g. so ExileBuddy can stop fixating on a fucking chest and stop to do something about the 50,000 minions that walked up to you while trying to get to that chest.
        /// I confirmed this exact issue this morning - it happened when it fixated on a Portal Scroll too. I'd imagine those tasks aren't too concerned with nearby mobs.
        /// 
        /// Also, for calculating where to place frost walls, a stat entry: WallMaximumLength: 56
        /// 
        /// See if I can figure out where the FPS drop is coming from while ExileBuddy is running. If I had to guess, it's most likely Alcor75PlayerMover.
        /// * LootItemsTask and OpenChestTask also contained a fuckton of inefficiency. The cleanup done there may help but not overly much.
        /// 
        /// Add mobs with the "Far shot" tag to prioritization with higher weight - they fucking hurt and need to die fast.
        /// * I've attempted to bake this into weighting - I'll have to watch for the debug messages to see if it works.
        /// 
        /// Check for the "Resists Cold" tag as a reason to prioritize Frostbite
        /// 
        /// Coroutine "logic" is called as a cross-frame blocking action. Coroutines.Yield is used to stop yield execution to other coroutines for a while instead of continuously blocking.
        /// Given how PlayerMover.MoveToward is called from these, it is *definitely* intended to be a blocking call that only returns when it's "complete"ish.
        /// This actually provides significant complication when altering any existing coroutine logic as it must be done from the perspective of any local method data possibly being out of date.
        /// * I've made some progress on LootItemsTask and OpenChestTask... we'll see how that goes.
        /// * Most recent changes (0.0.2.0) have fixed LootItemsTask, CombatTask, and OpenChestTask driving you face-first into a pack of pain.
        /// * I've replaced one of the Coroutine.Yields with a ghetto frame-skip mechanism. We'll see how it works before doing more of the same.
        /// 
        /// I need to create an abstraction point and base implementation for the common components.
        /// As an example, LootItemsTask and OpenChestTask share 50-75% of their logic and are _really_ just specialized types of Interaction tasks.
        ///     Both depend on the same checks for nearby NPCs, etc.
        /// As another example, Combat routines _really_ consist of some targeting and some other stuff and not *just* the IRoutine crap.
        /// 
        /// These base implementations would provide *great* places to define the common, unused crap, instead of forcing it into every class (e.g. IBase, IAuthored),
        /// as well as some of the common logic e.g. around logging, throttling of pulse speed/delaying next pulse, etc.
        /// 
        /// Notes: As expected, TaskManager is an IRunnable/ITask itself that just does a foreach on all registered tasks and synchronously calls .Tick/.Logic/etc.
        /// Which means, every task needs to execute as atomically as possible lest they block execution of all other tasks. Take note, OldGrindBot.
        /// 
        /// There are many duplicate tasks registered in the task manager used by OldGrindBot. This needs to be rectified. See Notepad++ for debug output.
        /// Results: {{CombatTask (Leash 50), CombatTask (Leash -1)}, {LootItemTask (Leash 50), LootItemTask (Leash -1)}, {OpenChestTask (Leash 50), OpenChestTask (Leash -1)}, {ExplorationCompleteTask (Early), ExplorationCompleteTask}}
        /// This is more complicated than originally expected. It's using two instances of the same task to handle the concerns of:
        /// 1) Looting everything around me (Leash 50)
        /// 2) Utilizing the area cache of my grind zone to nagivate to and loot the closest distant item (Leash -1).
        /// The problems begin when Leash -1 transitions to under 50 units distance and both instances attempt to interact with and loot the item.
        /// This should be condensed into one task. It's fucking dirty as it is.
        /// Yeah. Each of the individual tasks should handle things in order of distance. It's a two-stage job.
        /// 1) If we have rendered items, consult the rendered item set for a potential target.
        /// 2) If tehre were no rendered items or, at least, no target there, consult the area cache for a potential target.
        /// It'll take a little thinking to truly iron out, but that will work well enough.
        /// This means I've got two combat tasks calling this routine... no fucking wonder I was seeing double-curses, etc.
        /// 
        /// I need to consider low golem HP as grounds for resummoning during my pre-combat buffs.
        /// 
        /// I need to make this able to shoot at an angle away form me _toward_ an off-screen monster, without having to put the cursor on it. That way, it can still snipe at 
        ///     the really far, damaging fuckers, instead of standing there like a retard.
        /// 
        /// I really, *really* need to find a way to improve the walk-into-a-monster's-face logic that happens whenever we can't see our closest target.
        /// 
        /// Alcor75PlayerMover's dodge/kite stuff occasionally attempts to kite by walking face-first onto a fucking monster.
        /// Update: It was the combat routine, due to detecting a maximum melee range of 0 for whatever reason... noticed for sure against proximity shield mobs
        /// 
        /// There's a conflict between Explore task and LootItem/OpenChest task; they both try to move to an item at the same time.
        /// Or, rather, LootItem grabs something, Explore moves towards its objective, LootItem moves back and grabs something else, Explore moves away towards its objective, rinse/repeat
        /// 
        /// I can build a Coroutine around a given Action (method), giving it Yield/Resume support. I can do that *here*, most importantly.
        /// </summary>
        /// <returns></returns>
	    private async Task<bool> ProcessCombatLogic()
	    {
            // We'll always want to update Combat targeting when performing combat logic.
	        CombatTargeting.Update();

	        // We now signal always highlight needs to be disabled, but won't actually do it until we cast something.
	        _needsToDisableAlwaysHighlight = LokiPoe.ObjectManager.Objects.OfType<Chest>()
                .Any(c => c.Distance < RoutineSettings.Instance.MaxRangeRange && !c.IsOpened && c.IsStrongBox);

            // Populate any background information
            var isAnyMonsterWithinCombatRange = IsAnyMonsterWithinCombatRange();

            // IF we think we were continuously casting but aren't actually casting anything, unset the flag
            if (_isContinuouslyCasting && (!LokiPoe.Me.HasCurrentAction || LokiPoe.Me.CurrentAction.Skill.Name == "Move"))
            {
                //Log.Debug($"[{Name}:Tick] We were continuously casting but have no non-Move current action.");
                _isContinuouslyCasting = false;
            }

            var myPosition = LokiPoe.MyPosition;

	        // If we have flameblast, we need to use special logic for it.
	        if (_flameblastSlot != -1 && _isCastingFlameBlast)
	        {
                var flameblastResult = await ChannelFlameBlast(myPosition);
	            return flameblastResult;
	        }

	        // If any NPCs are in the "oh shit" zone, we want to avoid excess summons or we're going to get raped up the butt while trying to spawn zombies
            if (!isAnyMonsterWithinCombatRange)
	        {
                // Handle aura logic as a pre-combat step.
                if (_detectedAuraSkillIds.Any())
                {
                    // And now the question of the day - can I interrupt this task if I detect monsters?
                    var activateAurasResult = await ActivateAuras();
                    if (activateAurasResult) return true;
                }

                // Summon Golems as a pre-combat step.
                if (_detectedGolemSkillIds.Any())
                {
                    var summonGolemsResult = await SummonGolems(LokiPoe.MyPosition);
                    if (summonGolemsResult) return true;
                }
	        }

            var monsters = LokiPoe.ObjectManager.Objects.OfType<Monster>();
            var isAnyMonsterExtremelyClose = monsters.Any(m => m.IsActive && m.Distance <= 30);
	        if (!isAnyMonsterExtremelyClose)
	        {
                var combatSummoningResult = await HandleCombatSummoning();
                if (combatSummoningResult) return true;
            }

	        // Since EC has a cooldown, we can just cast it when mobs are in range to keep our endurance charges refreshed.
	        if (_enduringCrySlot != -1 && EnduranceChargesTimeLeft.TotalSeconds < 5 && isAnyMonsterWithinCombatRange)
	        {
	            // See if we can use the skill.
	            var skill = LokiPoe.InGameState.SkillBarHud.Slot(_enduringCrySlot);
	            if (skill.CanUse())
	            {
	                var useSkillResult = await UseSkill(skill);
	                if (useSkillResult) return true;
	            }
	        }

	        // Check for a surround to use flameblast, just example logic.
	        if (_flameblastSlot != -1)
	        {
	            if (NumberOfMobsNear(LokiPoe.Me, 15) >= 4)
	            {
	                _isCastingFlameBlast = true;
	                _lastFlameblastCharges = 0;
	                return true; // Restart task iteration
	            }
	        }

            if (await InteractWithShrines())
                return true; // We need to restart task iteration; this is a significant action

            // TODO: _currentLeashRange of -1 means we need to use a cached location system to prevent back and forth issues of mobs despawning.

            // This is pretty important. Otherwise, components can go invalid and exceptions are thrown.
            // TODO: Does the targeting list get cleared upon death / zoning? It doesn't appear to be...
            var bestTarget = CombatTargeting.Targets<Monster>().FirstOrDefault();

	        // No monsters, we can execute non-critical combat logic, like buffs, auras, etc...
	        // For this example, just going to continue executing bot logic.
	        if (bestTarget == null)
	        {
	            ResetCombatState();
	            return false; // We have no target; let the next task execute.
	        }

	        var targetPosition = bestTarget.Position;
	        var targetInteractionPosition = bestTarget.InteractCenterWorld;
	        var targetId = bestTarget.Id;
	        var targetName = bestTarget.Name;
	        var targetRarity = bestTarget.Rarity;
	        var targetDistance = bestTarget.Distance;
	        var isTargetCurseable = bestTarget.IsCursable;
	        var curseCount = bestTarget.CurseCount;
	        var monsterCountNearTarget = NumberOfMobsNear(bestTarget, 20);
	        var targetHasProximityShield = bestTarget.HasProximityShield;
	        var targetHasContagion = bestTarget.HasContagion;
	        var targetHasWither = bestTarget.HasWither;
	        var monsterCountWithinMeleeRangeOfMe = NumberOfMobsNear(LokiPoe.Me, RoutineSettings.Instance.MaxMeleeRange);
	        var monsterCountWithinTwentyUnitsOfTarget = NumberOfMobsNear(bestTarget, 20);

            var doesMonsterHaveCurseByCurseName = new Dictionary<string, bool>();
            foreach (var curseSlot in _detectedCurseSlotNumbers)
	        {
	            var skill = LokiPoe.InGameState.SkillBarHud.Slot(curseSlot);
	            doesMonsterHaveCurseByCurseName.Add(skill.Name, bestTarget.HasCurseFrom(skill.Name));
	        }

            // This doesn't necessarily account for transparent (shootable) walls. That explains why it keeps getting confused on them.
	        var isTargetVisible = ExilePather.CanObjectSee(LokiPoe.Me, bestTarget);
	        var pathDistanceToTarget = ExilePather.PathDistance(myPosition, targetPosition);
	        var isTargetBlockedByDoor = ClosedDoorBetween(LokiPoe.Me, bestTarget);

	        if (pathDistanceToTarget.CompareTo(float.MaxValue) == 0)
	        {
	            Log.Error($"[{Name}:Logic] Could not determine the path distance to the best target. Now blacklisting it.");
	            Blacklist.Add(targetId, TimeSpan.FromMinutes(1), "Unable to pathfind to.");
	            return true; // We do need to restart iteration if there are more targets... so, restart iteration
	        }

	        // Prevent combat loops from happening by preventing combat outside CombatRange.
	        if (pathDistanceToTarget > RoutineSettings.Instance.CombatRange)
	        {
	            EnableAlwaysHiglight();
	            return false; // We don't need to restart iteration; we can let the next task process.
	        }

	        // If I can't see the monster AND it's blocked, we need to move.
	        // If I'm ranged and I can see it, it's gonna die.
	        if (!isTargetVisible || targetDistance >= RoutineSettings.Instance.MaxRangeRange || isTargetBlockedByDoor)
	        {
	            Log.InfoFormat(
	                "[{4}:Logic] Now moving towards the monster {0} because [canSee: {1}][pathDistance: {2}][blockedByDoor: {3}]",
	                targetName, isTargetVisible, pathDistanceToTarget, isTargetBlockedByDoor, Name);

                var idealTargetPosition = GetWalkablePointInRangeAndLineOfSightOfTarget(LokiPoe.MyPosition, bestTarget, RoutineSettings.Instance.MaxRangeRange);
                var didMoveTowards = PlayerMover.MoveTowards(idealTargetPosition);
	            if (!didMoveTowards)
	                Log.Error($"[{Name}:Logic] MoveTowards failed for {idealTargetPosition}.");

	            return true; // We need to restart task iteration so this one can process again
	        }

	        // Handle totem logic.
	        if (_totemSlot != -1 && _totemStopwatch.ElapsedMilliseconds > RoutineSettings.Instance.TotemDelayMs)
	        {
	            var skill = LokiPoe.InGameState.SkillBarHud.Slot(_totemSlot);
	            if (skill.CanUse() && skill.DeployedObjects.Select(o => o as Monster).Count(t => !t.IsDead && t.Distance < 60) < LokiPoe.Me.MaxTotemCount)
	            {
	                var position = myPosition.GetPointAtDistanceAfterThis(targetPosition, targetDistance/2);
	                var usePositionedSkillResult = await UsePositionedSkill(skill, false, position);
	                if (usePositionedSkillResult)
	                {
	                    _totemStopwatch.Restart();
	                    return true;
	                }
	            }
	        }

            if (_vaalDisciplineSkillSlot >= 0 && LokiPoe.Me.EnergyShieldPercent < 40)
            {
                var skill = LokiPoe.InGameState.SkillBarHud.Slot(_vaalDisciplineSkillSlot);
                var wasSkillUsed = await UseSkill(skill);
                if (wasSkillUsed) return true; // Indicate task iteration should restart
            }

	        if (_frostWallSlot != -1)
	        {
                // Throw up a Frost Wall between us and any dangerous ranged targets
                var skill = LokiPoe.InGameState.SkillBarHud.Slot(_frostWallSlot);
	            if (skill.CanUse() && skill.CooldownsActive == 0)
	            {
	                var sufficientlyFarTargets = CombatTargeting.Targets<Monster>()
	                    .Where(m => m.Position.Distance(myPosition) >= 12);

	                // First attempt to find a super dangeorus target (e.g. one known to throw painful projectiles)
	                var dangerousTarget = sufficientlyFarTargets.FirstOrDefault(m => DangerousRangedNpcNames.Any(n => m.Name == n));

	                // Fall-back on the closest target we can wall off
	                if (dangerousTarget == null)
	                    dangerousTarget = sufficientlyFarTargets.FirstOrDefault();

	                if (dangerousTarget != null)
	                {
	                    // Get the position a few yards from us, in line between us and the target
	                    var frostWallPosition = LokiPoe.MyPosition.GetPointAtDistanceAfterThis(dangerousTarget.Position, dangerousTarget.Distance/2);
                        var meshPosition = Vector2i.Zero;
                        if (ExilePather.PolyPathfinder.ClosestPointOnMesh(frostWallPosition, out meshPosition) && meshPosition != Vector2i.Zero)
                        {
                            var usePositionedSkillResult = await UsePositionedSkill(skill, true, meshPosition);
                            if (usePositionedSkillResult) return true;
                        }
                    }
	            }
	        }

            // For Molten Shell, we want to limit cast time, since mobs that break the shield often would cause the CR to cast it over and over.
            if (_moltenShellSlot != -1 && isAnyMonsterWithinCombatRange && !LokiPoe.Me.HasMoltenShellBuff && 
                _moltenShellStopwatch.ElapsedMilliseconds >= RoutineSettings.Instance.MoltenShellDelayMs)
            {
                var skill = LokiPoe.InGameState.SkillBarHud.Slot(_moltenShellSlot);

                var useSkillResult = await UseSkill(skill);
                if (useSkillResult)
                {
                    _moltenShellStopwatch.Restart();
                    return true;
                }
            }

            // Handle trap logic.
            if (_trapSlot != -1 && _trapStopwatch.ElapsedMilliseconds > RoutineSettings.Instance.TrapDelayMs)
	        {
	            var skill = LokiPoe.InGameState.SkillBarHud.Slot(_trapSlot);
	            if (skill.CanUse())
	            {
	                var position = myPosition.GetPointAtDistanceAfterThis(targetPosition, targetDistance/2);
	                var usePositionedSkillResult = await UsePositionedSkill(skill, false, position);
	                if (usePositionedSkillResult)
	                {
	                    _trapStopwatch.Restart();
	                    return true;
	                }
	            }
	        }

	        // Handle curse logic - curse magic+ and packs of 4+, but only cast within MaxRangeRange.
	        var now = DateTime.Now;
	        if (now >= _timeOfNextCurseCheck)
	        {
	            _timeOfNextCurseCheck = now.AddMilliseconds(MillisecondsBetweenCurseChecks);

	            var shouldCheckCurses = myPosition.Distance(targetPosition) < RoutineSettings.Instance.MaxRangeRange && 
                    (targetRarity >= Rarity.Magic || monsterCountWithinTwentyUnitsOfTarget >= 3);
	            if (shouldCheckCurses)
	            {
	                foreach (var curseSlotNumber in _detectedCurseSlotNumbers)
	                {
	                    var skill = LokiPoe.InGameState.SkillBarHud.Slot(curseSlotNumber);
	                    if (skill.CanUse() && isTargetCurseable && curseCount < _totalCursesAllowed && !doesMonsterHaveCurseByCurseName[skill.Name])
	                    {
	                        var usePositionedSkillResult = await UsePositionedSkill(skill, true, targetPosition);
	                        if (usePositionedSkillResult) return true;
	                    }
	                }
	            }
	        }

	        // Simply cast Blood Rage if we have it.
	        if (_bloodRageSlot != -1)
	        {
	            // See if we can use the skill.
	            var skill = LokiPoe.InGameState.SkillBarHud.Slot(_bloodRageSlot);
	            if (skill.CanUse() && !LokiPoe.Me.HasBloodRageBuff && targetDistance < RoutineSettings.Instance.CombatRange)
	            {
	                var useSkillResult = await UseSkill(skill);
	                if (useSkillResult) return true;
	            }
	        }

	        // Simply cast RF if we have it.
	        if (_righteousFurySlot != -1)
	        {
	            // See if we can use the skill.
	            var skill = LokiPoe.InGameState.SkillBarHud.Slot(_righteousFurySlot);
	            if (skill.CanUse() && !LokiPoe.Me.HasRighteousFireBuff)
	            {
	                var useSkillResult = await UseSkill(skill);
	                if (useSkillResult) return true;
	            }
	        }

	        if (_summonRagingSpiritSlot != -1 && _summonRagingSpiritStopwatch.ElapsedMilliseconds > RoutineSettings.Instance.SummonRagingSpiritDelayMs)
	        {
	            var skill = LokiPoe.InGameState.SkillBarHud.Slot(_summonRagingSpiritSlot);
	            var max = skill.GetStat(StatTypeGGG.NumberOfRagingSpiritsAllowed);
	            if (skill.NumberDeployed < max && skill.CanUse())
	            {
	                ++_summonRagingSpiritCount;

	                await Coroutines.FinishCurrentAction();
	                await Coroutines.LatencyWait();

	                var err1 = LokiPoe.InGameState.SkillBarHud.UseAt(_summonRagingSpiritSlot, false, targetInteractionPosition);

	                if (_summonRagingSpiritCount >= RoutineSettings.Instance.SummonRagingSpiritCountPerDelay)
	                {
	                    _summonRagingSpiritCount = 0;
	                    _summonRagingSpiritStopwatch.Restart();
	                }

	                if (err1 == LokiPoe.InGameState.UseResult.None)
	                {
	                    await Coroutines.FinishCurrentAction(false);
	                    await Coroutines.LatencyWait();

	                    return true;
	                }

	                Log.ErrorFormat("[Logic] Use returned {0} for {1}.", err1, skill.Name);
	            }
	        }

	        if (_summonSkeletonsSlot != -1 && _summonSkeletonsStopwatch.ElapsedMilliseconds > RoutineSettings.Instance.SummonSkeletonDelayMs)
	        {
	            var skill = LokiPoe.InGameState.SkillBarHud.Slot(_summonSkeletonsSlot);
	            var max = skill.GetStat(StatTypeGGG.NumberOfSkeletonsAllowed);
	            if (skill.NumberDeployed < max && skill.CanUse())
	            {
	                ++_summonSkeletonCount;

	                DisableAlwaysHiglight();

                    // TODO: This should be precondition wait
	                await Coroutines.FinishCurrentAction();

	                var useAtResult = LokiPoe.InGameState.SkillBarHud.UseAt(_summonSkeletonsSlot, true,
	                    myPosition.GetPointAtDistanceAfterThis(targetPosition,targetDistance/2));

	                if (_summonSkeletonCount >= RoutineSettings.Instance.SummonSkeletonCountPerDelay)
	                {
	                    _summonSkeletonCount = 0;
	                    _summonSkeletonsStopwatch.Restart();
	                }

	                if (useAtResult != LokiPoe.InGameState.UseResult.None)
	                    Log.Error($"[{Name}] UseAt returned {useAtResult} for {skill.Name}.");

	                // TODO: This should be precondition wait
                    await Coroutines.FinishCurrentAction();
                    return true;
                }
	        }

	        if (_mineSlot != -1 && _mineStopwatch.ElapsedMilliseconds > RoutineSettings.Instance.MineDelayMs &&
	            myPosition.Distance(targetPosition) < RoutineSettings.Instance.MaxMeleeRange)
	        {
	            var skill = LokiPoe.InGameState.SkillBarHud.Slot(_mineSlot);
	            var maxDeployedMines = skill.GetStat(StatTypeGGG.SkillDisplayNumberOfRemoteMinesAllowed);
	            var isInstantMineDetonation = skill.GetStat(StatTypeGGG.MineDetonationIsInstant) == 1;
	            if (skill.NumberDeployed < maxDeployedMines && skill.CanUse())
	            {
	                DisableAlwaysHiglight();

	                // TODO: This should be precondition wait
	                await Coroutines.FinishCurrentAction();

	                var useAtResult = LokiPoe.InGameState.SkillBarHud.Use(_mineSlot, true);

	                if (useAtResult != LokiPoe.InGameState.UseResult.None)
	                    Log.Error($"[{Name}] UseAt returned {useAtResult} for {skill.Name}.");
	                
                    await Coroutines.LatencyWait();
                    await Coroutines.FinishCurrentAction(false);

	                if (!isInstantMineDetonation)
	                {
	                    await Coroutines.ReactionWait();
	                    LokiPoe.Input.SimulateKeyEvent(LokiPoe.Input.Binding.detonate_mines, true, false, false);
	                }

	                _mineStopwatch.Restart();
	                return true;
	            }
	        }

	        // Handle Wither logic.
	        if (_witherSlot != -1)
	        {
	            var skill = LokiPoe.InGameState.SkillBarHud.Slot(_witherSlot);
	            if (skill.CanUse(false, false, false) && !targetHasWither)
	            {
	                var usePositionedSkillResult = await UsePositionedSkill(skill, true, targetPosition);
	                if (usePositionedSkillResult) return true;
	            }
	        }

	        // Handle contagion logic.
	        if (_contagionSlot != -1)
	        {
	            var skill = LokiPoe.InGameState.SkillBarHud.Slot(_contagionSlot);
	            if (skill.CanUse(false, false, false) && !targetHasContagion)
	            {
	                var usePositionedSkillResult = await UsePositionedSkill(skill, true, targetPosition);
	                if (usePositionedSkillResult) return true;
	            }
	        }

	        // Handle cold snap logic. Only use when power charges won't be consumed.
	        if (_coldSnapSlot != -1)
	        {
	            var skill = LokiPoe.InGameState.SkillBarHud.Slot(_coldSnapSlot);
	            if (skill.CanUse(false, false, false))
	            {
	                var usePositionedSkillResult = await UsePositionedSkill(skill, true, targetPosition);
	                if (usePositionedSkillResult) return true;
	            }
	        }

            if (_orbOfStormsSkillSlot > 0)
            {
                var wasOrbOfStormsCast = await HandleOrbOfStorms(targetPosition);
                if (wasOrbOfStormsCast) return true;
            }

	        // Auto-cast any vaal skill at the best target as soon as it's usable.
	        if (RoutineSettings.Instance.AutoCastVaalSkills && _vaalStopwatch.ElapsedMilliseconds > 1000)
	        {
	            var firstVaalSkill = LokiPoe.InGameState.SkillBarHud.Skills.FirstOrDefault(s => s.SkillTags.Contains("vaal"));
	            if (firstVaalSkill != null)
	            {
	                if (firstVaalSkill.CanUse())
	                {
	                    var usePositionedSkillResult = await UsePositionedSkill(firstVaalSkill, true, targetPosition);
	                    if (usePositionedSkillResult)
	                    {
	                        _vaalStopwatch.Restart();
	                        return true;
	                    }
	                }
	            }
	        }

	        var shouldUseAoe = false;
	        var shouldUseMelee = false;

	        // Logic for figuring out if we should use an AoE skill or single target.
	        if (monsterCountNearTarget > 2 && targetRarity < Rarity.Rare)
	        {
	            shouldUseAoe = true;
	        }

	        // Logic for figuring out if we should use an AoE skill instead.
	        if (RoutineSettings.Instance.MaxMeleeRange > 0 && myPosition.Distance(targetPosition) < RoutineSettings.Instance.MaxMeleeRange)
	        {
	            shouldUseMelee = true;
	            shouldUseAoe = monsterCountWithinMeleeRangeOfMe >= 3;
	        }

	        var slot = GetPrimaryCombatSkillSlot(shouldUseMelee, shouldUseAoe);
	        if (slot == -1)
	        {
	            Log.Error($"[{Name}:Logic] There is no slot configured to use.");
	            return true;
	        }

            var maximumRange = shouldUseMelee || targetHasProximityShield ? RoutineSettings.Instance.MaxMeleeRange : RoutineSettings.Instance.MaxRangeRange;
            // Never allow a maximum range of 0. We'll just move forever. At worst, force it to like 5 or something.
            if (maximumRange < 5)
            {
                Log.Debug($"[{Name}:Logic] Adjusting maximum range from 0 to 5 to prevent perpetual movement.");
                maximumRange = 5;
            }

	        var currentDistance = LokiPoe.MyPosition.Distance(targetPosition);
	        if (currentDistance > maximumRange)
	        {
	            Log.Info($"[{Name}:Logic] Now moving towards {bestTarget.Name} because [dist ({currentDistance}) > maximum range ({maximumRange})]");

	            var idealTargetPosition = GetWalkablePointInRangeAndLineOfSightOfTarget(LokiPoe.MyPosition, bestTarget, maximumRange);

                // TODO: This is where I'd determine if that point is too close, and if so, LERP something sufficiently far enough away
                // along a line defined by the points (targetPosition, firstAcceptablePoint)

                if (!PlayerMover.MoveTowards(idealTargetPosition))
	            {
	                Log.Error($"[{Name}:Logic] MoveTowards failed for {idealTargetPosition}.");
	                // await Coroutines.FinishCurrentAction(); // Waiting to finish the current "action" after failing movement, before returning, seems pointless.
	            }
	            return true;
	        }

	        DisableAlwaysHiglight();

	        var slotSkill = LokiPoe.InGameState.SkillBarHud.Slot(slot);
	        if (slotSkill == null)
	        {
	            Log.Error($"[{Name}:Logic] There is no skill in the slot configured to use.");
	            return true;
	        }

            // This is what handles keeping the mouse on top of the current target in order to keep spamming a skill at/on them.
            // TODO: Factor in predictive angling for slower spells (e.g. Frostbolt)
            // TODO: This uses "cached target position" which may be stale
	        if (_isContinuouslyCasting && slot == _slotOfSkillBeingContinuouslyCast && 
                LokiPoe.Me.HasCurrentAction && (LokiPoe.ProcessHookManager.GetKeyState(slotSkill.BoundKey) & 0x8000) != 0)
	        {
	            var currentActionSkill = LokiPoe.Me.CurrentAction.Skill;
	            if (currentActionSkill != null && !currentActionSkill.InternalId.Equals("Interaction") && !currentActionSkill.InternalId.Equals("Move"))
	            {
                    // Keep the cursor on our target in order to keep it targeted
	                LokiPoe.Input.SetMousePos(targetPosition, false);
	                return true;
	            }
	        }

	        await Coroutines.FinishCurrentAction();

            // This is what handles starting the continuous cast of the primary skill.
            var beginUseAtResult = LokiPoe.InGameState.SkillBarHud.BeginUseAt(slot, RoutineSettings.Instance.AlwaysAttackInPlace, targetPosition);
	        if (beginUseAtResult != LokiPoe.InGameState.UseResult.None)
	        {
	            Log.Error($"[{Name}:Logic] BeginUseAt returned {beginUseAtResult}.");
	        }

	        _isContinuouslyCasting = true;
	        _slotOfSkillBeingContinuouslyCast = slot;

	        return true;
	    }

        /// <summary>
        /// Determine if any monster is within combat range
        /// </summary>
        /// <returns></returns>
	    private static bool IsAnyMonsterWithinCombatRange()
	    {
	        return LokiPoe.ObjectManager.Objects.OfType<Monster>()
	            .Any(m => m.IsActive && !m.IsMonolithed && m.Distance <= RoutineSettings.Instance.CombatRange);
	    }

	    /// <summary>
        /// Get the slot of the configured primary combat skill for the given scenario, falling back on the configured fallback if the primary is unavailable.
        /// </summary>
        /// <param name="shouldUseMelee"></param>
        /// <param name="shouldUseAoe"></param>
        /// <returns></returns>
	    private int GetPrimaryCombatSkillSlot(bool shouldUseMelee, bool shouldUseAoe)
	    {
	        var isAoeMeleeSlotCastable = IsSlotCastable(RoutineSettings.Instance.AoeMeleeSlot);
	        var isSingleTargetMeleeSlotCastable = IsSlotCastable(RoutineSettings.Instance.SingleTargetMeleeSlot);
	        var isAoeRangedSlotCastable = IsSlotCastable(RoutineSettings.Instance.AoeRangedSlot);
	        var isSingleTargetRangedSlotCastable = IsSlotCastable(RoutineSettings.Instance.SingleTargetRangedSlot);

	        var slot = -1;
	        if (shouldUseMelee)
	        {
	            if (shouldUseAoe && isAoeMeleeSlotCastable)
	                slot = RoutineSettings.Instance.AoeMeleeSlot;
	            else if (!shouldUseAoe && isSingleTargetMeleeSlotCastable)
	                slot = RoutineSettings.Instance.SingleTargetMeleeSlot;
	        }
	        else
	        {
	            if (shouldUseAoe && isAoeRangedSlotCastable)
	                slot = RoutineSettings.Instance.AoeRangedSlot;
	            else if (!shouldUseAoe && isSingleTargetRangedSlotCastable)
	                slot = RoutineSettings.Instance.SingleTargetRangedSlot;
	        }

	        if (slot == -1)
	            slot = RoutineSettings.Instance.FallbackSlot;
	        return slot;
	    }

        /// <summary>
        /// Multi-frame mini-coroutine for channeling Flameblast.
        /// </summary>
        /// <param name="myPos"></param>
        /// <returns></returns>
	    private async Task<bool> ChannelFlameBlast(Vector2i myPos)
	    {
	        var currentFlameblastCharges = LokiPoe.Me.FlameblastCharges;

	        // Handle being cast interrupted.
	        if (currentFlameblastCharges < _lastFlameblastCharges)
	        {
	            _isCastingFlameBlast = false;
	            return false;   // It was interrupted - we should continue on with combat logic.
	        }
	        _lastFlameblastCharges = currentFlameblastCharges;

	        if (currentFlameblastCharges >= RoutineSettings.Instance.MaxFlameBlastCharges)
	        {
	            // Stop using the skill, so it's cast.
	            await Coroutines.FinishCurrentAction();

	            _isCastingFlameBlast = false;
	        }
	        else
	        {
	            DisableAlwaysHiglight();

	            // Keep using the skill to build up charges.
	            var useAtResult = LokiPoe.InGameState.SkillBarHud.UseAt(_flameblastSlot, false, myPos);
	            if (useAtResult != LokiPoe.InGameState.UseResult.None)
	                Log.Error($"[{Name}] UseAt returned {useAtResult} for Flameblast.");
	        }

	        return true;
	    }

        /// <summary>
        /// Implements logic to handle a message passed through the system.
        /// Think of this as basic event handling.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public MessageResult Message(Message message)
        {
            Func<Tuple<object, string>[], object> f;
            if (_exposedSettings.TryGetValue(message.Id, out f))
            {
                var messageInputs = message.Inputs.ToArray();
                var setting = f(messageInputs);
                message.AddOutput(this, setting);

                return MessageResult.Processed;
            }

            if (message.Id == "GetCombatRange")
            {
                message.AddOutput(this, RoutineSettings.Instance.CombatRange);
                return MessageResult.Processed;
            }

            if (message.Id == "GetCombatTargeting")
            {
                message.AddOutput(this, CombatTargeting);
                return MessageResult.Processed;
            }

            if (message.Id == "ResetCombatTargeting")
            {
                CombatTargeting.ResetInclusionCalcuation();
                CombatTargeting.InclusionCalcuation += CombatTargetingOnInclusionCalcuation;

                CombatTargeting.ResetWeightCalculation();
                CombatTargeting.WeightCalculation += CombatTargetingOnWeightCalculation;
                return MessageResult.Processed;
            }

            // This would be a nearly-atomic operation with no effect on game state - continue task iteration afterwards.
            if (message.Id == "hook_core_area_changed_event")
            {
                _animatedItemBlacklist.Clear();
                _shrineTries.Clear();
                return MessageResult.Processed;
            }

            return MessageResult.Unprocessed;
        }

        #endregion

        #region Skill helpers

        /// <summary>
        /// Determine if the given skill is an aura
        /// </summary>
        /// <param name="skill"></param>
        /// <returns></returns>
        private static bool IsSkillAnAura(Skill skill)
        {
            var skillTags = skill.SkillTags;
            return (skillTags.Contains("aura") && !skillTags.Contains("vaal")) || skill.IsAurifiedCurse || skill.IsConsideredAura;
        }

	    private static readonly IEnumerable<string> AuraNames = new[]
	    {
            "Anger", "Clarity", "Determination", "Discipline", "Grace", "Haste", "Hatred", "Purity of Elements",
            "Purity of Fire", "Purity of Ice", "Purity of Lightning", "Vitality", "Wrath"
        };

        /// <summary>
        /// Determine if the given skill name corresponds to the known aura names
        /// </summary>
        /// <param name="skillName"></param>
        /// <returns></returns>
        private static bool IsSkillNameAnAuraName(string skillName)
        {
            return AuraNames.Contains(skillName);
        }

        /// <summary>
        /// Existing helper; help determine if a skill is usable
        /// </summary>
        /// <param name="skill"></param>
        /// <returns></returns>
        private static bool IsCastableHelper(Skill skill)
        {
            return skill != null && skill.IsCastable && !skill.IsTotem && !skill.IsTrap && !skill.IsMine;
        }

        #endregion

        #region Movement helpers

        /// <summary>
        /// Find the optimal point within line of sight of the given <see cref="targetMonster"/>, and if that's too close
        /// to the monster for safety, find an alternative within a 180* arc and within the given <see cref="maximumRange"/> of the monster,
        /// so that ranged classes can move within line-of-sight while maintaining a comfortable range.
        /// </summary>
        /// <param name="startPoint"></param>
        /// <param name="targetMonster"></param>
        /// <param name="maximumRange"></param>
        /// <returns></returns>
	    private Vector2i GetWalkablePointInRangeAndLineOfSightOfTarget(Vector2i startPoint, Monster targetMonster, int maximumRange)
	    {
            // Adjust maximumRanged down a bit in order to prevent movement attempt spam due to being RIGHT ON or near maximum range
	        maximumRange = (int) Math.Round(maximumRange*0.9);

	        var endPoint = targetMonster.Position;
            var pathToTarget = ExilePather.GetPointsOnSegment(startPoint, endPoint, true);

            var firstPositionInRangeAndSightOfTarget = pathToTarget
                .FirstOrDefault(v => v.Distance(endPoint) <= maximumRange && ExilePather.CanObjectSee(targetMonster, v, true));

            // If *no* position is within sight and range of the target, we have a problem.
	        if (firstPositionInRangeAndSightOfTarget == default(Vector2i)) return endPoint;

	        var distanceBetweenPointAndEndPoint = firstPositionInRangeAndSightOfTarget.Distance(endPoint);
	        Log.Debug($"[{Name}:Pathfinding] Found position in sight and range of target {firstPositionInRangeAndSightOfTarget} ({distanceBetweenPointAndEndPoint}).");

            // TODO: Make minimum range configurable. For now, default to 1/2 or 1/3 of maximum range.
            var minimumDistanceToTarget = maximumRange / 2;
            
            var isPointNearAnyMonster = IsPointWithinDistanceOfAnyActiveMonster(firstPositionInRangeAndSightOfTarget, minimumDistanceToTarget);
	        if (isPointNearAnyMonster)
	        {
                Log.Debug($"[{Name}:Pathfinding] First acceptable point is within minimum distance {minimumDistanceToTarget} of a monster.");

                // 1) Normalize vector to 0,0
	            // Well... I know how to normalize a line segment to 0,0... but how the hell does that apply to vectors
                // Looks like these Vector2i's store a point, but no direction. I can effectively treat them like points.

                var adjustedEndPoint = new Vector2i(endPoint.X - firstPositionInRangeAndSightOfTarget.X, endPoint.Y - firstPositionInRangeAndSightOfTarget.Y);
                var headingAngle = Math.Atan2(adjustedEndPoint.Y, adjustedEndPoint.X);
                // TODO: This can probably be significantly optimized

                // A heading angle of 0 would yield a maximum angle of 90 and a minimum angle of 270.
                // A heading angle of 90 would yield a maximum angle of 180 and a minimum angle of 0.
                // A heading angle of 180 would yield a maximum angle of 270 and a minimum angle of 90.
                // A heading angle of 270 would yield a maximum angle of 360 and a minimum angle of 180.
                var minimumAngle = headingAngle >= 90 ? headingAngle - 90 : (headingAngle + 360) - 90;

	            var bestPosition = firstPositionInRangeAndSightOfTarget;
	            var distanceFromBestPositionToEndPosition = firstPositionInRangeAndSightOfTarget.Distance(endPoint);
                var monsterCountNearBestPosition = MonsterCountWithinDistanceOfPosition(firstPositionInRangeAndSightOfTarget, minimumDistanceToTarget);

                // I can potentially just use the "minimum angle" as the starting point for my stepping and
                // rollover from there, handling the 360->0 translation
	            const int degreesPerStep = 8;
	            for (var index = 0; index < 20; index++)
	            {
	                var currentHeadingDegrees = (float)(index == 0 ? minimumAngle : minimumAngle + (index*degreesPerStep));
	                if (currentHeadingDegrees > 360) currentHeadingDegrees -= 360;

	                var currentHeadingRadians = MathEx.ToRadians(currentHeadingDegrees);
                    //Log.Debug($"[{Name}:Pathfinding] Current rotation is {currentHeadingDegrees}* ({currentHeadingRadians}rad)");
                    // LERP From the projected maximum point on line segment down to the minimum, stepping by 2 units
                    for (var projectedDistanceToTarget = maximumRange; projectedDistanceToTarget >= minimumDistanceToTarget; projectedDistanceToTarget -= 2)
                    {
                        // What a chain of kludges
                        // Extracted from MathEx.GetPointAt in order to cut down on unnecessary work
                        var x = (int) (Math.Cos(currentHeadingRadians)*projectedDistanceToTarget);
                        var y = (int) (Math.Sin(currentHeadingRadians)*projectedDistanceToTarget);
                        var potentialPosition = firstPositionInRangeAndSightOfTarget + new Vector2i(x, y);

                        //Log.Debug($"[{Name}:Pathfinding] Potential position: {potentialPosition} ({firstPositionInRangeAndSightOfTarget} + ({x},{y} [from {currentHeadingRadians} & {projectedDistanceToTarget}]))");

                        // If we can't walk to the point there's really nothing more to do
                        var isPointWalkable = ExilePather.IsWalkable(potentialPosition);
                        if (!isPointWalkable) continue;

                        // Warning: The clamp from float to int may slightly alter distance.
                        // If a raycast fails due to collision, it's no good
                        var collisionPoint = Vector2i.Zero;
                        var isRaycastClear = ExilePather.Raycast(endPoint, potentialPosition, out collisionPoint, true);
                        if (!isRaycastClear)
                        {
                            //Log.Debug($"[{Name}:Pathfinding] Raycasting discovered a collision at position {collisionPoint}.");

                            // Minor optimization: I know how far this collision is from our target. I can forward this loop until it clears the collision.
                            var collisionDistanceToTarget = endPoint.Distance(collisionPoint);
                            if (projectedDistanceToTarget > collisionDistanceToTarget)
                            {
                                //  This will set us EXACTLY AT the position of the collision. We want one step PAST the position of the collision.
                                // Additionally, delta is unnecessary entirely.
                                var adjustedDistanceToTarget = collisionDistanceToTarget - 2;
                                if (adjustedDistanceToTarget%2 != 0) adjustedDistanceToTarget -= 1; // Make sure we're adjusting by an even amount to match our stepping

                                projectedDistanceToTarget = adjustedDistanceToTarget;
                            }
                            continue;
                        }

                        var distanceFromPotentialPositionToEndPosition = potentialPosition.Distance(endPoint);
                        var monsterCountNearPotentialPosition = MonsterCountWithinDistanceOfPosition(potentialPosition, minimumDistanceToTarget);
                        //Log.Debug($"[{Name}:Pathfinding] Found walkable point {potentialPosition} in vision of target, at distance {distanceFromPotentialPositionToTarget}.");

                        if (monsterCountNearPotentialPosition < monsterCountNearBestPosition || 
                            (monsterCountNearPotentialPosition == monsterCountNearBestPosition && distanceFromPotentialPositionToEndPosition > distanceFromBestPositionToEndPosition))
                        {
                            //Log.Debug($"[{Name}:Pathfinding] Potential position {potentialPosition} ({distanceFromPotentialPositionToEndPosition} distance, {monsterCountNearPotentialPosition} monsters) is a better option than our previous {bestPosition} ({bestPosition.Distance(endPoint)} distance, {monsterCountNearBestPosition} monsters).");
                            bestPosition = potentialPosition;
                            monsterCountNearBestPosition = monsterCountNearPotentialPosition;
                            distanceFromBestPositionToEndPosition = distanceFromPotentialPositionToEndPosition;
                        }
                    }
                }

	            if (bestPosition != default(Vector2i))
	            {
	                Log.Debug($"[{Name}:Pathfinding] Using best position {bestPosition} ({bestPosition.Distance(endPoint)}).");
	                return bestPosition;
	            }
	        }

            Log.Debug($"[{Name}:Pathfinding] Could not find better position; falling back on position from existing path.");
	        return firstPositionInRangeAndSightOfTarget;
	    }

        /// <summary>
        /// Determine if the given point is within the given distance of any active monster.
        /// </summary>
        /// <param name="point"></param>
        /// <param name="minimumDistance"></param>
        /// <returns></returns>
	    private static bool IsPointWithinDistanceOfAnyActiveMonster(Vector2i point, int minimumDistance)
	    {
	        return LokiPoe.ObjectManager.Objects.OfType<Monster>()
	            .Any(m => m.IsActive && !m.IsMonolithed && m.Position.Distance(point) <= minimumDistance);
	    }

        /// <summary>
        /// Determine the number of active monsters within the given distance of the given position.
        /// </summary>
        /// <param name="position"></param>
        /// <param name="distance"></param>
        /// <returns></returns>
	    private static int MonsterCountWithinDistanceOfPosition(Vector2i position, int distance)
	    {
            return LokiPoe.ObjectManager.Objects.OfType<Monster>()
                .Count(m => m.IsActive && !m.IsMonolithed && m.Position.Distance(position) <= distance);
        }

	    #endregion

        /// <summary>
        /// Determine if the given item meets all basic criteria for Animate Guardian targets.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="maxLevel"></param>
        /// <returns></returns>
        private static bool DoesItemMeetAnimateGuardianCriteria(Item item, int maxLevel)
        {
            return item.RequiredLevel <= maxLevel && item.IsIdentified && !item.IsChromatic && item.SocketCount < 5 && item.MaxLinkCount < 5 && item.Rarity <= Rarity.Magic;
        }

        private static readonly MetadataFlags[] InvalidAnimateGuardianMetadataFlags = new MetadataFlags[]
        {
            MetadataFlags.Claws,
            MetadataFlags.OneHandAxes,
            MetadataFlags.OneHandMaces,
            MetadataFlags.OneHandSwords,
            MetadataFlags.OneHandThrustingSwords,
            MetadataFlags.TwoHandAxes,
            MetadataFlags.TwoHandMaces,
            MetadataFlags.TwoHandSwords
        };

        private WorldItem GetBestAnimateGuardianTarget(Monster monster, int maxLevel)
		{
			var worldItems = LokiPoe.ObjectManager.GetObjects().OfType<WorldItem>()
				.Where(wi => !_animatedItemBlacklist.Contains(wi.Id) && wi.Distance < 30 && DoesItemMeetAnimateGuardianCriteria(wi.Item, maxLevel))
				.OrderBy(wi => wi.Distance);
			foreach (var worldItem in worldItems)
			{
				var item = worldItem.Item;
				if (monster == null || monster.LeftHandWeaponVisual == "")
				{
                    var isItemInvalidAnimateGuardianTarget = item.HasMetadataFlags(InvalidAnimateGuardianMetadataFlags);
                    if (isItemInvalidAnimateGuardianTarget)
					{
						_animatedItemBlacklist.Add(worldItem.Id);
						return worldItem;
					}
				}

				if (monster == null || monster.ChestVisual == "")
				{
					if (item.HasMetadataFlags(MetadataFlags.BodyArmours))
					{
						_animatedItemBlacklist.Add(worldItem.Id);
						return worldItem;
					}
				}

				if (monster == null || monster.HelmVisual == "")
				{
					if (item.HasMetadataFlags(MetadataFlags.Helmets))
					{
						_animatedItemBlacklist.Add(worldItem.Id);
						return worldItem;
					}
				}

				if (monster == null || monster.BootsVisual == "")
				{
					if (item.HasMetadataFlags(MetadataFlags.Boots))
					{
						_animatedItemBlacklist.Add(worldItem.Id);
						return worldItem;
					}
				}

				if (monster == null || monster.GlovesVisual == "")
				{
					if (item.HasMetadataFlags(MetadataFlags.Gloves))
					{
						_animatedItemBlacklist.Add(worldItem.Id);
						return worldItem;
					}
				}
			}

			return null;
		}

        private static readonly MetadataFlags[] InvalidAnimateWeaponMetadataFlags = new MetadataFlags[]
        {
            MetadataFlags.Claws,
            MetadataFlags.OneHandAxes,
            MetadataFlags.OneHandMaces,
            MetadataFlags.OneHandSwords,
            MetadataFlags.OneHandThrustingSwords,
            MetadataFlags.TwoHandAxes,
            MetadataFlags.TwoHandMaces,
            MetadataFlags.TwoHandSwords,
            MetadataFlags.Daggers,
            MetadataFlags.Staves
        };

		private WorldItem GetBestAnimateWeaponTarget(int maxLevel)
		{
			var worldItems = LokiPoe.ObjectManager.GetObjectsByType<WorldItem>()
				.Where(wi => !_animatedItemBlacklist.Contains(wi.Id) && wi.Distance < 30 && DoesItemMeetAnimateGuardianCriteria(wi.Item, maxLevel))
				.OrderBy(wi => wi.Distance);

			foreach (var worldItem in worldItems)
			{
				var item = worldItem.Item;
				if (item.HasMetadataFlags(InvalidAnimateWeaponMetadataFlags))
				{
					_animatedItemBlacklist.Add(worldItem.Id);
					return worldItem;
				}
			}
			return null;
		}

		private Monster GetUsableCorpseClosestToPosition(Vector2i position)
		{
            // TODO: This ExilePather pathing might be extremely performance intensive. We need to review its use and potentially caching it.
			return LokiPoe.ObjectManager.GetObjectsByType<Monster>()
				.Where(m => m.IsActiveDead && m.CorpseUsable && m.Rarity != Rarity.Unique && m.Distance < 30 &&
                        //ExilePather.PathDistance(myPosition, m.Position, false, !RoutineSettings.Instance.LeaveFrame) < 30
                    ExilePather.PathDistance(position, m.Position) < 30
                )
				.OrderBy(m => m.Distance)
                .FirstOrDefault();
		}

        /// <summary>
        /// Reset all temporary values & variables for tracking combat state.
        /// </summary>
		private void ResetCombatState()
		{
			EnableAlwaysHiglight();

			if (_isContinuouslyCasting)
			{
                _isContinuouslyCasting = false;
                _slotOfSkillBeingContinuouslyCast = -1;

                LokiPoe.ProcessHookManager.ClearAllKeyStates();
			}
		}

		private readonly Dictionary<int, int> _shrineTries = new Dictionary<int, int>();

		private async Task<bool> InteractWithShrines()
		{
			// If the user wants to avoid shrine logic due to stuck issues, simply return without doing anything.
			if (RoutineSettings.Instance.SkipShrines)
				return false;

			// TODO: Shrines need speical CR logic, because it's now the CRs responsibility for handling all combaat situations,
			// and shrines are now considered a combat situation due their nature.

			// Check for any active shrines.
			var shrines = LokiPoe.ObjectManager.Objects.OfType<Shrine>()
				.Where(s => !Blacklist.Contains(s.Id) && !s.IsDeactivated && s.Distance < 50)
				.OrderBy(s => s.Distance);

            // For now, just take the first shrine found.
            var shrine = shrines.FirstOrDefault();
            if (shrine == null)
				return false;

			Log.Info($"[{Name}:HandleShrines]");

			int interactionAttempts;

			if (!_shrineTries.TryGetValue(shrine.Id, out interactionAttempts))
			{
				interactionAttempts = 0;
				_shrineTries.Add(shrine.Id, interactionAttempts);
			}

			if (interactionAttempts > 10)
			{
				Blacklist.Add(shrine.Id, TimeSpan.FromHours(1), "Could not interact with the shrine.");
				return true;
			}

			// Handle Skeletal Shrine in a special way, or handle priority between multiple shrines at the same time.
			var skeletonShrineOverrideFlag = shrine.ShrineId == "Skeletons";

			// Try and only move to touch it when we have a somewhat navigable path.
			if (skeletonShrineOverrideFlag || (NumberOfMobsBetween(LokiPoe.Me, shrine, 5, true) < 5 && NumberOfMobsNear(LokiPoe.Me, 20) < 3))
			{
				var myPosition = LokiPoe.MyPosition;
				var walkablePositionForShrine = ExilePather.FastWalkablePositionFor(shrine);

				// We need to filter out based on pathfinding, since otherwise, a large gap will lockup the bot.
				//var pathDistance = ExilePather.PathDistance(myPosition, walkablePositionForShrine, false, !RoutineSettings.Instance.LeaveFrame);
                var pathDistance = ExilePather.PathDistance(myPosition, walkablePositionForShrine);
				if (pathDistance > 50)
				{
				    Log.Debug($"[{Name}:HandleShrines] shrine {shrine.Name} ({shrine.Id}) is too far to use.");
					return false;
				}

                Log.Debug($"[{Name}:HandleShrines] Moving to shrine {shrine.Name} ({shrine.Id}) [{walkablePositionForShrine}, {pathDistance} units away].");

                //var canSee = ExilePather.CanObjectSee(LokiPoe.Me, pos, !RoutineSettings.Instance.LeaveFrame);

                // We're in distance when we're sure we're close to the position, but also that the path we need to take to the position
                // isn't too much further. This prevents problems with things on higher ground when we are on lower, and vice-versa.
                var inDistance = myPosition.Distance(walkablePositionForShrine) < 20 && pathDistance < 25;
				if (inDistance)
				{
					Log.Debug($"[{Name}:HandleShrines] Now attempting to interact with shrine {shrine.Name} ({shrine.Id}).");

					await Coroutines.FinishCurrentAction();
					await Coroutines.InteractWith(shrine);

					_shrineTries[shrine.Id]++;
				}
				else if (!PlayerMover.MoveTowards(walkablePositionForShrine))
				{
					Log.Error($"[{Name}:HandleShrines] Could not move to shrine position {walkablePositionForShrine}.");
                    Blacklist.Add(shrine.Id, TimeSpan.FromHours(1), "Could not move towards the shrine.");
					await Coroutines.FinishCurrentAction();
				}

				return true;
			}

			return false;
		}

		private bool _needsToDisableAlwaysHighlight;

		// This logic is now CR specific, because Strongbox gui labels interfere with targeting,
		// but not general movement using Move only.
		private void DisableAlwaysHiglight()
		{
		    if (!_needsToDisableAlwaysHighlight || !LokiPoe.ConfigManager.IsAlwaysHighlightEnabled) return;

		    Log.Debug($"[{Name}] Disabling \"always highlght\" in order to avoid skill targeting issues...");
            LokiPoe.Input.SimulateKeyEvent(LokiPoe.Input.Binding.highlight_toggle, true, false, false);

            // Really, really ghetto way of skipping a frame
		    using (LokiPoe.Memory.ReleaseFrame(LokiPoe.UseHardlock))
		    {
		        
		    }
		}

		// This logic is now CR specific, because Strongbox gui labels interfere with targeting,
		// but not general movement using Move only.
		private void EnableAlwaysHiglight()
		{
		    if (LokiPoe.ConfigManager.IsAlwaysHighlightEnabled) return;

		    Log.Info($"[{Name}] Enabling \"always highlight\".");
            LokiPoe.Input.SimulateKeyEvent(LokiPoe.Input.Binding.highlight_toggle, true, false, false);

            // Really, really ghetto way of skipping a frame
            using (LokiPoe.Memory.ReleaseFrame(LokiPoe.UseHardlock))
            {

            }
        }

        /// <summary>
        /// Determine if the skill at the given slot is castable.
        /// </summary>
        /// <param name="slot"></param>
        /// <returns></returns>
	    private static bool IsSlotCastable(int slot)
	    {
	        if (slot < 0) return false;

            var slotSkill = LokiPoe.InGameState.SkillBarHud.Slot(slot);
	        if (slotSkill == null || !slotSkill.CanUse()) return false;

	        return true;
	    }

        #region Co-routines / Sub routines

	    private Vector2i _pointOfLastOrbOfStormsUse = Vector2i.Zero;
	    private DateTime _timeWhenOrbOfStormsExpires = DateTime.Now;
	    private const int OrbOfStormsEffectRadius = 10;

        /// <summary>
        /// Minimalist multi-frame routine for using Orb of Storms, if necessary.
        /// 
        /// </summary>
        /// <returns>"true" if we performed a game state-changing operation and must stop routine processing this frame, otherwise "false"</returns>
	    private async Task<bool> HandleOrbOfStorms(Vector2i targetPosition)
        {
            if (_orbOfStormsSkillSlot < 0) return false;

            var now = DateTime.Now;
            // If Orb of Storms hasn't expired and we're still within range, there's no reason to cast it again
            if (now < _timeWhenOrbOfStormsExpires && LokiPoe.MyPosition.Distance(_pointOfLastOrbOfStormsUse) < OrbOfStormsEffectRadius)
                return false;

            var orbOfStormsSkill = LokiPoe.InGameState.SkillBarHud.Slot(_orbOfStormsSkillSlot);
            if (!orbOfStormsSkill.IsValid || !orbOfStormsSkill.CanUse()) return false;

            var wasOrbOfStormsSuccessful = await UsePositionedSkill(orbOfStormsSkill, true, targetPosition);
            if (!wasOrbOfStormsSuccessful) return false;

            var orbOfStormsDurationInMilliseconds = orbOfStormsSkill.Stats[StatTypeGGG.SkillEffectDuration];
            _timeWhenOrbOfStormsExpires = DateTime.Now.AddMilliseconds(orbOfStormsDurationInMilliseconds);
            _pointOfLastOrbOfStormsUse = LokiPoe.MyPosition;
            return true;
        }

        /// <summary>
        /// Multi-frame routine for activating any usable aura skills.
        /// </summary>
        /// <returns></returns>
        private async Task<bool> ActivateAuras()
        {
            var auraSkills = LokiPoe.InGameState.SkillBarHud.Skills.Where(s => _detectedAuraSkillIds.Any(n => n == s.Id));

            // If I'm missing a golem, summon it.
            var usedAura = false;
            foreach (var skill in auraSkills)
            {
                // If activating this skill would cap us out on mana, we need to pass.
                if (LokiPoe.ObjectManager.Me.ManaReservedPercent + skill.SpellCost() >= 99) continue;

                // Discipline requires special handling - AmIUsingConsideredAuraWithThis always returns "false" for it.
                if (skill.Id == DisciplineSkillId)
                {
                    var myId = LokiPoe.Me.Id;
                    var isDisciplineAuraPresentFromMe = LokiPoe.Me.Auras.Any(a => a.CasterId == myId && a.Name == DisciplineAuraName);
                    if (isDisciplineAuraPresentFromMe) continue;
                }
                else if (skill.AmIUsingConsideredAuraWithThis || (skill.IsAurifiedCurse && skill.AmICursingWithThis)) continue;

                // If it's not on the hotbar, move it onto the hotbar.
                if (skill.Slot < 0)
                {
                    Log.Info($"[{Name}:ActivateAuras] Temporarily making skill {skill.Name} active in slot 4.");
                    await AssignSkillToSlotFour(skill);
                }

                // Cast it.
                var useSkillResult = await UseSkill(skill);
                if (useSkillResult)
                    usedAura = true;

                // Interruption option 1: Polling for reasons to cancel (e.g. nearby monsters)
                // Interruption option 2: Respecting a CancellationToken and its requests to abort
                // Let's try #1 for now...
                // Wait. My goal is to migrate these toward being as atomic as possible - which means not using a motherfucking loop.
                // I should experiment with both options. On one hand, it'd be nice to have an async/await-compatible "behavior" guidance class
                //  which doesn't have to worry about the nuances of cross-frame operations.
                // On the other hand, I know an atomic pulsed architecture works... I dunno. It requires further research.

                // I should almost move this to Tick() since it's designed to be a constantly-running background information update source... I'll do that.
                var isAnyMonsterWithinCombatRange = IsAnyMonsterWithinCombatRange();
                if (isAnyMonsterWithinCombatRange)
                {
                    Log.Info($"[{Name}:ActivateAuras] A monster was detected within combat range; aborting aura activation.");
                    break;
                }
            }

            if (usedAura)
            {
                // If we have to reset the temporary slot, do so.
                if (_skillIdOriginallyInSlotFour >= 0)
                {
                    await RestoreOriginalSkillToSlotFour();
                }

                return true;
            }
            return false;
        }

        /// <summary>
        /// Multi-frame process for restoring the skill originally in slot 4, to slot 4.
        /// </summary>
        /// <returns></returns>
	    private async Task RestoreOriginalSkillToSlotFour()
	    {
	        var skillAtSlotFour = LokiPoe.InGameState.SkillBarHud.Slot(4);
	        var skillOriginallyInSlotFour =
	            LokiPoe.InGameState.SkillBarHud.Skills.First(s => s.Id == _skillIdOriginallyInSlotFour);
	        if (skillAtSlotFour.Id != skillOriginallyInSlotFour.Id)
	        {
	            Log.Info($"[{Name}:ActivateAuras] Restoring skill {skillOriginallyInSlotFour.Name} to slot 4.");
	            await AssignSkillToSlotFour(skillOriginallyInSlotFour);
	        }
	    }

	    /// <summary>
        /// Multi-frame process for summoning a corpse-based minion.
        /// </summary>
        /// <param name="skillSlot"></param>
        /// <param name="skillGoverningStat"></param>
        /// <returns></returns>
        private async Task<bool> SummonCorpseBasedMinion(int skillSlot, StatTypeGGG skillGoverningStat)
        {
            // See if we can use the skill.
            var skill = LokiPoe.InGameState.SkillBarHud.Slot(skillSlot);
            if (!skill.CanUse()) return false;

            var max = skill.GetStat(skillGoverningStat);
            if (skill.NumberDeployed >= max) return false;

            // Check for a target near us.
            var myPosition = LokiPoe.MyPosition;
            var target = GetUsableCorpseClosestToPosition(myPosition);
            if (target == null) return false;

            Log.Info($"[{Name}:SummonCorpseBasedMinion] Using {skill.Name} on {target.Name}.");

            var useTargetedSkillResult = await UsePositionedSkill(skill, false, target.Position);
            return useTargetedSkillResult;
        }

        /// <summary>
        /// Multi-frame process for assigning the given skill to skill slot #4.
        /// </summary>
        /// <param name="skill"></param>
        /// <returns></returns>
        private async Task AssignSkillToSlotFour(Skill skill)
        {
            Log.Debug($"[{Name}:AssignSkillToSlotFour] Assigning skill {skill.Name} to slot 4.");

            await Coroutine.Sleep(1000);
            var setSlotResult = LokiPoe.InGameState.SkillBarHud.SetSlot(4, skill);
            if (setSlotResult == LokiPoe.InGameState.SetSlotResult.None)
            {
                await Coroutines.LatencyWait();
                await Coroutine.Sleep(1000);
            }
            else
            {
                Log.Error($"[{Name}:AssignSkillToSlotFour] Unable to set slot 4 to skill {skill.Name}.");
            }
        }

        /// <summary>
        /// Multi-frame process for interrupting any skill being cast.
        /// </summary>
        /// <returns></returns>
	    private static async Task InterruptAnyCurrentSkill()
        {
            LokiPoe.ProcessHookManager.ClearAllKeyStates();
            await Coroutines.LatencyWait();
        }

        /// <summary>
        /// Multi-frame process for using the given skill.
        /// </summary>
        /// <param name="skill"></param>
        /// <returns></returns>
        private async Task<bool> UseSkill(Skill skill)
        {
            if (_isContinuouslyCasting)
                await InterruptAnyCurrentSkill();

            LokiPoe.InGameState.UseResult useResult;
            using (LokiPoe.AcquireFrame())
            {
                useResult = LokiPoe.InGameState.SkillBarHud.Use(skill.Slot, false);
                if (useResult == LokiPoe.InGameState.UseResult.None)
                {
                    return true;
                }
            }

            Log.Error($"[{Name}:UseSkill] Use returned {useResult} for {skill.Name}.");
            return false;
        }

	    /// <summary>
        /// Multi-frame process for using the given skill on the given target, optionally specifying to cast in-place,
        /// disabling "always highlight" if necessary.
        /// </summary>
        /// <param name="skill"></param>
        /// <param name="inPlace"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        private async Task<bool> UseTargetedSkill(Skill skill, bool inPlace, NetworkObject target)
        {
            DisableAlwaysHiglight();

            if (_isContinuouslyCasting)
                await InterruptAnyCurrentSkill();

	        LokiPoe.InGameState.UseResult useResult;
	        using (LokiPoe.AcquireFrame())
	        {
	            useResult = LokiPoe.InGameState.SkillBarHud.UseOn(skill.Slot, inPlace, target);
	            if (useResult == LokiPoe.InGameState.UseResult.None)
	            {
	                // We need to remove the item highlighting.
	                LokiPoe.ProcessHookManager.ClearAllKeyStates();

	                return true;
	            }
	        }

	        Log.Error($"[{Name}:UseTargetedSkill] Use returned {useResult} for {skill.Name}.");
            return false;
        }

        /// <summary>
        /// Multi-frame process for using the given skill at the given position, optionally casting in-place,
        /// disabling "always highlight" if necessary.
        /// </summary>
        /// <param name="skill"></param>
        /// <param name="inPlace"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        private async Task<bool> UsePositionedSkill(Skill skill, bool inPlace, Vector2i position)
        {
            DisableAlwaysHiglight();

            if (_isContinuouslyCasting)
                await InterruptAnyCurrentSkill();

            LokiPoe.InGameState.UseResult useResult;
            using (LokiPoe.AcquireFrame())
            {
                useResult = LokiPoe.InGameState.SkillBarHud.UseAt(skill.Slot, inPlace, position);
                if (useResult == LokiPoe.InGameState.UseResult.None)
                {
                    // We need to remove the item highlighting.
                    LokiPoe.ProcessHookManager.ClearAllKeyStates();

                    return true;
                }
            }

            Log.Error($"[{Name}:UsePositionedSkill] Use returned {useResult} for {skill.Name}.");
            return false;
        }

        /// <summary>
        /// Multi-frame mini-coroutine for summoning any available golems.
        /// </summary>
        /// <param name="myPosition"></param>
        /// <returns></returns>
	    private async Task<bool> SummonGolems(Vector2i myPosition)
        {
            var golemSkills = LokiPoe.InGameState.SkillBarHud.Skills.Where(s => _detectedGolemSkillIds.Any(n => n == s.Id));

            var maxSummonedGolems = golemSkills.First().GetStat(StatTypeGGG.NumberOfGolemsAllowed); // Max summoned golems should be constant across the skills
            var currentGolemCount = golemSkills.Sum(s => s.NumberDeployed);

            // TODO: re-summon any golem at <= 25% HP as it'll likely die in the next engagement anyway

            // If I'm missing a golem, summon it.
            var summonedGolem = false;
            while (currentGolemCount < maxSummonedGolems)
            {
                var skill = golemSkills.First(s => s.NumberDeployed == 0);

                // If it's not on the hotbar, move it onto the hotbar.
                if (skill.Slot < 0)
                {
                    Log.Info($"[{Name}:SummonGolems] Temporarily making skill {skill.Name} active in slot 4.");
                    await AssignSkillToSlotFour(skill);
                }

                // Cast it.
                var usePositionedSkillResult = await UsePositionedSkill(skill, true, myPosition);
                if (usePositionedSkillResult)
                {
                    summonedGolem = true;
                }

                // Refresh our golem count
                currentGolemCount = golemSkills.Sum(s => s.NumberDeployed);

                var isAnyMonsterWithinCombatRange = IsAnyMonsterWithinCombatRange();
                if (isAnyMonsterWithinCombatRange)
                {
                    Log.Info($"[{Name}:SummonGolems] A monster was detected within combat range; aborting golem summoning.");
                    break;
                }
            }

            if (summonedGolem)
            {
                _summonGolemStopwatch.Restart();

                // If we have to reset the temporary slot, do so.
                if (_skillIdOriginallyInSlotFour >= 0)
                {
                    await RestoreOriginalSkillToSlotFour();
                }

                return true;
            }
            return false;
        }

        /// <summary>
        /// Multi-frame mini-coroutine handling any necessary summoning during combat.
        /// </summary>
        /// <returns></returns>
        private async Task<bool> HandleCombatSummoning()
        {
            // Limit this logic once per second, because it can get expensive and slow things down if run too fast.
            if (_animateGuardianSlot != -1 && _animateGuardianStopwatch.ElapsedMilliseconds > 1000)
            {
                // See if we can use the skill.
                var skill = LokiPoe.InGameState.SkillBarHud.Slot(_animateGuardianSlot);
                if (skill.CanUse())
                {
                    // Check for a target near us.
                    var target = GetBestAnimateGuardianTarget(skill.DeployedObjects.FirstOrDefault() as Monster, skill.GetStat(StatTypeGGG.AnimateItemMaximumLevelRequirement));
                    if (target != null)
                    {
                        Log.Info($"[Logic] Using {skill.Name} on {target.Name}.");

                        var useTargetedSkillResult = await UseTargetedSkill(skill, true, target);
                        if (useTargetedSkillResult)
                        {
                            _animateGuardianStopwatch.Restart();
                            return true;
                        }
                    }
                }
            }

            // Limit this logic once per second, because it can get expensive and slow things down if run too fast.
            if (_animateWeaponSlot != -1 && _animateWeaponStopwatch.ElapsedMilliseconds > 1000)
            {
                // See if we can use the skill.
                var skill = LokiPoe.InGameState.SkillBarHud.Slot(_animateWeaponSlot);
                if (skill.CanUse())
                {
                    // Check for a target near us.
                    var target = GetBestAnimateWeaponTarget(skill.GetStat(StatTypeGGG.AnimateItemMaximumLevelRequirement));
                    if (target != null)
                    {
                        Log.Info($"[Logic] Using {skill.Name} on {target.Name}.");

                        var useTargetedSkillResult = await UseTargetedSkill(skill, true, target);
                        if (useTargetedSkillResult)
                        {
                            _animateWeaponStopwatch.Restart();
                            return true;
                        }
                    }
                }
            }

            // If we have Raise Spectre, we can look for dead bodies to use for our army as we move around.
            if (_raiseSpectreSlot != -1)
            {
                var summonResult = await SummonCorpseBasedMinion(_raiseSpectreSlot, StatTypeGGG.NumberOfSpectresAllowed);
                if (summonResult) return true;
            }

            // If we have Raise Zombie, we can look for dead bodies to use for our army as we move around.
            if (_raiseZombieSlot != -1)
            {
                var summonResult = await SummonCorpseBasedMinion(_raiseZombieSlot, StatTypeGGG.NumberOfZombiesAllowed);
                if (summonResult) return true;
            }

            return false;
        }

        #endregion
    }
}