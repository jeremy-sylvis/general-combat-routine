﻿<UserControl
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    mc:Ignorable="d"
    d:DesignHeight="440" d:DesignWidth="627">
    <Grid x:Name="Root">
        <ScrollViewer VerticalScrollBarVisibility="Auto">
            <StackPanel>
                <GroupBox Header="Skill Slots">
                    <Grid>
                        <Grid.RowDefinitions>
                            <RowDefinition Height="Auto" />
                            <RowDefinition Height="Auto" />
                            <RowDefinition Height="Auto" />
                        </Grid.RowDefinitions>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="2*" />
                            <ColumnDefinition Width="*" />
                            <ColumnDefinition Width="2*" />
                            <ColumnDefinition Width="*" />
                        </Grid.ColumnDefinitions>

                        <Label Grid.Row="0" Grid.Column="0" Content="Single Target Melee Skill Slot" Margin="3"
                           ToolTipService.ToolTip="The skill slot (1-8) to use in melee range. If set to -1, it will not be used." />
                        <ComboBox Name="SingleTargetMeleeSlotComboBox" Grid.Row="0" Grid.Column="1" Margin="3" IsEditable="False" />

                        <Label Grid.Row="0" Grid.Column="2" Content="Single Target Ranged Skill Slot" Margin="3"
                           ToolTipService.ToolTip="The skill slot (1-8) to use outside of melee range. If set to -1, it will not be used." />
                        <ComboBox Name="SingleTargetRangedSlotComboBox" Grid.Row="0" Grid.Column="3" Margin="3" IsEditable="False" />

                        <Label Grid.Row="1" Grid.Column="0" Content="Aoe Melee Skill Slot" Margin="3"
                           ToolTipService.ToolTip="The skill slot  (1-8) to use in melee range for AoE. If set to -1, it will not be used." />
                        <ComboBox Name="AoeMeleeSlotComboBox" Grid.Row="1" Grid.Column="1" Margin="3" IsEditable="False" />

                        <Label Grid.Row="1" Grid.Column="2" Content="Aoe Ranged Skill Slot" Margin="3"
                           ToolTipService.ToolTip="The skill slot (1-8) to use outside of melee range for AoE. If set to -1, it will not be used." />
                        <ComboBox Name="AoeRangedSlotComboBox" Grid.Row="1" Grid.Column="3" Margin="3" IsEditable="False" />

                        <Label Grid.Row="2" Grid.Column="0" Content="Fallback Skill Slot" Margin="3"
                           ToolTipService.ToolTip="The skill to force use when out of mana, or the desired skill cannot be cast." />
                        <ComboBox Name="FallbackSlotComboBox" Grid.Row="2" Grid.Column="1" Margin="3" IsEditable="False" />
                    </Grid>
                </GroupBox>
                <GroupBox Header="Skill Ranges">
                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="2*" />
                            <ColumnDefinition Width="*" />
                            <ColumnDefinition Width="2*" />
                            <ColumnDefinition Width="*" />
                        </Grid.ColumnDefinitions>
                        <Grid.RowDefinitions>
                            <RowDefinition Height="Auto" />
                            <RowDefinition Height="Auto" />
                        </Grid.RowDefinitions>

                        <Label Grid.Row="0" Grid.Column="0" Content="Combat Range" Margin="3"
                           ToolTipService.ToolTip="How far to consider combat targets. Do not set too high, or the bot might get stuck running back and forth trying to get to a target." />
                        <TextBox Name="CombatRangeTextBox" Grid.Row="0" Grid.Column="1" Margin="3" />

                        <Label Grid.Row="1" Grid.Column="0" Content="Max Melee Distance" Margin="3"
                           ToolTipService.ToolTip="How close does a mob need to be to trigger the Melee skill. Do not set too high, as the cursor will overlap the GUI." />
                        <TextBox Name="MaxMeleeRangeTextBox" Grid.Row="1" Grid.Column="1" Margin="3" />

                        <Label Grid.Row="1" Grid.Column="2" Content="Max Ranged Distance" Margin="3"
                           ToolTipService.ToolTip="How close does a mob need to be to trigger the Range skill. Do not set too high, as the cursor will overlap the GUI." />
                        <TextBox Name="MaxRangeRangeTextBox" Grid.Row="1" Grid.Column="3" Margin="3" />
                    </Grid>
                </GroupBox>
                <GroupBox Header="Miscellaneous Skill Settings">
                    <Grid>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="2*" />
                            <ColumnDefinition Width="*" />
                            <ColumnDefinition Width="2*" />
                            <ColumnDefinition Width="*" />
                        </Grid.ColumnDefinitions>
                        <Grid.RowDefinitions>
                            <RowDefinition Height="Auto" />
                            <RowDefinition Height="Auto" />
                            <RowDefinition Height="Auto" />
                            <RowDefinition Height="Auto" />
                            <RowDefinition Height="Auto" />
                            <RowDefinition Height="Auto" />
                            <RowDefinition Height="Auto" />
                            <RowDefinition Height="Auto" />
                        </Grid.RowDefinitions>
                        
                        <Label Grid.Row="0" Grid.Column="0" Content="Always Attack In-place" Margin="3"
                           ToolTipService.ToolTip="Forces the combat routine to always attack in-place, e.g. ranged skill use won't trigger movement." />
                        <CheckBox Name="AlwaysAttackInPlaceCheckBox" Grid.Row="0" Grid.Column="1" Margin="3" HorizontalAlignment="Center" VerticalAlignment="Center" />

                        <Label Grid.Row="0" Grid.Column="2" Content="Flame Blast Charge Threshold" Margin="3"
                           ToolTipService.ToolTip="The # of Flame Blast charges to accumulate before releasing charges." />
                        <TextBox Name="MaxFlameBlastChargesTextBox" Grid.Row="0" Grid.Column="3" Margin="3" />

                        <Label Grid.Row="1" Grid.Column="0" Content="Molten Shell Delay" Margin="3"
                           ToolTipService.ToolTip="The delay between usages of Molten Shell, in milliseconds." />
                        <TextBox Name="MoltenShellDelayMsTextBox" Grid.Row="1" Grid.Column="1" Margin="3" />

                        <Label Grid.Row="1" Grid.Column="2" Content="Totem Delay" Margin="3"
                           ToolTipService.ToolTip="The delay between usages of totems, in milliseconds." />
                        <TextBox Name="TotemDelayMsTextBox" Grid.Row="1" Grid.Column="3" Margin="3" />

                        <Label Grid.Row="2" Grid.Column="0" Content="Trap Delay" Margin="3"
                           ToolTipService.ToolTip="The delay between usages of traps, in milliseconds." />
                        <TextBox Name="TrapDelayMsTextBox" Grid.Row="2" Grid.Column="1" Margin="3" />

                        <Label Grid.Row="2" Grid.Column="2" Content="Mine Delay" Margin="3"
                           ToolTipService.ToolTip="The delay between usages of mines, in milliseconds." />
                        <TextBox Name="MineDelayMsTextBox" Grid.Row="2" Grid.Column="3" Margin="3" />

                        <Label Grid.Row="3" Grid.Column="0" Content="Summon Raging Spirit Casts" Margin="3"
                           ToolTipService.ToolTip="The number of casts of Summon Raging Spirit to allow before triggering a delay." />
                        <TextBox Name="SummonRagingSpiritCountPerDelayTextBox" Grid.Row="3" Grid.Column="1" Margin="3" />

                        <Label Grid.Row="3" Grid.Column="2" Content="Summon Raging Spirit Delay" Margin="3"
                           ToolTipService.ToolTip="The delay between batches usages of Summon Raging Spirit, in milliseconds." />
                        <TextBox Name="SummonRagingSpiritDelayMsTextBox" Grid.Row="3" Grid.Column="3" Margin="3" />

                        <Label Grid.Row="4" Grid.Column="0" Content="Summon Skeleton Casts" Margin="3"
                           ToolTipService.ToolTip="The number of casts of Summon Skeleton to allow before triggering a delay." />
                        <TextBox Name="SummonSkeletonCountPerDelayTextBox" Grid.Row="4" Grid.Column="1" Margin="3" />

                        <Label Grid.Row="4" Grid.Column="2" Content="Summon Skeleton Delay" Margin="3"
                           ToolTipService.ToolTip="The delay between batches of usages of Summon Skeleton, in milliseconds." />
                        <TextBox Name="SummonSkeletonDelayMsTextBox" Grid.Row="4" Grid.Column="3" Margin="3" />

                        <Label Grid.Row="5" Grid.Column="0" Content="Use Vaal Skills" Margin="3"
                           ToolTipService.ToolTip="If checked, the combat routine will use any detected Vaal skills as available." />
                        <CheckBox Name="AutoCastVaalSkillsCheckBox" Grid.Row="5" Grid.Column="1" Margin="3" HorizontalAlignment="Center" VerticalAlignment="Center" />

                        <Label Grid.Row="5" Grid.Column="2" Content="Log Aura Debug Info" Margin="3"
                           ToolTipService.ToolTip="When checked, the combat routine will dump additional diagnostic information for debugging Auras." />
                        <CheckBox Name="DebugAurasCheckBox" Grid.Row="5" Grid.Column="3" Margin="3" HorizontalAlignment="Center" VerticalAlignment="Center" />

                        <Label Grid.Row="6" Grid.Column="0" Content="Use Auras from items" Margin="3"
                           ToolTipService.ToolTip="When checked, we will use auras provided by items over auras provided by skills." />
                        <CheckBox Name="EnableAurasFromItemsCheckBox" Grid.Row="6" Grid.Column="1" Margin="3" HorizontalAlignment="Center" VerticalAlignment="Center" />

                        <Label Grid.Row="6" Grid.Column="2" Content="Use cross-frame pathfinding" Margin="3"
                           ToolTipService.ToolTip="When checked, we will allow cross-frame pathfinding operations to occur." />
                        <CheckBox Name="LeaveFrameCheckBox" Grid.Row="6" Grid.Column="3" Margin="3" HorizontalAlignment="Center" VerticalAlignment="Center" />

                        <Label Grid.Row="7" Grid.Column="0" Content="Skill ID Blacklist" Margin="3"
                           ToolTipService.ToolTip="A list of IDs of skills to not use, separated by commas e.g. &quot;591, 39154, 39&quot;" />
                        <TextBox Name="BlacklistedSkillIdsTextBox" Grid.Row="7" Grid.Column="1" Margin="3" />

                        <Label Grid.Row="7" Grid.Column="2" Content="Skip Shrines" Margin="3"
                           ToolTipService.ToolTip="If checked, the combat routine will ignore shrines." />
                        <CheckBox Name="SkipShrinesCheckBox" Grid.Row="7" Grid.Column="3" Margin="3" HorizontalAlignment="Center" VerticalAlignment="Center" />
                    </Grid>
                </GroupBox>
            </StackPanel>
        </ScrollViewer>
    </Grid>
</UserControl>