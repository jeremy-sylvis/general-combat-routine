﻿using System.Collections.Generic;
using Loki.Bot.Pathfinding;
using Loki.Common;

namespace GeneralCombatRoutine.PoeShims
{
    public class ExilePatherShim : IExilePather
    {
        /// <summary>
        /// Get the points representing a path along the line segment defined by the two given points.
        /// </summary>
        /// <param name="startPoint">The starting point of the line segment.</param>
        /// <param name="endPoint">The ending point of the line segment.</param>
        /// <returns>The set of points representing the path between the two given points.</returns>
        public IEnumerable<Vector2i> GetPointsOnSegment(Vector2i startPoint, Vector2i endPoint)
        {
            return ExilePather.GetPointsOnSegment(startPoint, endPoint, true);
        }
    }
}
