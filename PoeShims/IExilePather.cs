﻿using System.Collections.Generic;
using Loki.Common;

namespace GeneralCombatRoutine.PoeShims
{
    public interface IExilePather
    {
        IEnumerable<Vector2i> GetPointsOnSegment(Vector2i startPoint, Vector2i endPoint);
    }
}
