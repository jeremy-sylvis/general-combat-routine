﻿Forum Thread: 
https://www.thebuddyforum.com/threads/exilebuddy-3-0-reference-thread.406684

[3.0.0.7-oriath]
* Updates for 3.0.0.7.
* CommonEvents removed and integrated into OldGrindBot.
* OldGrindBot removed from inclusion with Exilebuddy.
* OGB specific properties added to ObjectManager have been removed and placed into ObjectManagerHelper in v3.cs.

[3.0.0.5-oriath]
* Updates for 3.0.0.5.
* Added Item.HighestImplicitValue
* ItemFilterEditor updates to support HighestImplicitValue.
* IsMapRoom updated to handle 2_11_lab.

[3.0.0.17]
* InstanceInfo.IsGamePaused added to detect game pauses due to various in-game events.

[3.0.0 Phase 4]
* GreyMagic updates.
* Microsoft.Net.Compilers updated to 2.3.0 from 2.2.0.

[3.0.0 Phase 3]
* Added InGameState.IsSkipAllTutorialsButtonShowing.
* Added InGameState.SkipAllTutorials.
* Added InGameState.IsHelpPopupShowing.
* Added PlayerComponent.HasHideout.
* PlayerComponent.HideoutLevel now correctly returns 0 when no hideout exists.
* Added a button to set OGB's current grind zone.
* Various tasks commented out from OGB for now.
* Added a DumpTab plugin.
* Re-added ItemFilterEditor for now because it's too annoying testing without it.

[3.0.0 Phase 2]
* Old plugins being removed:
** AutoFlask - An alternative is present in the Default assembly now.
** Chicken - An alternative is present in the Default assembly now.
** StuckDetection - An alternative already exists in the Default assembly.
** Stats - Never did anything really.
** ItemFilterEditor - Total revamp coming / an alternative already exists in the Default assembly.
* Removed IItemFilter.
* StaticLocationManager has been removed from Loki.Bot.
* The unused ItemEvaluatorEx class has been removed.
* ThirdPartyLoader txt file loading has been removed in favor of a single json file.
* Added a new ThirdPartyConfigMaker tool.
* ThirdPartyLoader now supports dependency based loading.

[3.0.0 Phase 1]
* LokiPoe.DevLogging added to toggle off native data dumps in ToString functions.
* A lot of ToString functions updated to provide more usable data.
* DatWorldAreaWrapper.Difficulty removed.
* LocalData.AreaHash now is based on the area id and seed.
* AreaPather revamp in progress. Not correct for the new map layout but mostly ok.
* LokiPoe.GetZoneId removed.
* LokiPoe.LabyrinthTrialAreaIds added.
* InventoryUi updates for cosmetics tab addition:
** IsInventoryTabSelected / IsCosmeticsTabSelected added
** SwitchToInventoryTab / SwitchToCosmeticsTab added
* LabyrinthMapSelectorUi added but not finished yet.
* WorldUi.GoToLabyrinth added.
* WorldUi.TakeWaypoint variants that took Difficulty have been removed.
* GlobalWarningDialog.IsSealProphecyOverlayOpen added.
* QuestComponent.Difficulty removed.
* Prophecy.Difficulty removed.

[Pre 3.0.0]

All changes are detailed here: https://www.thebuddyforum.com/threads/big-thread-of-upcoming-3-0-changes.299068

Global
* Exilebuddy now uses VS 2017.
* Buddy.Overlay removed.
* MahApps updated to 1.5.0 from 1.4.3.
* Newtonsoft.Json updated to 10.0.3 from 9.0.1.
* DotNetCompilerPlatform updated to 1.0.5 from 1.0.3.
* Microsoft.Net.Compilers updated to 2.2.0 from 2.0.1.
* Previously marked Obsolete entries removed:
** InventoryControlWrapper.EssenceTabItem, InventoryControlWrapper.CurrencyTabItem [use CustomTabItem].
** InventoryControlWrapper.GetItemCost [use GetItemCostEx]
** DatQuestStateWrapper.StateId [use Id]
** StashUi's currency tab API [use StashUi.CurrencyTab API instead]
** Explorer.Delegate [use CurrentDelegate]

Loki.Game
[Part 1]
* Objects.Items revamps, optimizations, and caching.
** SocketableItem removed. [Use Item.ObjectTypeIsSocketableItem rather than 'Item is SocketableItem']
*** SocketedSkillGemsByLinks now returns a List rather than an IEnumerable.
** LinkedSocketColors now returns a List rather than an IEnumerable.
** Currency removed. [Use Item.ObjectTypeIsCurrency rather than 'Item is Currency']
** Armor removed. [Use Item.ObjectTypeIsArmor rather than 'Item is Armor']
** Map removed. [Use Item.ObjectTypeIsMap rather than 'Item is Map']
*** MonsterPackSize -> MapMonsterPackSize
*** ItemQuantity -> MapItemQuantity
*** ItemRarity -> MapItemRarity
*** RawItemRarity -> MapRawItemRarity
*** RawItemQuantity -> MapRawItemQuantity
*** Tier -> MapTier
*** Level -> MapLevel
*** Area -> MapArea
** Shield removed. [Use Item.ObjectTypeIsShield rather than 'Item is Shield']
** SealedProphecy removed. [Use Item.ObjectTypeIsSealedProphecy rather than 'Item is SealedProphecy']
*** Difficulty -> ProphecyDifficulty
** Weapon removed. [Use Item.ObjectTypeIsWeapon rather than 'Item is Weapon']
** Flask removed. [Use Item.ObjectTypeIsFlask rather than 'Item is Flask']
** Leaguestone removed. [Use Item.ObjectTypeIsLeaguestone rather than 'Item is Leaguestone']
** SkillGem removed. [Use Item.ObjectTypeIsSkillGem rather than 'Item is SkillGem']
*** GemTypes now returns a List rather than an IEnumerable.
*** GetAttributeRequirements now returns a bool if the function was performed or not.
*** Level -> SkillGemLevel
[Part 2]
* ObjectTypeIsXX removed. 
** Check components for being null to know if an Item is of a specific type (works with almost all except Leaguestone and Currency).
*** For Currency, there is no true type to begin with, so just check Rarity.
*** For Leaguestones, there is no true type to begin with, so use metadata or class from BaseItemTypes.
*** For reference:
**** ObjectTypeIsSocketableItem = Components.SocketsComponent != null;
**** ObjectTypeIsArmor = Components.ArmorComponent != null;
**** ObjectTypeIsMap = Components.MapComponent != null;
**** ObjectTypeIsShield = Components.ShieldComponent != null;
**** ObjectTypeIsSealedProphecy = Components.ProphecyComponent != null;
**** ObjectTypeIsWeapon = Components.WeaponComponent != null;
**** ObjectTypeIsFlask = Components.FlaskComponent != null;
**** ObjectTypeIsSkillGem = Components.SkillGemComponent != null;
**** ObjectTypeIsLeaguestone = HasMetadataFlag(MetadataFlags.Leaguestones);
**** ObjectTypeIsCurrency = Rarity == Rarity.Currency;
* New enum based metadata processing added to replace old string based processing system.
** 'IsXXType' type properties removed. Use 'HasMetadataFlags' instead (see the thread for all details).
*** IsHelmetType -> HasMetadataFlags(MetadataFlags.Helmets)
** HasMetadataFlags / AnyMetadataFlags functions support batch processing enums for convenience and cleaner code.
[Part 3]
* CompositeItemType integrated into Item from EXtensions.
[Part 4]
* Skill ToString updated to cover most of the object for debugging any skill related issues.

Loki.Bot
* DefaultItemEvaluator is now an empty item evaluator rather than having some default pickup rules.
* IPlayerMover updates.
* DefaultPlayerMover updates.
* IMandatory removed as it's no longer needed.
* IPlugin no longer forces an implementation of IRunnable
* IRunnable split into two new interfaces
* ITickEvents.Tick to receive Tick events
* IStartStopEvents.Start/Stop to receive Start/Stop events
* ILogic split into ILogicProvider and IMessageHandler; IBot, IRoutine, IPlugin, and ITask affected.
* ITask.Run interface function added to hold main task logic instead of relying on "task_execute".
* IContent added. This is basically an IPlugin without IEnableable.
* TaskManager.Execute -> SendMessage to handle Message objects.
* TaskManager.Logic -> ProvideLogic to handle Logic objects.
* TaskManager.Run added to run the main task function.
* ThirdPartyInstance.ContentInstances added.
* Utility.BroadcastEventExecute -> BroadcastMessage
* Utility.BroadcastEventLogic -> BroadcastLogicRequest
* PluginManager now allows plugins that don't implement IStartStopEvents to be enabled/disabled at runtime
* ThirdPartyLoader will now fail to load content that is missing files referenced in their FileList.

Exilebuddy
* ContentManager is now intitialized before BotManager.
* Settings window updates for IContent support and the layout changes.
* Settings window now allows plugins that don't implement IStartStopEvents to be enabled/disabled at runtime

Bots / Plugins / Routines
* "Event" model updated. Old code that used Logic for events now uses Messages.
** For example: "core_player_died_event" is now handled in IMessageHandler.Message.
* Tasks updated to new model. For most tasks this means:
** ILogicProvider.Logic added as an empty coroutine that returns LogicResult.Unprovided.
** IMessageHandler.Message added as an empty function that returns MessageResult.Unprocessed;
** The old Logic(string type, params dynamic[] param) coroutine is now just Run()
*** If the coroutine only checked for "task_execute", then the logic can stay as-is.
*** Otherwise, the non-"task_execute" logic now goes in ILogicProvider.Logic.
*** Exception: Tasks that run via a second TaskManager can be setup differently - see AutoPassives, Monoliths, GemLeveler, and Breaches as an example.
* Content compiling updates:
** Content can now include Properties\AssemblyInfo.cs.
** AssemblyVersion should be set as so: [assembly: AssemblyVersion("1.0.*")]
** AssemblyVersion should have the following: [assembly: AssemblyKeyFile("ThirdParty.snk")]
** Content is now Strong Name Signed via ThirdParty.snk.
** ThirdParty.snk can be copied into local projects so code compiles on your side.
* Existing plugins now only implement the relevant ITickEvents /IStartStopEvents interfaces.
* Content does not need to log Start/Tick/Stop/Enable/Disable/Initialize/Deinitialize events anymore, these are taken care of on the bot side for consistency.
* CommonEvents now exists for Legacy and Default content as two separate content providers that are only Ticked by bot bases that use them.