﻿These files are for contributing localizations to Exilebuddy.

*DO NOT* change anything inside "Localization.resx"/"Localization.Designer.cs". This is the master define file for all localizations.

Use the program "ResEx": https://resex.codeplex.com/ to edit the localization(s) of your choice. It's really simple to use and free.

Only change/submit localization changes that you were assigned to. 

If you notice an issue in another localization file, please simply post on the forums. This is to help reduce files getting out of sync between different contributors.

Each bot update will contain the latest localization files. Please be sure to work from clean copies each time in case new things are added.