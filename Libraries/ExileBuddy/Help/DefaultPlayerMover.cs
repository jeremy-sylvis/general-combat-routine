﻿using System.Diagnostics;
using System.Linq;
using log4net;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;
using System.Drawing;
using Loki.Game.Objects;

namespace Loki.Bot
{
	/// <summary>
	/// The default player mover.
	/// </summary>
	public class DefaultPlayerMover : IPlayerMover
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();
		private PathfindingCommand _cmd;
		private readonly Stopwatch _sw = new Stopwatch();
		private bool _doAdjustments = false;
		private LokiPoe.ConfigManager.NetworkingType _networkingMode = LokiPoe.ConfigManager.NetworkingType.Unknown;
		//private IndexedList<Vector2i> _originalPath;
		private int _moveRange = 13;
		private int _pathRefreshRate = 1000;
		private int _singleUseDistance = 25;
		private bool _avoidWallHugging = true;
		private bool _avoidDoorwayTraps = false;

		/// <summary>
		/// These are areas that always have issues with stock pathfinding, so adjustments will be made.
		/// </summary>
		private readonly string[] _forcedAdjustmentAreas = new[]
		{
			"The City of Sarn",
			"The Slums",
		};

		private LokiPoe.TerrainDataEntry[,] _tgts;
		private uint _tgtSeed;

		private LokiPoe.TerrainDataEntry TgtUnderPlayer
		{
			get
			{
				var myPos = LokiPoe.LocalData.MyPosition;
				return _tgts[myPos.X/23, myPos.Y/23];
			}
		}
		
		/// <summary>
		/// Determines if the given point is inside the polygon
		/// </summary>
		/// <param name="polygon">the vertices of polygon</param>
		/// <param name="testPoint">the given point</param>
		/// <returns>true if the point is inside the polygon; otherwise, false</returns>
		public static bool IsPointInPolygon4(PointF[] polygon, PointF testPoint)
		{
			bool result = false;
			int j = polygon.Count() - 1;
			for (int i = 0; i < polygon.Count(); i++)
			{
				if (polygon[i].Y < testPoint.Y && polygon[j].Y >= testPoint.Y || polygon[j].Y < testPoint.Y && polygon[i].Y >= testPoint.Y)
				{
					if (polygon[i].X + (testPoint.Y - polygon[i].Y) / (polygon[j].Y - polygon[i].Y) * (polygon[j].X - polygon[i].X) < testPoint.X)
					{
						result = !result;
					}
				}
				j = i;
			}
			return result;
		}

		private bool _waitingForTrap;
		private int _trapId;

		/// <summary>
		/// Attempts to move towards a position. This function will perform pathfinding logic and take into consideration move distance
		/// to try and smoothly move towards a point.
		/// </summary>
		/// <param name="position">The position to move towards.</param>
		/// <param name="user">A user object passed.</param>
		/// <returns>true if the position was moved towards, and false if there was a pathfinding error.</returns>
		public bool MoveTowards(Vector2i position, params dynamic[] user)
		{
			var myPosition = LokiPoe.MyPosition;

			if (_networkingMode == LokiPoe.ConfigManager.NetworkingType.Predictive)
			{
				// Generate new paths in predictive more frequently to avoid back and forth issues from the new movement model
				_pathRefreshRate = 16;
			}
			else
			{
				_pathRefreshRate = 1000;
			}

			if (
				_cmd == null || // No command yet
				_cmd.Path == null ||
				_cmd.EndPoint != position || // Moving to a new position
				LokiPoe.CurrentWorldArea.IsTown || // In town, always generate new paths
				(_sw.IsRunning && _sw.ElapsedMilliseconds > _pathRefreshRate) || // New paths on interval
				_cmd.Path.Count <= 2 || // Not enough points
				_cmd.Path.All(p => myPosition.Distance(p) > 7))
				// Try and find a better path to follow since we're off course
			{
				_cmd = new PathfindingCommand(myPosition, position, 3, _avoidWallHugging);
				if (!ExilePather.FindPath(ref _cmd))
				{
					_sw.Restart();
					Log.ErrorFormat("[DefaultPlayerMover::MoveTowards] ExilePather.FindPath failed from {0} to {1}.",
						myPosition, position);
					return false;
				}
				//Log.InfoFormat("[DefaultPlayerMover::MoveTowards] Finding new path.");
				_sw.Restart();
				//_originalPath = new IndexedList<Vector2i>(_cmd.Path);
			}

			// Eliminate points until we find one within a good moving range.
			while (_cmd.Path.Count > 1)
			{
				if (_cmd.Path[0].Distance(myPosition) < _moveRange)
				{
					_cmd.Path.RemoveAt(0);
				}
				else
				{
					break;
				}
			}

			var point = _cmd.Path[0];

			point += new Vector2i(LokiPoe.Random.Next(-2, 3), LokiPoe.Random.Next(-2, 3));

			var cwa = LokiPoe.CurrentWorldArea;
			if (!cwa.IsTown && !cwa.IsHideoutArea && (_doAdjustments || _forcedAdjustmentAreas.Contains(cwa.Name)))
			{
				var negX = 0;
				var posX = 0;

				var tmp1 = point;
				var tmp2 = point;

				for (var i = 0; i < 10; i++)
				{
					tmp1.X--;
					if (!ExilePather.IsWalkable(tmp1))
					{
						negX++;
					}

					tmp2.X++;
					if (!ExilePather.IsWalkable(tmp2))
					{
						posX++;
					}
				}

				if (negX > 5 && posX == 0)
				{
					point.X += 15;
					Log.InfoFormat("[DefaultPlayerMover::MoveTowards] Adjustments being made!");
					_cmd.Path[0] = point;
				}
				else if (posX > 5 && negX == 0)
				{
					point.X -= 15;
					Log.InfoFormat("[DefaultPlayerMover::MoveTowards] Adjustments being made!");
					_cmd.Path[0] = point;
				}
			}

			// Le sigh...
			if (cwa.IsTown && cwa.Act == 3)
			{
				var seed = LokiPoe.LocalData.AreaHash;
				if (_tgtSeed != seed || _tgts == null)
				{
					Log.InfoFormat("[DefaultPlayerMover::MoveTowards] Now building TGT info.");
					_tgts = LokiPoe.TerrainData.TgtEntries;
					_tgtSeed = seed;
				}
				if (TgtUnderPlayer.TgtName.Equals("Art/Models/Terrain/Act3Town/Act3_town_01_01_c16r7.tgt"))
				{
					Log.InfoFormat("[DefaultPlayerMover::MoveTowards] Act 3 Town force adjustment being made!");
					point.Y += 5;
				}
			}

			var move = LokiPoe.InGameState.SkillBarHud.LastBoundMoveSkill;
			if (move == null)
			{
				Log.ErrorFormat("[DefaultPlayerMover::MoveTowards] Please assign the \"Move\" skill to your skillbar!");
				return false;
			}

			if (_avoidDoorwayTraps)
			{
				var traps = LokiPoe.ObjectManager.Objects.Where(d => d.IsLabyrinthRollerTrap).ToList();
				var seenTrap = false;
				foreach (var trap in traps)
				{
					var tb = trap as TriggerableBlockage;
					if (tb == null)
						continue;

					if (_waitingForTrap)
					{
						if (trap.Id == _trapId)
						{
							seenTrap = true;
							if (tb.IsOpened)
							{
								_waitingForTrap = false;
								_trapId = 0;
								break;
							}
						}
						else
						{
							continue;
						}
					}

					if(tb.Distance > 25)
						continue;

					// If the trap is opened, not blocking, ignore it.
					if (tb.IsOpened)
					{
						continue;
					}

					// Otherwise, see if the trap lies in between where we're moving towards.
					var start = new Vector2i(trap.Position.X - 20, trap.Position.Y);
					var end = new Vector2i(trap.Position.X + 20, trap.Position.Y);
					var poly = new PointF[]
					{
						new PointF(start.X, start.Y + 20), new PointF(start.X, start.Y - 20), new PointF(end.X, end.Y + 20), new PointF(end.X, end.Y - 20)
					};
					if (IsPointInPolygon4(poly, new PointF(point.X, point.Y)))
					{
						Log.WarnFormat("Waiting for a trap...");
						LokiPoe.ProcessHookManager.ClearAllKeyStates();
						LokiPoe.InGameState.SkillBarHud.UseAt(move.Slots.Last(), false, myPosition.GetPointAtDistanceBeforeThis(trap.Position, 10));

						_waitingForTrap = true;
						_trapId = trap.Id;

						return true;
					}
				}
				if (_waitingForTrap)
				{
					if (seenTrap)
					{
						return true;
					}

					_waitingForTrap = false;
					_trapId = 0;
					Log.WarnFormat("The trap to wait for was not seen.");
				}
			}

			if ((LokiPoe.ProcessHookManager.GetKeyState(move.BoundKeys.Last()) & 0x8000) != 0 &&
				LokiPoe.Me.HasCurrentAction)
			{
				if (myPosition.Distance(position) < _singleUseDistance)
				{
					LokiPoe.ProcessHookManager.ClearAllKeyStates();
					LokiPoe.InGameState.SkillBarHud.UseAt(move.Slots.Last(), false, point);
					//Log.WarnFormat("[SkillBarHud.UseAt]");
				}
				else
				{
					LokiPoe.Input.SetMousePos(point, false);
					//Log.WarnFormat("[Input.SetMousePos]");
				}
			}
			else
			{
				LokiPoe.ProcessHookManager.ClearAllKeyStates();
				if (myPosition.Distance(position) < _singleUseDistance)
				{
					LokiPoe.InGameState.SkillBarHud.UseAt(move.Slots.Last(), false, point);
					//Log.WarnFormat("[SkillBarHud.UseAt]");
				}
				else
				{
					LokiPoe.InGameState.SkillBarHud.BeginUseAt(move.Slots.Last(), false, point);
					//Log.WarnFormat("[BeginUseAt]");
				}
			}

			return true;
		}

		#region Implementation of IMessageHandler

		/// <summary>
		/// Implements logic to handle a message passed through the system.
		/// </summary>
		/// <param name="message">The message to be processed.</param>
		/// <returns>A tuple of a MessageResult and object.</returns>
		public MessageResult Message(Message message)
		{
			if (message.Id == "SetMoveRange")
			{
				_moveRange = message.GetInput<int>();
				Log.InfoFormat("[DefaultPlayerMover::Message] {0} => {1}", message.Id, _moveRange);
				return MessageResult.Processed;
			}

			if (message.Id == "GetMoveRange")
			{
				message.AddOutput(this, _moveRange);
				return MessageResult.Processed;
			}

			if (message.Id == "SetSingleUseDistance")
			{
				_singleUseDistance = message.GetInput<int>();
				Log.InfoFormat("[DefaultPlayerMover::Message] {0} => {1}", message.Id, _singleUseDistance);
				return MessageResult.Processed;
			}

			if (message.Id == "GetSingleUseDistance")
			{
				message.AddOutput(this, _singleUseDistance);
				return MessageResult.Processed;
			}

			if (message.Id == "SetAvoidWallHugging")
			{
				_avoidWallHugging = message.GetInput<bool>();
				Log.InfoFormat("[DefaultPlayerMover::Message] {0} => {1}", message.Id, _avoidWallHugging);
				return MessageResult.Processed;
			}

			if (message.Id == "GetAvoidWallHugging")
			{
				message.AddOutput(this, _avoidWallHugging);
				return MessageResult.Processed;
			}

			if (message.Id == "SetAvoidWallHugging")
			{
				_avoidWallHugging = message.GetInput<bool>();
				Log.InfoFormat("[DefaultPlayerMover::Message] {0} => {1}", message.Id, _avoidWallHugging);
				return MessageResult.Processed;
			}

			if (message.Id == "GetAvoidDoorwayTraps")
			{
				message.AddOutput(this, _avoidDoorwayTraps);
				return MessageResult.Processed;
			}

			if (message.Id == "SetAvoidDoorwayTraps")
			{
				_avoidDoorwayTraps = message.GetInput<bool>();
				Log.InfoFormat("[DefaultPlayerMover::Message] {0} => {1}", message.Id, _avoidDoorwayTraps);
				return MessageResult.Processed;
			}

			if (message.Id == "GetDoAdjustments")
			{
				message.AddOutput(this, _doAdjustments);
				return MessageResult.Processed;
			}

			if (message.Id == "GetPathfindingCommand")
			{
				message.AddOutput(this, _cmd);
				return MessageResult.Processed;
			}

			if (message.Id == "SetNetworkingMode")
			{
				_networkingMode = message.GetInput<LokiPoe.ConfigManager.NetworkingType>();
				return MessageResult.Processed;
			}

			return MessageResult.Unprocessed;
		}

		#endregion
	}
}