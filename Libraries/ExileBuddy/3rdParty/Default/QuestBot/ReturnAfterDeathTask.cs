﻿using System.Threading.Tasks;
using Default.EXtensions;
using Default.EXtensions.CachedObjects;
using Loki.Bot;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace Default.QuestBot
{
    public class ReturnAfterDeathTask : ITask
    {
        private CachedObject _transition;

        public async Task<bool> Run()
        {
            if (_transition == null)
                return false;

            var pos = _transition.Position;
            if (pos.IsFar)
            {
                if (!pos.TryCome())
                {
                    GlobalLog.Debug("[ReturnAfterDeathTask] Transition is unwalkable. Skipping this task.");
                    _transition = null;
                }
                return true;
            }
            var transitionObj = (AreaTransition) _transition.Object;
            if (!await PlayerAction.TakeTransition(transitionObj))
            {
                ErrorManager.ReportError();
                return true;
            }
            _transition = null;
            return true;
        }

        public MessageResult Message(Message message)
        {
            if (message.Id == Events.Messages.PlayerResurrected)
            {
                _transition = null;

                var area = World.CurrentArea;

                if (area.IsTown || area == World.Act4.DaressoDream || area == World.Act4.BellyOfBeast2 || area == World.Act4.Harvest)
                {
                    GlobalLog.Debug($"[ReturnAfterDeathTask] Skipping this task because area is {area.Name}.");
                    return MessageResult.Processed;
                }

                var transition = LokiPoe.ObjectManager.Objects
                    .Closest<AreaTransition>(a => a.TransitionType == TransitionTypes.Local && a.Distance <= 50);

                if (transition == null)
                {
                    GlobalLog.Debug("[ReturnAfterDeathTask] There is no local area transition nearby. Skipping this task.");
                    return MessageResult.Processed;
                }
                _transition = new CachedObject(transition);
                GlobalLog.Debug($"[ReturnAfterDeathTask] Detected local transition {_transition.Position}");
                return MessageResult.Processed;
            }
            return MessageResult.Unprocessed;
        }

        #region Unused interface methods

        public async Task<LogicResult> Logic(Logic logic)
        {
            return LogicResult.Unprovided;
        }

        public void Start()
        {
        }

        public void Stop()
        {
        }

        public void Tick()
        {
        }

        public string Name => "ReturnAfterDeathTask";
        public string Description => "Task for taking closest local transition after death.";
        public string Author => "ExVault";
        public string Version => "1.0";

        #endregion
    }
}