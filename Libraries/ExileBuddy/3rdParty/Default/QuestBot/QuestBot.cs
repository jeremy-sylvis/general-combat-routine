﻿using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Buddy.Coroutines;
using Default.EXtensions;
using Default.EXtensions.CommonTasks;
using Default.EXtensions.Global;
using log4net;
using Loki.Bot;
using Loki.Bot.Pathfinding;
using Loki.Common;
using Loki.Game;
using settings = Default.QuestBot.Settings;
using UserControl = System.Windows.Controls.UserControl;

namespace Default.QuestBot
{
    public class QuestBot : IBot, ITaskManagerHolder
    {
        private static readonly ILog Log = Logger.GetLoggerInstanceForType();

        private Gui _gui;
        private Coroutine _coroutine;

        private readonly TaskManager _taskManager = new TaskManager();

        public void Start()
        {
            ItemEvaluator.Instance = DefaultItemEvaluator.Instance;
            Explorer.CurrentDelegate = user => CombatAreaCache.Current.Explorer.BasicExplorer;
            ComplexExplorer.SetSettingsDelegate(QuestBotExploration, "QuestBot");

            // Cache all bound keys.
            LokiPoe.Input.Binding.Update();

            // Reset the default MsBetweenTicks on start.
            Log.Debug($"[Start] MsBetweenTicks: {BotManager.MsBetweenTicks}.");
            Log.Debug($"[Start] NetworkingMode: {LokiPoe.ConfigManager.NetworkingMode}.");
            Log.Debug($"[Start] KeyPickup: {LokiPoe.ConfigManager.KeyPickup}.");
            Log.Debug($"[Start] IsAutoEquipEnabled: {LokiPoe.ConfigManager.IsAutoEquipEnabled}.");
            Log.Debug($"[Start] PlayerMover.Instance: {PlayerMover.Instance.GetType()}.");

            // Since this bot will be performing client actions, we need to enable the process hook manager.
            LokiPoe.ProcessHookManager.Enable();

            _coroutine = null;

            ExilePather.Reload();

            _taskManager.Reset();

            AddTasks();

            Events.Start();
            PluginManager.Start();
            RoutineManager.Start();
            _taskManager.Start();

            foreach (var plugin in PluginManager.EnabledPlugins)
            {
                Log.Debug($"[Start] The plugin {plugin.Name} is enabled.");
            }

            Log.Debug($"[Start] PlayerMover.Instance: {PlayerMover.Instance.GetType()}.");

            PlayerMover.Message(new Loki.Bot.Message("SetNetworkingMode", this, LokiPoe.ConfigManager.NetworkingMode)); // TODO: Replace

            if (ExilePather.BlockTrialOfAscendancy == FeatureEnum.Unset)
            {
                ExilePather.BlockTrialOfAscendancy = FeatureEnum.Enabled;
            }
        }

        public void Tick()
        {
            if (_coroutine == null)
            {
                _coroutine = new Coroutine(() => MainCoroutine());
            }

            ExilePather.Reload();

            Events.Tick();
            CombatAreaCache.Tick();
            _taskManager.Tick();
            PluginManager.Tick();
            RoutineManager.Tick();
            StuckDetection.Tick();

            // Check to see if the coroutine is finished. If it is, stop the bot.
            if (_coroutine.IsFinished)
            {
                Log.Debug($"The bot coroutine has finished in a state of {_coroutine.Status}");
                BotManager.Stop();
                return;
            }

            try
            {
                _coroutine.Resume();
            }
            catch
            {
                var c = _coroutine;
                _coroutine = null;
                c.Dispose();
                throw;
            }
        }

        public void Stop()
        {
            _taskManager.Stop();
            PluginManager.Stop();
            RoutineManager.Stop();

            // When the bot is stopped, we want to remove the process hook manager.
            LokiPoe.ProcessHookManager.Disable();

            // Cleanup the coroutine.
            if (_coroutine != null)
            {
                _coroutine.Dispose();
                _coroutine = null;
            }
        }

        private async Task MainCoroutine()
        {
            while (true)
            {
                if (LokiPoe.IsInLoginScreen)
                {
                    // Offload auto login logic to a plugin.
                    var logic = new Logic("hook_login_screen", this);
                    foreach (var plugin in PluginManager.EnabledPlugins)
                    {
                        if (await plugin.Logic(logic) == LogicResult.Provided)
                            break;
                    }
                }
                else if (LokiPoe.IsInCharacterSelectionScreen)
                {
                    // Offload character selection logic to a plugin.
                    var logic = new Logic("hook_character_selection", this);
                    foreach (var plugin in PluginManager.EnabledPlugins)
                    {
                        if (await plugin.Logic(logic) == LogicResult.Provided)
                            break;
                    }
                }
                else if (LokiPoe.IsInGame)
                {
                    // Wait for game pause
                    if (LokiPoe.InstanceInfo.IsGamePaused)
                    {
                        Log.Debug("Waiting for game pause");
                        await Wait.StuckDetectionSleep(200);
                    }
                    // Resurrect character if it is dead
                    else if (LokiPoe.Me.IsDead)
                    {
                        await ResurrectionLogic.Execute();
                    }
                    // What the bot does now is up to the registered tasks.
                    else
                    {
                        await _taskManager.Run(TaskGroup.Enabled, RunBehavior.UntilHandled);
                    }
                }
                else
                {
                    // Most likely in a loading screen, which will cause us to block on the executor, 
                    // but just in case we hit something else that would cause us to execute...
                    await Coroutine.Sleep(1000);
                    continue;
                }

                // End of the tick.
                await Coroutine.Yield();
            }
            // ReSharper disable once FunctionNeverReturns
        }

        public MessageResult Message(Loki.Bot.Message message)
        {
            var handled = false;
            var id = message.Id;

            if (id == BotStructure.GetTaskManagerMessage)
            {
                message.AddOutput(this, _taskManager);
                handled = true;
            }
            else if (id == Events.Messages.IngameBotStart)
            {
                CheckMoveSkill();
                handled = true;
            }
            else if (message.Id == Events.Messages.PlayerDied)
            {
                int deathCount = message.GetInput<int>();
                GrindingHandler.OnPlayerDied(deathCount);
                handled = true;
            }

            Events.FireEventsFromMessage(message);

            var res = _taskManager.SendMessage(TaskGroup.Enabled, message);
            if (res == MessageResult.Processed)
                handled = true;

            return handled ? MessageResult.Processed : MessageResult.Unprocessed;
        }

        public async Task<LogicResult> Logic(Logic logic)
        {
            return await _taskManager.ProvideLogic(TaskGroup.Enabled, RunBehavior.UntilHandled, logic);
        }

        public TaskManager GetTaskManager()
        {
            return _taskManager;
        }

        public void Initialize()
        {
            BotManager.OnBotChanged += BotManagerOnOnBotChanged;
        }

        public void Deinitialize()
        {
            BotManager.OnBotChanged -= BotManagerOnOnBotChanged;
        }

        private void BotManagerOnOnBotChanged(object sender, BotChangedEventArgs botChangedEventArgs)
        {
            if (botChangedEventArgs.New == this)
            {
                ItemEvaluator.Instance = DefaultItemEvaluator.Instance;
            }
        }

        private void AddTasks()
        {
            _taskManager.Add(new ClearCursorTask());
            _taskManager.Add(new LeaveAreaTask());
            _taskManager.Add(new HandleBlockingChestsTask());
            _taskManager.Add(new HandleBlockingObjectTask());
            _taskManager.Add(new CombatTask(50));
            _taskManager.Add(new ReturnAfterDeathTask());
            _taskManager.Add(new PostCombatHookTask());
            _taskManager.Add(new LootItemTask());
            _taskManager.Add(new OpenChestTask());
            _taskManager.Add(new CombatTask(-1));
            _taskManager.Add(new IdTask());
            _taskManager.Add(new SellTask());
            _taskManager.Add(new StashTask());
            _taskManager.Add(new SortInventoryTask());
            _taskManager.Add(new VendorTask());
            _taskManager.Add(new ReturnAfterTownrunTask());
            _taskManager.Add(new OpenWaypointTask());
            _taskManager.Add(new QuestTask());
            _taskManager.Add(new FallbackTask());
        }

        private static ExplorationSettings QuestBotExploration()
        {
            var areaId = World.CurrentArea.Id;

            if (areaId == World.Act4.GrandArena.Id)
                return new ExplorationSettings(false, true, true, tileSeenRadius: 3);

            if (areaId == World.Act6.PrisonerGate.Id)
                return new ExplorationSettings(false, true);

            if (areaId == World.Act7.Crypt.Id)
                return new ExplorationSettings(false, true);

            if (areaId == World.Act7.MaligaroSanctum.Id)
                return new ExplorationSettings(false, true, openPortals: true);

            return new ExplorationSettings();
        }

        private static void CheckMoveSkill()
        {
            var skill = LokiPoe.InGameState.SkillBarHud.LastBoundMoveSkill;
            if (skill == null || skill.BoundKeys.Last() == Keys.LButton)
            {
                Log.Error("The \"Move\" skill should be present on a skillbar and bound to non left mouse button.");
                BotManager.Stop();
            }
        }

        public string Name => "QuestBot";
        public string Description => "Bot for doing quests.";
        public string Author => "ExVault";
        public string Version => "2.0";
        public JsonSettings Settings => settings.Instance;
        public UserControl Control => _gui ?? (_gui = new Gui());
        public override string ToString() => $"{Name}: {Description}";
    }
}