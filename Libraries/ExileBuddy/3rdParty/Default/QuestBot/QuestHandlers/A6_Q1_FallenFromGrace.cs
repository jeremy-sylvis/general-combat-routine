﻿using System.Threading.Tasks;
using Default.EXtensions;
using Default.EXtensions.Global;
using Default.EXtensions.Positions;
using Loki.Game;
using Loki.Game.Objects;

namespace Default.QuestBot.QuestHandlers
{
    public static class A6_Q1_FallenFromGrace
    {
        private const int FinishedStateMinimum = 2;
        private static bool _finished;

        private static readonly WalkablePosition LillyShipPos = new WalkablePosition("Lilly Roth", 229, 158);

        private static Npc LillyRoth => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Lilly_Roth)
            .FirstOrDefault<Npc>();

        public static void Tick()
        {
            _finished = QuestManager.GetStateInaccurate(Quests.FallenFromGrace) <= FinishedStateMinimum;
        }

        public static async Task<bool> ClearStrand()
        {
            if (_finished)
                return false;

            if (World.Act6.TwilightStrand.IsCurrentArea)
            {
                if (await TrackMobLogic.Execute())
                    return true;

                if (!await CombatAreaCache.Current.Explorer.Execute())
                {
                    if (QuestManager.GetState(Quests.FallenFromGrace) <= FinishedStateMinimum)
                        return false;

                    GlobalLog.Error("[ClearTwilightStrand] Twilight Strand is fully explored but not all monsters were killed. Now going to create a new Twilight Strand instance.");

                    Travel.RequestNewInstance(World.Act6.TwilightStrand);

                    if (!await PlayerAction.TpToTown())
                        ErrorManager.ReportError();
                }
                return true;
            }
            await Travel.To(World.Act6.TwilightStrand);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            if (World.Act6.LioneyeWatch.IsCurrentArea)
            {
                if (Helpers.PlayerHasQuestItem(QuestItemMetadata.BookFallenFromGrace))
                {
                    if (!await Helpers.UseQuestItem(QuestItemMetadata.BookFallenFromGrace))
                        ErrorManager.ReportError();

                    return false;
                }

                await LillyShipPos.ComeAtOnce();

                var lilly = LillyRoth;
                if (lilly == null)
                {
                    GlobalLog.Error("[FallenFromGrace] Unexpected error. There is no Lilly Roth near ship.");
                    ErrorManager.ReportCriticalError();
                    return true;
                }

                if (!await lilly.AsTownNpc().TakeReward(null, "Twilight Strand Reward"))
                    ErrorManager.ReportError();

                return false;
            }
            await Travel.To(World.Act6.LioneyeWatch);
            return true;
        }
    }
}