﻿using System.Threading.Tasks;
using Loki.Bot;
using Loki.Game;

namespace Default.EXtensions.Global
{
    public static class ResurrectionLogic
    {
        public static async Task Execute()
        {
            if (!await Resurrect(true))
            {
                if (!await Resurrect(false))
                {
                    GlobalLog.Error("[ResurrectTask] Resurrection failed. Now going to logout.");
                    if (!await PlayerAction.Logout())
                    {
                        GlobalLog.Error("[ResurrectTask] Logout failed. Now stopping the bot.");
                        BotManager.Stop();
                        return;
                    }
                }
            }
            GlobalLog.Info("[Events] Player resurrected.");
            Utility.BroadcastMessage(null, Events.Messages.PlayerResurrected);
        }

        private static async Task<bool> Resurrect(bool toCheckpoint, int attempts = 3)
        {
            GlobalLog.Debug($"[ResurrectTask] Now going to resurrect to {(toCheckpoint ? "checkpoint" : "town")}.");

            LokiPoe.ProcessHookManager.Reset();

            for (int i = 1; i <= attempts; ++i)
            {
                await Wait.SleepSafe(1000, 1500);

                var err = toCheckpoint
                    ? LokiPoe.InGameState.ResurrectPanel.ResurrectToCheckPoint()
                    : LokiPoe.InGameState.ResurrectPanel.ResurrectToTown();

                if (err != LokiPoe.InGameState.ResurrectResult.None)
                {
                    GlobalLog.Error($"[ResurrectTask] Fail to resurrect. Error: \"{err}\".");
                    continue;
                }
                if (await Wait.For(() => !LokiPoe.Me.IsDead, "resurrection", 200, 5000))
                {
                    GlobalLog.Debug("[ResurrectTask] Player has been successfully resurrected.");
                    await Wait.SleepSafe(250);
                    return true;
                }
            }
            GlobalLog.Error("[ResurrectTask] All resurrection attempts have been spent.");
            return false;
        }
    }
}