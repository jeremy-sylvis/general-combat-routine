﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Default.EXtensions.CachedObjects;
using Default.EXtensions.Positions;
using Loki.Bot;
using Loki.Game;

namespace Default.EXtensions.Global
{
    public class ComplexExplorer
    {
        public const string LocalTransitionEnteredMessage = "explorer_local_transition_entered_event";
        public static event Action LocalTransitionEntered;
        public ExplorationSettings Settings { get; }
        public bool Finished { get; private set; }

        private const int MaxTransitionAttempts = 5;
        private static readonly Interval LogInterval = new Interval(1000);
        private static Func<ExplorationSettings> _settingsDelegate;

        private CachedTransition _transition;
        private WorldPosition _levelAnchor;
        private readonly GridExplorer _explorer;
        private readonly Dictionary<WorldPosition, GridExplorer> _explorers;

        // to prevent previous level explorer from ticking while we are taking transition to next level 
        private bool _disableTick;

        // to detect external exploration interruption (chicken, disconnect) in multilevel areas
        private WorldPosition _myLastPos;

        public GridExplorer BasicExplorer => Settings.BasicExploration ? _explorer : _explorers[_levelAnchor];

        public ComplexExplorer()
        {
            Settings = _settingsDelegate != null ? _settingsDelegate() : new ExplorationSettings();
            Settings.LogProperties();
            if (Settings.BasicExploration)
            {
                _explorer = CreateExplorer();
            }
            else
            {
                _explorers = new Dictionary<WorldPosition, GridExplorer>();
                InitNewLevel();
            }
        }

        public async Task<bool> Execute()
        {
            if (Finished) return false;

            if (Settings.BasicExploration)
            {
                if (!BasicExploration())
                    Finished = true;
            }
            else
            {
                if (!await ComplexExploration())
                    Finished = true;
            }
            return true;
        }

        private bool BasicExploration()
        {
            var explorer = BasicExplorer;

            if (!explorer.HasLocation) return false;

            var location = explorer.Location;
            if (LogInterval.Elapsed)
            {
                var distance = LokiPoe.MyPosition.Distance(location);
                var percent = Math.Round(explorer.PercentComplete, 1);
                GlobalLog.Debug($"[ComplexExplorer] Exploring to the location {location} ({distance}) [{percent} %].");
            }
            if (!PlayerMover.MoveTowards(location))
            {
                GlobalLog.Error($"[ComplexExplorer] MoveTowards failed for {location}. Adding this location to ignore list.");
                explorer.Ignore(location);
            }
            return true;
        }

        private async Task<bool> ComplexExploration()
        {
            if (_myLastPos.Distance >= 100 && !_myLastPos.PathExists)
            {
                GlobalLog.Error("[ComplexExplorer] Cannot pathfind to my last position. Now resetting current level and all visited transitions.");
                HandleExternalJump();
            }

            _myLastPos = new WorldPosition(LokiPoe.MyPosition);

            if (Settings.FastTransition)
            {
                if (_transition != null)
                {
                    await EnterTransition();
                    return true;
                }
                _transition = FrontTransition;
                if (_transition != null) return true;
            }
            if (!BasicExploration())
            {
                if (_transition != null)
                {
                    await EnterTransition();
                    return true;
                }
                _transition = FrontTransition;
                if (_transition == null)
                {
                    if (Settings.Backtracking)
                    {
                        _transition = BackTransition;
                        if (_transition != null) return true;
                    }
                    GlobalLog.Warn("[ComplexExplorer] Out of area transitions. Now finishing the exploration.");
                    return false;
                }
            }
            return true;
        }

        private async Task EnterTransition()
        {
            var pos = _transition.Position;
            if (!pos.Initialized)
            {
                if (!pos.Initialize())
                {
                    GlobalLog.Debug($"[ComplexExplorer] Marking {pos} as unwalkable.");
                    _transition.Unwalkable = true;
                    _transition = null;
                    return;
                }
            }
            if (pos.IsFar)
            {
                // Still have to check this due to possible external jumps
                if (!pos.TryCome())
                {
                    GlobalLog.Debug($"[ComplexExplorer] Fail to move to {pos}. Marking this transition as unwalkable.");
                    _transition.Unwalkable = true;
                    _transition = null;
                }
                return;
            }

            var transitionObj = _transition.Object;

            if (!transitionObj.IsTargetable)
            {
                if (transitionObj.Metadata.Contains("sarcophagus_transition"))
                {
                    if (await HandleSarcophagus() && transitionObj.Fresh().IsTargetable)
                        return;
                }
                if (_transition.InteractionAttempts >= MaxTransitionAttempts)
                {
                    GlobalLog.Error("[ComplexExplorer] Area transition did not become targetable. Now ignoring it.");
                    _transition.Ignored = true;
                    _transition = null;
                    return;
                }
                var attempts = ++_transition.InteractionAttempts;
                GlobalLog.Debug($"[ComplexExplorer] Waiting for \"{pos.Name}\" to become targetable ({attempts}/{MaxTransitionAttempts})");
                await Wait.SleepSafe(500);
                return;
            }

            _disableTick = true;
            if (!await PlayerAction.TakeTransition(transitionObj))
            {
                var attempts = ++_transition.InteractionAttempts;
                GlobalLog.Error($"[ComplexExplorer] Fail to enter {pos}. Attempt: {attempts}/{MaxTransitionAttempts}");
                if (attempts >= MaxTransitionAttempts)
                {
                    GlobalLog.Error("[ComplexExplorer] All attempts to enter an area transition have been spent. Now ignoring it.");
                    _transition.Ignored = true;
                    _transition = null;
                }
                else
                {
                    await Wait.SleepSafe(500);
                }         
            }
            else
            {
                PostEnter();

                GlobalLog.Info("[ComplexExplorer] LocalTransitionEntered event.");
                LocalTransitionEntered?.Invoke();
                Utility.BroadcastMessage(this, LocalTransitionEnteredMessage);

                if (Settings.OpenPortals)
                    await PlayerAction.CreateTownPortal();
            }
            _disableTick = false;
        }

        private void PostEnter()
        {
            _transition.Visited = true;
            _transition = null;
            ResetLevelAnchor();
            MarkBackTransition();
            Blacklist.Reset();
        }

        private static void MarkBackTransition()
        {
            CombatAreaCache.Tick(); //ensure that area transition list is updated

            var backTransition = CombatAreaCache.Current.AreaTransitions
                .Where(a => a.Type == TransitionType.Local && !a.LeadsBack && !a.Visited && a.Position.Distance <= 50)
                .OrderBy(a => a.Position.DistanceSqr)
                .FirstOrDefault();

            if (backTransition == null)
            {
                GlobalLog.Debug("[ComplexExplorer] No back transition detected.");
                return;
            }
            backTransition.LeadsBack = true;
            GlobalLog.Debug($"[ComplexExplorer] Marking {backTransition.Position} as back transition.");
        }

        private void HandleExternalJump()
        {
            _transition = null;
            foreach (var t in CombatAreaCache.Current.AreaTransitions)
            {
                t.Visited = false;
            }
            // Current explorer is ticking on a wrong level, reset it.
            _disableTick = true;
            BasicExplorer.Reset();
            ResetLevelAnchor();
            _disableTick = false;
        }

        private static async Task<bool> HandleSarcophagus()
        {
            var sarcophagus = LokiPoe.ObjectManager.Objects.Find(o => o.Metadata.Contains("sarcophagus_door"));
            if (sarcophagus == null)
            {
                GlobalLog.Error("[ComplexExplorer] There is no sarcophagus.");
                return false;
            }
            if (!sarcophagus.IsTargetable)
            {
                GlobalLog.Error("[ComplexExplorer] Sarcophagus is not targetable.");
                return false;
            }
            return await PlayerAction.Interact(sarcophagus, () => !sarcophagus.Fresh().IsTargetable, "sarcophagus interaction", 3000, 1);
        }

        private void ResetLevelAnchor()
        {
            var anchors = _explorers.Keys;
            foreach (var anchor in anchors)
            {
                if (anchor.PathExists)
                {
                    GlobalLog.Debug($"[ComplexExplorer] Resetting anchor point to existing one {anchor}");
                    _levelAnchor = anchor;
                    _myLastPos = new WorldPosition(LokiPoe.MyPosition);
                    return;
                }
            }
            InitNewLevel();
        }

        private void InitNewLevel()
        {
            _levelAnchor = new WorldPosition(LokiPoe.MyPosition);
            _myLastPos = _levelAnchor;
            _explorers.Add(_levelAnchor, CreateExplorer());
            GlobalLog.Debug($"[ComplexExplorer] Creating new level anchor {_levelAnchor}");
        }

        private GridExplorer CreateExplorer()
        {
            var explorer = new GridExplorer
            {
                AutoResetOnAreaChange = false,
                TileKnownRadius = Settings.TileKnownRadius,
                TileSeenRadius = Settings.TileSeenRadius
            };
            explorer.Start();
            return explorer;
        }

        public void Tick()
        {
            if (_disableTick)
                return;

            BasicExplorer.Tick();
        }

        public static void SetSettingsDelegate(Func<ExplorationSettings> settingsDelegate, string owner)
        {
            GlobalLog.Info($"[ComplexExplorer] Using {owner} exploration settings.");
            _settingsDelegate = settingsDelegate;
        }

        private static CachedTransition FrontTransition => CombatAreaCache.Current.AreaTransitions
            .Where(t => t.Type == TransitionType.Local && !t.Visited && !t.LeadsBack)
            .ClosestValid();

        private static CachedTransition BackTransition => CombatAreaCache.Current.AreaTransitions
            .Where(t => t.Type == TransitionType.Local && !t.Visited)
            .ClosestValid();
    }

    public class ExplorationSettings
    {
        public const int DefaultTileKnownRadius = 7;
        public const int DefaultTileSeenRadius = 4;

        public bool BasicExploration { get; set; }
        public bool FastTransition { get; set; }
        public bool Backtracking { get; set; }
        public bool OpenPortals { get; set; }
        public int TileKnownRadius { get; set; }
        public int TileSeenRadius { get; set; }

        public ExplorationSettings
        (
            bool basicExploration = true,
            bool fastTransition = false,
            bool backtracking = false,
            bool openPortals = false,
            int tileKnownRadius = DefaultTileKnownRadius,
            int tileSeenRadius = DefaultTileSeenRadius
        )
        {
            BasicExploration = basicExploration;
            FastTransition = fastTransition;
            Backtracking = backtracking;
            OpenPortals = openPortals;
            TileKnownRadius = tileKnownRadius;
            TileSeenRadius = tileSeenRadius;
        }
    }
}