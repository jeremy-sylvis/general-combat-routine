﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Default.EXtensions.CachedObjects;
using Loki.Bot;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace Default.EXtensions.Global
{
    public class CombatAreaCache
    {
        private static readonly TimeSpan Lifetime = TimeSpan.FromMinutes(15);
        private static readonly Interval ScanInterval = new Interval(200);
        private static readonly Interval ItemScanInterval = new Interval(25);

        private static readonly Dictionary<uint, CombatAreaCache> Caches = new Dictionary<uint, CombatAreaCache>();

        public static CombatAreaCache Current
        {
            get
            {
                var hash = LokiPoe.LocalData.AreaHash;
                if (Caches.TryGetValue(hash, out var cache))
                {
                    cache._lastAccessTime.Restart();
                    return cache;
                }
                RemoveOldCaches();
                var newCache = new CombatAreaCache(hash);
                Caches.Add(hash, newCache);
                return newCache;
            }
        }

        public uint Hash { get; }
        public DatWorldAreaWrapper WorldArea { get; }
        public ComplexExplorer Explorer { get; }
        public int DeathCount { get; internal set; }
        public int StuckCount { get; internal set; }


        public readonly List<CachedWorldItem> Items = new List<CachedWorldItem>();
        public readonly List<CachedObject> Chests = new List<CachedObject>();
        public readonly List<CachedObject> SpecialChests = new List<CachedObject>();
        public readonly List<CachedStrongbox> Strongboxes = new List<CachedStrongbox>();
        public readonly List<CachedObject> Shrines = new List<CachedObject>();
        public readonly List<CachedObject> Monsters = new List<CachedObject>();
        public readonly List<CachedTransition> AreaTransitions = new List<CachedTransition>();
        public readonly ObjectDictionary Storage = new ObjectDictionary();

        private readonly HashSet<int> _processedObjects = new HashSet<int>();

        //keep this in a separate collection to reset on ItemEvaluatorRefresh
        private readonly HashSet<int> _processedItems = new HashSet<int>();

        private readonly Stopwatch _lastAccessTime;

        static CombatAreaCache()
        {
            ItemEvaluator.OnRefreshed += OnItemEvaluatorRefresh;
            ComplexExplorer.LocalTransitionEntered += OnLocalTransitionEntered;
        }

        private CombatAreaCache(uint hash)
        {
            GlobalLog.Info($"[CombatAreaCache] Creating cache for \"{World.CurrentArea.Name}\" (hash: {hash})");
            Hash = hash;
            WorldArea = World.CurrentArea;
            Explorer = new ComplexExplorer();
            _lastAccessTime = Stopwatch.StartNew();
        }

        private static void RemoveOldCaches()
        {
            var toRemove = Caches.Where(c => c.Value._lastAccessTime.Elapsed > Lifetime).Select(c => c.Value).ToList();
            foreach (var cache in toRemove)
            {
                GlobalLog.Info($"[CombatAreaCache] Removing cache for \"{cache.WorldArea.Name}\" (hash: {cache.Hash}). Last accessed {(int) cache._lastAccessTime.Elapsed.TotalMinutes} minutes ago.");
                Caches.Remove(cache.Hash);
            }
        }

        public static void Tick()
        {
            if (!LokiPoe.IsInGame || LokiPoe.Me.IsDead || !World.CurrentArea.IsCombatArea)
                return;

            Current.OnTick();
        }

        private void OnTick()
        {
            Current.Explorer.Tick();

            if (ItemScanInterval.Elapsed)
            {
                WorldItemScan();
            }

            if (ScanInterval.Elapsed)
            {
                foreach (var obj in LokiPoe.ObjectManager.Objects)
                {
                    var mob = obj as Monster;
                    if (mob != null)
                    {
                        ProcessMonster(mob);
                        continue;
                    }

                    var chest = obj as Chest;
                    if (chest != null)
                    {
                        if (IsSpecialChest(chest))
                        {
                            ProcessSpeacialChest(chest);
                            continue;
                        }
                        if (chest.IsStrongBox)
                        {
                            ProcessStrongbox(chest);
                            continue;
                        }
                        ProcessChest(chest);
                        continue;
                    }

                    var shrine = obj as Shrine;
                    if (shrine != null)
                    {
                        ProcessShrine(shrine);
                        continue;
                    }

                    var transition = obj as AreaTransition;
                    if (transition != null)
                    {
                        ProcessTransition(transition);
                    }
                }
                UpdateMonsters();
            }
        }

        private void WorldItemScan()
        {
            foreach (var obj in LokiPoe.ObjectManager.Objects)
            {
                var worldItem = obj as WorldItem;

                if (worldItem == null)
                    continue;

                if (worldItem.IsAllocatedToOther && DateTime.Now < worldItem.PublicTime)
                    continue;

                var id = worldItem.Id;
                if (_processedItems.Contains(id))
                    continue;

                var item = worldItem.Item;
                if (ItemEvaluator.Match(item, EvaluationType.PickUp))
                {
                    var pos = worldItem.WalkablePosition();
                    pos.Initialized = true; //disable walkable position searching for items
                    Items.Add(new CachedWorldItem(id, pos, item.Size, item.Rarity));
                }
                _processedItems.Add(id);
            }
        }

        private void ProcessMonster(Monster mob)
        {
            if (mob.IsDead || mob.Reaction != Reaction.Enemy)
                return;

            if (mob.Invincible && !mob.Metadata.Contains("Emerge"))
                return;

            var id = mob.Id;

            if (_processedObjects.Contains(id))
                return;

            if (SkipThisMob(mob))
            {
                _processedObjects.Add(id);
                return;
            }

            var pos = mob.WalkablePosition();
            pos.Initialized = true; //disable walkable position searching for monsters
            Monsters.Add(new CachedObject(id, pos));
            _processedObjects.Add(id);
        }

        private void ProcessChest(Chest chest)
        {
            if (chest.IsOpened || chest.IsLocked || chest.OpensOnDamage || !chest.IsTargetable)
                return;

            var id = chest.Id;
            if (_processedObjects.Contains(id))
                return;

            Chests.Add(new CachedObject(id, chest.WalkablePosition(5, 20)));
            _processedObjects.Add(id);
        }

        private void ProcessSpeacialChest(Chest chest)
        {
            if (chest.IsOpened || !chest.IsTargetable)
                return;

            var id = chest.Id;
            if (_processedObjects.Contains(id))
                return;

            var pos = chest.WalkablePosition();
            SpecialChests.Add(new CachedObject(id, pos));
            _processedObjects.Add(id);
            GlobalLog.Warn($"[CombatAreaCache] Registering {pos}");
        }

        private void ProcessStrongbox(Chest box)
        {
            if (box.IsOpened || box.IsLocked || !box.IsTargetable)
                return;

            var id = box.Id;
            if (_processedObjects.Contains(id))
                return;

            var pos = box.WalkablePosition();
            Strongboxes.Add(new CachedStrongbox(id, pos, box.Rarity));
            _processedObjects.Add(id);
            GlobalLog.Warn($"[CombatAreaCache] Registering {pos}");
        }

        private void ProcessShrine(Shrine shrine)
        {
            if (shrine.IsDeactivated || !shrine.IsTargetable || shrine.Name == "Divine Shrine")
                return;

            var id = shrine.Id;
            if (_processedObjects.Contains(id))
                return;

            var pos = shrine.WalkablePosition();
            Shrines.Add(new CachedObject(id, pos));
            _processedObjects.Add(id);
            GlobalLog.Warn($"[CombatAreaCache] Registering {pos}");
        }

        private void ProcessTransition(AreaTransition t)
        {
            var id = t.Id;
            if (_processedObjects.Contains(id))
                return;

            TransitionType type;
            var gggType = t.TransitionType;
            if (gggType == TransitionTypes.Local)
            {
                type = TransitionType.Local;

                var name = t.Name;
                if (WorldArea.Name == MapNames.Abyss && name != "Caldera of The King")
                {
                    GlobalLog.Debug($"[CombatAreaCache] Skipping \"{name}\" area transition because it leads to the same level.");
                    _processedObjects.Add(id);
                    return;
                }
            }
            else if (t.ExplicitAffixes.Any(a => a.Category == "MapMissionMods"))
            {
                type = TransitionType.Master;
            }
            else if (t.Metadata.Contains("LabyrinthTrial"))
            {
                type = TransitionType.Trial;
            }
            else if (t.ExplicitAffixes.Any(a => a.InternalName.Contains("CorruptedSideArea")))
            {
                type = TransitionType.Vaal;
            }
            else
            {
                type = TransitionType.Regular;
            }
            var pos = t.WalkablePosition();
            AreaTransitions.Add(new CachedTransition(id, pos, type, t.Destination));
            _processedObjects.Add(id);
            GlobalLog.Debug($"[CombatAreaCache] Registering {pos} (Type: {type})");
        }

        private void UpdateMonsters()
        {
            var toRemove = new List<CachedObject>();
            foreach (var cachedMob in Monsters)
            {
                var monsterObj = cachedMob.Object as Monster;
                if (monsterObj != null)
                {
                    if (monsterObj.IsDead || monsterObj.Reaction != Reaction.Enemy)
                    {
                        toRemove.Add(cachedMob);
                        if (cachedMob == TrackMobLogic.CurrentTarget)
                            TrackMobLogic.CurrentTarget = null;
                    }
                    else
                    {
                        var pos = monsterObj.WalkablePosition();
                        pos.Initialized = true;
                        cachedMob.Position = pos;
                    }
                }
                else
                {
                    //remove monsters that were close to us, but now are null (corpse exploded, shattered etc)
                    //optimal distance is debatable, but its not recommended to be higher than 100
                    if (cachedMob.Position.Distance <= 80)
                    {
                        toRemove.Add(cachedMob);
                        if (cachedMob == TrackMobLogic.CurrentTarget)
                            TrackMobLogic.CurrentTarget = null;
                    }
                }
            }
            foreach (var mob in toRemove)
            {
                Monsters.Remove(mob);
                //also remove from processed, just in case
                _processedObjects.Remove(mob.Id);
            }
        }

        private static bool SkipThisMob(Monster mob)
        {
            foreach (var affix in mob.ExplicitAffixes)
            {
                var name = affix.DisplayName;
                if (name == "Necrovigil" || name == "Voidspawn of Abaxoth")
                    return true;
            }
            return mob.Auras.Exists(aura => aura.Name == "shrine_godmode");
        }

        private static bool IsSpecialChest(Chest chest)
        {
            var m = chest.Metadata;

            if (SpecialChestMetadada.Contains(m))
                return true;

            if (m.Contains("/Breach/"))
                return true;

            if (m.Contains("/PerandusChests/"))
                return true;

            return false;
        }

        private static readonly HashSet<string> SpecialChestMetadada = new HashSet<string>
        {
            "Metadata/Chests/BootyChest",
            "Metadata/Chests/NotSoBootyChest",
            "Metadata/Chests/VaultTreasurePile",
            "Metadata/Chests/GhostPirateBootyChest",
            "Metadata/Chests/StatueMakersTools",
            "Metadata/Chests/StrongBoxes/VaultsOfAtziriUniqueChest",
            "Metadata/Chests/SideArea/SideAreaChest",
            "Metadata/Chests/CopperChestEpic3",
            "Metadata/Chests/TutorialSupportGemChest"
        };

        private static void OnLocalTransitionEntered()
        {
            GlobalLog.Info("[CombatAreaCache] Resetting unwalkable flags on all cached objects.");

            var cache = Current;

            foreach (var item in cache.Items)
            {
                item.Unwalkable = false;
            }
            foreach (var monster in cache.Monsters)
            {
                monster.Unwalkable = false;
            }
            foreach (var chest in cache.Chests)
            {
                chest.Unwalkable = false;
            }
            foreach (var specialChest in cache.SpecialChests)
            {
                specialChest.Unwalkable = false;
            }
            foreach (var strongbox in cache.Strongboxes)
            {
                strongbox.Unwalkable = false;
            }
            foreach (var shrine in cache.Shrines)
            {
                shrine.Unwalkable = false;
            }
            foreach (var transition in cache.AreaTransitions)
            {
                transition.Unwalkable = false;
            }
        }

        private static void OnItemEvaluatorRefresh(object sender, ItemEvaluatorRefreshedEventArgs args)
        {
            if (Caches.TryGetValue(LokiPoe.LocalData.AreaHash, out var cache))
            {
                GlobalLog.Info("[CombatAreaCache] Clearing processed items.");
                cache._processedItems.Clear();
            }
        }

        public class ObjectDictionary
        {
            private readonly Dictionary<string, object> _dict = new Dictionary<string, object>();

            public object this[string key]
            {
                get
                {
                    object obj;
                    _dict.TryGetValue(key, out obj);
                    return obj;
                }
                set
                {
                    if (!_dict.ContainsKey(key))
                    {
                        GlobalLog.Warn($"[Storage] Registering [{key}] = [{value ?? "null"}]");
                        _dict.Add(key, value);
                    }
                    else
                    {
                        _dict[key] = value;
                    }
                }
            }

            public bool Contains(string key)
            {
                return _dict.ContainsKey(key);
            }
        }
    }
}