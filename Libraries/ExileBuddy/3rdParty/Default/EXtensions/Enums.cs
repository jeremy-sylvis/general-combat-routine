﻿namespace Default.EXtensions
{
    public enum TaskPosition
    {
        Before,
        After,
        Replace
    }

    public enum TransitionType
    {
        Regular,
        Local,
        Vaal,
        Master,
        Trial,
    }
}