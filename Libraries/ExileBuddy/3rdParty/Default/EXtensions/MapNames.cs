﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using Loki.Bot;
using Loki.Game.GameData;

namespace Default.EXtensions
{
    [SuppressMessage("ReSharper", "UnassignedReadonlyField")]
    public static class MapNames
    {
        [AreaId("MapAtlasArcade")]
        public static readonly string Arcade;

        [AreaId("MapAtlasCrystalOre")]
        public static readonly string CrystalOre;

        [AreaId("MapAtlasDesert")]
        public static readonly string Desert;

        [AreaId("MapAtlasJungleValley")]
        public static readonly string JungleValley;

        [AreaId("MapAtlasBeach")]
        public static readonly string Beach;

        [AreaId("MapAtlasFactory")]
        public static readonly string Factory;

        [AreaId("MapAtlasGhetto")]
        public static readonly string Ghetto;

        [AreaId("MapAtlasOasis")]
        public static readonly string Oasis;

        [AreaId("MapAtlasAridLake")]
        public static readonly string AridLake;

        [AreaId("MapAtlasCavern")]
        public static readonly string Cavern;

        [AreaId("MapAtlasChannel")]
        public static readonly string Channel;

        [AreaId("MapAtlasGrotto")]
        public static readonly string Grotto;

        [AreaId("MapAtlasMarshes")]
        public static readonly string Marshes;

        [AreaId("MapAtlasSewer")]
        public static readonly string Sewer;

        [AreaId("MapAtlasVaalPyramid")]
        public static readonly string VaalPyramid;

        [AreaId("MapAtlasAcademy")]
        public static readonly string Academy;

        [AreaId("MapAtlasAcidLakes")]
        public static readonly string AcidLakes;

        [AreaId("MapAtlasDungeon")]
        public static readonly string Dungeon;

        [AreaId("MapAtlasGraveyard")]
        public static readonly string Graveyard;

        [AreaId("MapAtlasPhantasmagoria")]
        public static readonly string Phantasmagoria;

        [AreaId("MapAtlasVilla")]
        public static readonly string Villa;

        [AreaId("MapAtlasWastePool")]
        public static readonly string WastePool;

        [AreaId("MapAtlasBurialChambers")]
        public static readonly string BurialChambers;

        [AreaId("MapAtlasDryPeninsula")]
        public static readonly string Peninsula;

        [AreaId("MapAtlasDunes")]
        public static readonly string Dunes;

        [AreaId("MapAtlasMesa")]
        public static readonly string Mesa;

        [AreaId("MapAtlasPit")]
        public static readonly string Pit;

        [AreaId("MapAtlasPrimordialPool")]
        public static readonly string PrimordialPool;

        [AreaId("MapAtlasSpiderLair_")]
        public static readonly string SpiderLair;

        [AreaId("MapAtlasTower")]
        public static readonly string Tower;

        [AreaId("MapAtlasCanyon")]
        public static readonly string Canyon;

        [AreaId("MapAtlasQuarry")]
        public static readonly string Quarry;

        [AreaId("MapAtlasRacecourse")]
        public static readonly string Racecourse;

        [AreaId("MapAtlasRamparts")]
        public static readonly string Ramparts;

        [AreaId("MapAtlasSpiderForest")]
        public static readonly string SpiderForest;

        [AreaId("MapAtlasStrand")]
        public static readonly string Strand;

        [AreaId("MapAtlasThicket")]
        public static readonly string Thicket;

        [AreaId("MapAtlasVaalCity")]
        public static readonly string VaalCity;

        [AreaId("MapAtlasWharf")]
        public static readonly string Wharf;

        [AreaId("MapAtlasArachnidTomb")]
        public static readonly string ArachnidTomb;

        [AreaId("MapAtlasCastleRuins")]
        public static readonly string CastleRuins;

        [AreaId("MapAtlasCatacomb_")]
        public static readonly string Catacombs;

        [AreaId("MapAtlasCells")]
        public static readonly string Cells;

        [AreaId("MapAtlasArmory")]
        public static readonly string Armoury;

        [AreaId("MapAtlasAshenWood")]
        public static readonly string AshenWood;

        [AreaId("MapAtlasMudGeyser")]
        public static readonly string MudGeyser;

        [AreaId("MapAtlasArachnidNest")]
        public static readonly string ArachnidNest;

        [AreaId("MapAtlasArena")]
        public static readonly string Arena;

        [AreaId("MapAtlasBog")]
        public static readonly string Bog;

        [AreaId("MapAtlasCemetery")]
        public static readonly string Cemetery;

        [AreaId("MapAtlasBarrows")]
        public static readonly string Barrows;

        [AreaId("MapAtlasAtoll")]
        public static readonly string Atoll;

        [AreaId("MapAtlasPier")]
        public static readonly string Pier;

        [AreaId("MapAtlasShore")]
        public static readonly string Shore;

        [AreaId("MapAtlasTropicalIsland")]
        public static readonly string TropicalIsland;

        [AreaId("MapAtlasCoves")]
        public static readonly string Coves;

        [AreaId("MapAtlasCrypt")]
        public static readonly string Crypt;

        [AreaId("MapAtlasMuseum")]
        public static readonly string Museum;

        [AreaId("MapAtlasOrchard")]
        public static readonly string Orchard;

        [AreaId("MapAtlasOvergrownShrine")]
        public static readonly string OvergrownShrine;

        [AreaId("MapAtlasPromenade")]
        public static readonly string Promenade;

        [AreaId("MapAtlasReef")]
        public static readonly string Reef;

        [AreaId("MapAtlasTemple")]
        public static readonly string Temple;

        [AreaId("MapAtlasColonnade")]
        public static readonly string Colonnade;

        [AreaId("MapAtlasArsenal")]
        public static readonly string Arsenal;

        [AreaId("MapAtlasCourtyard")]
        public static readonly string Courtyard;

        [AreaId("MapAtlasMalformation")]
        public static readonly string Malformation;

        [AreaId("MapAtlasQuay")]
        public static readonly string Port;

        [AreaId("MapAtlasTerrace")]
        public static readonly string Terrace;

        [AreaId("MapAtlasUndergroundRiver")]
        public static readonly string UndergroundRiver;

        [AreaId("MapAtlasBazaar")]
        public static readonly string Bazaar;

        [AreaId("MapAtlasChateau")]
        public static readonly string Chateau;

        [AreaId("MapAtlasExcavation")]
        public static readonly string Excavation;

        [AreaId("MapAtlasPrecinct")]
        public static readonly string Precinct;

        [AreaId("MapAtlasTortureChamber")]
        public static readonly string TortureChamber;

        [AreaId("MapAtlasUndergroundSea")]
        public static readonly string UndergroundSea;

        [AreaId("MapAtlasWasteland")]
        public static readonly string Wasteland;

        [AreaId("MapAtlasCrematorium")]
        public static readonly string Crematorium;

        [AreaId("MapAtlasEstuary")]
        public static readonly string Estuary;

        [AreaId("MapAtlasNecropolis")]
        public static readonly string Necropolis;

        [AreaId("MapAtlasPlateau")]
        public static readonly string Plateau;

        [AreaId("MapAtlasIvoryTemple")]
        public static readonly string IvoryTemple;

        [AreaId("MapAtlasResidence")]
        public static readonly string Residence;

        [AreaId("MapAtlasShipyard")]
        public static readonly string Shipyard;

        [AreaId("MapAtlasVault")]
        public static readonly string Vault;

        [AreaId("MapAtlasLair")]
        public static readonly string Lair;

        [AreaId("MapAtlasBeacon")]
        public static readonly string Beacon;

        [AreaId("MapAtlasGorge")]
        public static readonly string Gorge;

        [AreaId("MapAtlasHighGardens")]
        public static readonly string HighGardens;

        [AreaId("MapAtlasPlaza")]
        public static readonly string Plaza;

        [AreaId("MapAtlasScriptorium")]
        public static readonly string Scriptorium;

        [AreaId("MapAtlasSulphurWastes")]
        public static readonly string SulphurWastes;

        [AreaId("MapAtlasWaterways")]
        public static readonly string Waterways;

        [AreaId("MapAtlasMaze")]
        public static readonly string Maze;

        [AreaId("MapAtlasMineralPools")]
        public static readonly string MineralPools;

        [AreaId("MapAtlasPalace")]
        public static readonly string Palace;

        [AreaId("MapAtlasShrine")]
        public static readonly string Shrine;

        [AreaId("MapAtlasSprings")]
        public static readonly string Springs;

        [AreaId("MapAtlasVolcano")]
        public static readonly string Volcano;

        [AreaId("MapAtlasAbyss")]
        public static readonly string Abyss;

        [AreaId("MapAtlasColosseum")]
        public static readonly string Colosseum;

        [AreaId("MapAtlasCore")]
        public static readonly string Core;

        [AreaId("MapAtlasDarkForest")]
        public static readonly string DarkForest;

        [AreaId("MapAtlasOvergrownRuin")]
        public static readonly string OvergrownRuin;

        [AreaId("MapAtlasChimera")]
        public static readonly string PitOfChimera;

        [AreaId("MapAtlasHydra")]
        public static readonly string LairOfHydra;

        [AreaId("MapAtlasPhoenix")]
        public static readonly string ForgeOfPhoenix;

        [AreaId("MapAtlasMinotaur")]
        public static readonly string MazeOfMinotaur;

        [AreaId("MapAtlasShapersRealm")]
        public static readonly string ShaperRealm;

        [AreaId("MapAtlasVaalTemple")]
        public static readonly string VaalTemple;

        [AreaId("MapAtlasVaalPyramidUnique_")]
        public static readonly string VaultsOfAtziri;

        [AreaId("MapAtlasGraveyardUnique")]
        public static readonly string HallowedGround;

        [AreaId("MapAtlasMuseumUnique")]
        public static readonly string PutridCloister;

        [AreaId("MapAtlasPitUnique")]
        public static readonly string DarbelPromise;

        [AreaId("MapAtlasStrandUnique")]
        public static readonly string WhakawairuaTuahu;

        [AreaId("MapAtlasCatacombUnique")]
        public static readonly string OlmecSanctum;

        [AreaId("MapAtlasAtollUnique")]
        public static readonly string MaelstromOfChaos;

        [AreaId("MapAtlasTropicalIslandUnique")]
        public static readonly string UntaintedParadise;

        [AreaId("MapAtlasCryptUnique")]
        public static readonly string CowardTrial;

        [AreaId("MapAtlasOvergrownShrineUnique")]
        public static readonly string ActonNightmare;

        [AreaId("MapAtlasPromenadeUnique")]
        public static readonly string HallOfGrandmasters;

        [AreaId("MapAtlasReefUnique")]
        public static readonly string MaoKun;

        [AreaId("MapAtlasTempleUnique")]
        public static readonly string PoorjoyAsylum;

        [AreaId("MapAtlasCourtyardUnique")]
        public static readonly string VinktarSquare;

        [AreaId("MapAtlasUndergroundRiverUnique")]
        public static readonly string CaerBlaidd;

        [AreaId("MapAtlasChateauUnique")]
        public static readonly string PerandusManor;

        [AreaId("MapAtlasTortureChamberUnique")]
        public static readonly string ObaCursedTrove;

        [AreaId("MapAtlasNecropolisUnique")]
        public static readonly string DeathAndTaxes;

        static MapNames()
        {
            var fieldDict = new Dictionary<string, FieldInfo>();
            foreach (var field in typeof(MapNames).GetFields())
            {
                var idAttribute = field.GetCustomAttribute<AreaId>();
                if (idAttribute != null)
                {
                    fieldDict.Add(idAttribute.Id, field);
                }
            }

            int total = fieldDict.Count;
            int processed = 0;

            foreach (var area in Dat.WorldAreas)
            {
                if (processed >= total) break;
                if (fieldDict.TryGetValue(area.Id, out FieldInfo field))
                {
                    field.SetValue(null, area.Name);
                    ++processed;
                }
            }
            if (processed < total)
            {
                GlobalLog.Error("[MapNames] Update is required. Not all fields were initialized.");
                BotManager.Stop();
            }
        }
    }
}