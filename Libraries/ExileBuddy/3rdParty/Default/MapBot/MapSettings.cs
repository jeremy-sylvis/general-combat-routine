﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Default.EXtensions;
using Loki;
using Newtonsoft.Json;

namespace Default.MapBot
{
    public class MapSettings
    {
        private static readonly string SettingsPath = Path.Combine(Configuration.Instance.Path, "MapBot", "MapSettings.json");

        private static MapSettings _instance;
        public static MapSettings Instance => _instance ?? (_instance = new MapSettings());

        private MapSettings()
        {
            InitList();
            Load();
            InitDict();
            Configuration.OnSaveAll += (sender, args) => { Save(); };

            MapList = MapList.OrderByDescending(m => m.Priority).ToList();
        }

        public List<MapData> MapList { get; } = new List<MapData>();
        public Dictionary<string, MapData> MapDict { get; } = new Dictionary<string, MapData>();

        private void InitList()
        {
            MapList.Add(new MapData(MapNames.Arcade, 1, MapType.Regular));
            MapList.Add(new MapData(MapNames.CrystalOre, 1, MapType.Complex));
            MapList.Add(new MapData(MapNames.Desert, 1, MapType.Regular));
            MapList.Add(new MapData(MapNames.JungleValley, 1, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Beach, 2, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Factory, 2, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Ghetto, 2, MapType.Regular));
            MapList.Add(new MapData(MapNames.Oasis, 2, MapType.Regular));
            MapList.Add(new MapData(MapNames.AridLake, 3, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Cavern, 3, MapType.Regular));
            MapList.Add(new MapData(MapNames.Channel, 3, MapType.Regular));
            MapList.Add(new MapData(MapNames.Grotto, 3, MapType.Regular));
            MapList.Add(new MapData(MapNames.Marshes, 3, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Sewer, 3, MapType.Regular));
            MapList.Add(new MapData(MapNames.VaalPyramid, 3, MapType.Multilevel));
            MapList.Add(new MapData(MapNames.Academy, 4, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.AcidLakes, 4, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Dungeon, 4, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Graveyard, 4, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Phantasmagoria, 4, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Villa, 4, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.WastePool, 4, MapType.Regular));
            MapList.Add(new MapData(MapNames.BurialChambers, 5, MapType.Complex));
            MapList.Add(new MapData(MapNames.Dunes, 5, MapType.Regular));
            MapList.Add(new MapData(MapNames.Mesa, 5, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Peninsula, 5, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Pit, 5, MapType.Regular));
            MapList.Add(new MapData(MapNames.PrimordialPool, 5, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.SpiderLair, 5, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Tower, 5, MapType.Multilevel));
            MapList.Add(new MapData(MapNames.Canyon, 6, MapType.Regular));
            MapList.Add(new MapData(MapNames.Quarry, 6, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Racecourse, 6, MapType.Multilevel));
            MapList.Add(new MapData(MapNames.Ramparts, 6, MapType.Multilevel));
            MapList.Add(new MapData(MapNames.SpiderForest, 6, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Strand, 6, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Thicket, 6, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.VaalCity, 6, MapType.Regular));
            MapList.Add(new MapData(MapNames.Wharf, 6, MapType.Regular));
            MapList.Add(new MapData(MapNames.ArachnidTomb, 7, MapType.Multilevel));
            MapList.Add(new MapData(MapNames.Armoury, 7, MapType.Regular));
            MapList.Add(new MapData(MapNames.AshenWood, 7, MapType.Regular));
            MapList.Add(new MapData(MapNames.CastleRuins, 7, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Catacombs, 7, MapType.Regular));
            MapList.Add(new MapData(MapNames.Cells, 7, MapType.Regular));
            MapList.Add(new MapData(MapNames.MudGeyser, 7, MapType.Regular));
            MapList.Add(new MapData(MapNames.ArachnidNest, 8, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Arena, 8, MapType.Complex));
            MapList.Add(new MapData(MapNames.Atoll, 8, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Barrows, 8, MapType.Complex));
            MapList.Add(new MapData(MapNames.Bog, 8, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Cemetery, 8, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Pier, 8, MapType.Bossroom) {UnsupportedBossroom = true});
            MapList.Add(new MapData(MapNames.Shore, 8, MapType.Regular));
            MapList.Add(new MapData(MapNames.TropicalIsland, 8, MapType.Multilevel));
            MapList.Add(new MapData(MapNames.Coves, 9, MapType.Regular));
            MapList.Add(new MapData(MapNames.Crypt, 9, MapType.Regular));
            MapList.Add(new MapData(MapNames.Museum, 9, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Orchard, 9, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.OvergrownShrine, 9, MapType.Regular));
            MapList.Add(new MapData(MapNames.Promenade, 9, MapType.Regular));
            MapList.Add(new MapData(MapNames.Reef, 9, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Temple, 9, MapType.Bossroom) {UnsupportedBossroom = true});
            MapList.Add(new MapData(MapNames.Arsenal, 10, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Colonnade, 10, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Courtyard, 10, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Malformation, 10, MapType.Regular));
            MapList.Add(new MapData(MapNames.Port, 10, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Terrace, 10, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.UndergroundRiver, 10, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Bazaar, 11, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Chateau, 11, MapType.Regular));
            MapList.Add(new MapData(MapNames.Excavation, 11, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Precinct, 11, MapType.Regular));
            MapList.Add(new MapData(MapNames.TortureChamber, 11, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.UndergroundSea, 11, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Wasteland, 11, MapType.Regular));
            MapList.Add(new MapData(MapNames.Crematorium, 12, MapType.Multilevel));
            MapList.Add(new MapData(MapNames.Estuary, 12, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.IvoryTemple, 12, MapType.Complex));
            MapList.Add(new MapData(MapNames.Necropolis, 12, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Plateau, 12, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Residence, 12, MapType.Multilevel));
            MapList.Add(new MapData(MapNames.Shipyard, 12, MapType.Regular));
            MapList.Add(new MapData(MapNames.Vault, 12, MapType.Bossroom) {Unsupported = true});
            MapList.Add(new MapData(MapNames.Beacon, 13, MapType.Regular));
            MapList.Add(new MapData(MapNames.Gorge, 13, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.HighGardens, 13, MapType.Regular));
            MapList.Add(new MapData(MapNames.Lair, 13, MapType.Multilevel));
            MapList.Add(new MapData(MapNames.Plaza, 13, MapType.Bossroom) {Unsupported = true});
            MapList.Add(new MapData(MapNames.Scriptorium, 13, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.SulphurWastes, 13, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Waterways, 13, MapType.Regular));
            MapList.Add(new MapData(MapNames.Maze, 14, MapType.Regular));
            MapList.Add(new MapData(MapNames.MineralPools, 14, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Palace, 14, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Shrine, 14, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Springs, 14, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Volcano, 14, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Abyss, 15, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.Colosseum, 15, MapType.Multilevel));
            MapList.Add(new MapData(MapNames.Core, 15, MapType.Bossroom) {UnsupportedBossroom = true});
            MapList.Add(new MapData(MapNames.DarkForest, 15, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.OvergrownRuin, 15, MapType.Multilevel));
            MapList.Add(new MapData(MapNames.VaalTemple, 16, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.ForgeOfPhoenix, 16, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.LairOfHydra, 16, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.MazeOfMinotaur, 16, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.PitOfChimera, 16, MapType.Bossroom));
            MapList.Add(new MapData(MapNames.VaultsOfAtziri, 3, MapType.Regular) {Ignored = true});
            MapList.Add(new MapData(MapNames.WhakawairuaTuahu, 6, MapType.Multilevel) {Ignored = true});
            MapList.Add(new MapData(MapNames.OlmecSanctum, 7, MapType.Complex) {Ignored = true});
            MapList.Add(new MapData(MapNames.MaelstromOfChaos, 8, MapType.Bossroom) {Ignored = true});
            MapList.Add(new MapData(MapNames.MaoKun, 9, MapType.Regular) {Ignored = true});
            MapList.Add(new MapData(MapNames.PoorjoyAsylum, 9, MapType.Regular) {Ignored = true});
            MapList.Add(new MapData(MapNames.PutridCloister, 9, MapType.Multilevel) {Ignored = true});
            MapList.Add(new MapData(MapNames.CaerBlaidd, 10, MapType.Bossroom) {Ignored = true});
        }

        private void InitDict()
        {
            foreach (var data in MapList)
            {
                MapDict.Add(data.Name, data);
            }
        }

        private void Load()
        {
            if (!File.Exists(SettingsPath))
                return;

            var json = File.ReadAllText(SettingsPath);
            if (string.IsNullOrWhiteSpace(json))
            {
                GlobalLog.Error("[MapBot] Fail to load \"MapSettings.json\". File is empty.");
                return;
            }
            var parts = JsonConvert.DeserializeObject<Dictionary<string, EditablePart>>(json);
            if (parts == null)
            {
                GlobalLog.Error("[MapBot] Fail to load \"MapSettings.json\". Json deserealizer returned null.");
                return;
            }
            foreach (var data in MapList)
            {
                if (parts.TryGetValue(data.Name, out var part))
                {
                    data.Priority = part.Priority;
                    data.Ignored = part.Ignore;
                    data.IgnoredBossroom = part.IgnoreBossroom;
                    data.MobRemaining = part.MobRemaining;
                    data.StrictMobRemaining = part.StrictMobRemaining;
                    data.ExplorationPercent = part.ExplorationPercent;
                    data.StrictExplorationPercent = part.StrictExplorationPercent;
                    data.TrackMob = part.TrackMob;
                    data.FastTransition = part.FastTransition;
                }
            }
        }

        private void Save()
        {
            var parts = new Dictionary<string, EditablePart>(MapList.Count);

            foreach (var map in MapList)
            {
                var part = new EditablePart
                {
                    Priority = map.Priority,
                    Ignore = map.Ignored,
                    IgnoreBossroom = map.IgnoredBossroom,
                    MobRemaining = map.MobRemaining,
                    StrictMobRemaining = map.StrictMobRemaining,
                    ExplorationPercent = map.ExplorationPercent,
                    StrictExplorationPercent = map.StrictExplorationPercent,
                    TrackMob = map.TrackMob,
                    FastTransition = map.FastTransition
                };
                parts.Add(map.Name, part);
            }
            var json = JsonConvert.SerializeObject(parts, Formatting.Indented);
            File.WriteAllText(SettingsPath, json);
        }

        private class EditablePart
        {
            public int Priority;
            public bool Ignore;
            public bool IgnoreBossroom;
            public int MobRemaining;
            public bool StrictMobRemaining;
            public int ExplorationPercent;
            public bool StrictExplorationPercent;
            public bool? TrackMob;
            public bool? FastTransition;
        }
    }
}