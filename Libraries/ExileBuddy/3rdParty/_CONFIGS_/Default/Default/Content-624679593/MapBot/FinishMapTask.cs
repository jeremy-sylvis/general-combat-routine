﻿using System.Threading.Tasks;
using Default.EXtensions;
using Loki.Bot;

namespace Default.MapBot
{
    public class FinishMapTask : ITask
    {
        private const int MaxPulses = 4;
        private static int _pulse;

        public async Task<bool> Run()
        {
            if (!World.CurrentArea.IsMap)
                return false;

            await Coroutines.FinishCurrentAction();

            if (_pulse < MaxPulses)
            {
                ++_pulse;
                GlobalLog.Info($"[FinishMapTask] Final pulse {_pulse}/{MaxPulses}");
                await Wait.SleepSafe(500);
                return true;
            }

            GlobalLog.Warn("[FinishMapTask] Now leaving current map.");

            if (!await PlayerAction.TpToTown())
            {
                ErrorManager.ReportError();
                return true;
            }

            MapBot.IsOnRun = false;
            Statistics.Instance.OnMapFinish();
            GlobalLog.Info("[MapBot] MapFinished event.");
            Utility.BroadcastMessage(this, MapBot.Messages.MapFinished);
            return true;
        }

        public MessageResult Message(Message message)
        {
            if (message.Id == MapBot.Messages.NewMapEntered)
            {
                _pulse = 0;
                return MessageResult.Processed;
            }
            return MessageResult.Unprocessed;
        }

        #region Unused interface methods

        public async Task<LogicResult> Logic(Logic logic)
        {
            return LogicResult.Unprovided;
        }

        public void Tick()
        {
        }

        public void Start()
        {
        }

        public void Stop()
        {
        }

        public string Name => "FinishMapTask";
        public string Description => "Task for leaving current map.";
        public string Author => "ExVault";
        public string Version => "1.0";

        #endregion
    }
}