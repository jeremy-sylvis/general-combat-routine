﻿using System.Linq;
using System.Threading.Tasks;
using Default.EXtensions;
using Default.EXtensions.CachedObjects;
using Default.EXtensions.Global;
using Loki.Bot;
using Loki.Game;

namespace Default.MapBot
{
    public class TransitionTriggerTask : ITask
    {
        private const int MaxInteractionAttempts = 10;
        private const int MaxTransitionWaits = 50;

        private static readonly Interval TickInterval = new Interval(200);

        private static bool _enabled;
        private static CachedObject _trigger;
        private static int _waitCount;

        public async Task<bool> Run()
        {
            if (!_enabled || _trigger == null || MapExplorationTask.MapCompleted)
                return false;

            if (!World.CurrentArea.IsMap)
                return false;

            var pos = _trigger.Position;
            if (pos.IsFar)
            {
                if (!pos.TryCome())
                {
                    GlobalLog.Error($"[TransitionTriggerTask] Fail to move to {pos}. Transition trigger is unwalkable. Bot will not be able to enter a bossroom.");
                    _enabled = false;
                }
                return true;
            }
            var triggerObj = _trigger.Object;
            if (triggerObj == null || !triggerObj.IsTargetable)
            {
                if (CombatAreaCache.Current.AreaTransitions.Any(a => a.Type == TransitionType.Local && a.Position.Distance <= 50))
                {
                    GlobalLog.Debug("[TransitionTriggerTask] Area transition has been successfully unlocked.");
                    _enabled = false;
                    return true;
                }
                ++_waitCount;
                if (_waitCount > MaxTransitionWaits)
                {
                    GlobalLog.Error("[TransitionTriggerTask] Unexpected error. Waiting for area transition spawn timeout.");
                    _enabled = false;
                    return true;
                }
                GlobalLog.Debug($"[TransitionTriggerTask] Waiting for area transition spawn {_waitCount}/{MaxTransitionWaits}");
                await Wait.StuckDetectionSleep(200);
                return true;
            }
            var attempts = ++_trigger.InteractionAttempts;
            if (attempts > MaxInteractionAttempts)
            {
                GlobalLog.Error("[TransitionTriggerTask] All attempts to interact with transition trigger have been spent.");
                _enabled = false;
                return true;
            }
            if (await PlayerAction.Interact(triggerObj, 1))
            {
                await Wait.LatencySleep();
                await Wait.For(() => !triggerObj.Fresh().IsTargetable, "trigger become untargetable", 100, 500);
            }
            else
            {
                await Wait.SleepSafe(500);
            }
            return true;
        }

        public void Tick()
        {
            if (!_enabled || _trigger != null || MapExplorationTask.MapCompleted)
                return;

            if (!TickInterval.Elapsed)
                return;

            if (!LokiPoe.IsInGame || !World.CurrentArea.IsMap)
                return;

            foreach (var obj in LokiPoe.ObjectManager.Objects)
            {
                if (!obj.IsTargetable)
                    continue;

                if (obj.Metadata != "Metadata/QuestObjects/Library/HiddenDoorTrigger" &&
                    obj.Metadata != "Metadata/Chests/Sarcophagi/sarcophagus_door")
                    continue;

                var pos = obj.WalkablePosition();
                _trigger = new CachedObject(obj.Id, pos);
                GlobalLog.Warn($"[TransitionTriggerTask] Registering {pos}");
                break;
            }
        }

        public MessageResult Message(Message message)
        {
            if (message.Id == MapBot.Messages.NewMapEntered)
            {
                _enabled = false;
                _trigger = null;
                _waitCount = 0;

                var areaName = message.GetInput<string>();

                if (areaName == MapNames.Academy ||
                    areaName == MapNames.Museum ||
                    areaName == MapNames.Scriptorium ||
                    areaName == MapNames.Necropolis)
                {
                    if (MapData.Current.IgnoredBossroom)
                    {
                        GlobalLog.Info("[TransitionTriggerTask] Skipping this task because bossroom is ignored.");
                    }
                    else
                    {
                        _enabled = true;
                        GlobalLog.Info($"[TransitionTriggerTask] Enabled ({areaName})");
                    }
                }
                return MessageResult.Processed;
            }
            return MessageResult.Unprocessed;
        }

        #region Unused interface methods

        public async Task<LogicResult> Logic(Logic logic)
        {
            return LogicResult.Unprovided;
        }

        public void Start()
        {
        }

        public void Stop()
        {
        }

        public string Name => "TransitionTriggerTask";
        public string Description => "Task that opens transition triggers.";
        public string Author => "ExVault";
        public string Version => "1.0";

        #endregion
    }
}