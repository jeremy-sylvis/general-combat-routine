﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Default.EXtensions.CachedObjects;
using Default.EXtensions.Positions;
using Loki.Bot;
using Loki.Common;
using Loki.Game.GameData;
using Loki.Game.Objects;
using StashUi = Loki.Game.LokiPoe.InGameState.StashUi;

namespace Default.EXtensions.CommonTasks
{
    public class StashTask : ITask
    {
        private static bool _checkInvalidTabs = true;
        private static bool _checkFullTabs = true;

        public async Task<bool> Run()
        {
            var area = World.CurrentArea;
            if (!area.IsTown && !area.IsHideoutArea)
                return false;

            var itemsToStash = new List<StashItem>();

            foreach (var item in Inventories.InventoryItems)
            {
                var stashItem = new StashItem(item);
                var itemType = stashItem.Type.ItemType;

                if (itemType == ItemTypes.Quest)
                    continue;

                var name = stashItem.Name;

                if (itemType == ItemTypes.Currency && (name == CurrencyNames.Wisdom || name == CurrencyNames.Portal))
                    continue;

                itemsToStash.Add(stashItem);
            }

            var excessWisdoms = Inventories.GetExcessCurrency(CurrencyNames.Wisdom);
            if (excessWisdoms != null)
            {
                foreach (var wisdom in excessWisdoms)
                {
                    itemsToStash.Add(new StashItem(wisdom));
                }
            }

            if (!Settings.Instance.SellExcessPortals)
            {
                var excessPortals = Inventories.GetExcessCurrency(CurrencyNames.Portal);
                if (excessPortals != null)
                {
                    foreach (var portal in excessPortals)
                    {
                        itemsToStash.Add(new StashItem(portal));
                    }
                }
            }

            if (itemsToStash.Count == 0)
            {
                GlobalLog.Info("[StashTask] No items to stash.");
                return false;
            }

            if (_checkInvalidTabs)
            {
                if (!await Inventories.OpenStash())
                {
                    ErrorManager.ReportError();
                    return true;
                }
                var wrongTabs = GetNonexistentTabs();
                if (wrongTabs.Count > 0)
                {
                    GlobalLog.Error("[StashTask] The following tabs are specified in stashing rules, but do not exist in stash:");
                    GlobalLog.Error($"{string.Join(", ", wrongTabs)}");
                    GlobalLog.Error("[StashTask] Please provide correct tab names.");
                    BotManager.Stop();
                    return true;
                }
                GlobalLog.Debug("[StashTask] All tabs specified in stashing rules exist in stash.");
                _checkInvalidTabs = false;
            }

            if (_checkFullTabs)
            {
                if (Settings.Instance.FullTabs.Count > 0)
                {
                    if (!await Inventories.OpenStash())
                    {
                        ErrorManager.ReportError();
                        return true;
                    }

                    var cleanedTabs = new List<string>();
                    var actualTabs = StashUi.TabControl.TabNames;

                    foreach (var tab in Settings.Instance.FullTabs)
                    {
                        if (!actualTabs.Contains(tab))
                        {
                            GlobalLog.Warn($"[StashTask] Full tab check: \"{tab}\" tab no longer exists. Removing it from full tab list.");
                            cleanedTabs.Add(tab);
                            continue;
                        }
                        if (!await Inventories.OpenStashTab(tab))
                        {
                            ErrorManager.ReportError();
                            return true;
                        }
                        if (StashUi.StashTabInfo.IsPremiumCurrency)
                        {
                            GlobalLog.Warn($"[StashTask] Full tab check: \"{tab}\" tab is premium currency. Fullness is variable. Marking this tab as not full.");
                            cleanedTabs.Add(tab);
                            continue;
                        }
                        if (StashUi.InventoryControl.Inventory.AvailableInventorySquares >= 8)
                        {
                            GlobalLog.Warn($"[StashTask] Full tab check: \"{tab}\" tab is no longer full. Bot will stash items to this tab again.");
                            cleanedTabs.Add(tab);
                            continue;
                        }
                        GlobalLog.Warn($"[StashTask] Full tab check: \"{tab}\" tab is still full.");
                    }
                    foreach (var tab in cleanedTabs)
                    {
                        Settings.Instance.FullTabs.Remove(tab);
                    }
                }
                _checkFullTabs = false;
            }

            GlobalLog.Info($"[StashTask] {itemsToStash.Count} items to stash.");

            AssignStashTabs(itemsToStash);

            foreach (var item in itemsToStash.OrderBy(i => i.StashTab).ThenBy(i => i.Position, Position.Comparer.Instance))
            {
                var itemName = item.Name;
                var itemPos = item.Position;
                var tabName = item.StashTab;

                GlobalLog.Debug($"[StashTask] Now going to stash \"{itemName}\" to \"{tabName}\" tab.");

                if (!await Inventories.OpenStashTab(tabName))
                {
                    ErrorManager.ReportError();
                    return true;
                }
                if (!Inventories.StashTabCanFitItem(itemPos))
                {
                    GlobalLog.Warn($"[StashTask] Cannot fit \"{itemName}\" to \"{tabName}\" tab. Now marking that tab as full.");
                    Settings.Instance.FullTabs.Add(tabName);
                    return true;
                }
                if (!await Inventories.FastMoveFromInventory(itemPos))
                {
                    ErrorManager.ReportError();
                    return true;
                }
                GlobalLog.Info($"[Events] Item stashed ({item.FullName})");
                Utility.BroadcastMessage(this, Events.Messages.ItemStashedEvent, item);
            }
            await Coroutines.CloseBlockingWindows();
            return true;
        }

        private static void AssignStashTabs(IEnumerable<StashItem> items)
        {
            foreach (var item in items)
            {
                var itemType = item.Type.ItemType;
                if (itemType == ItemTypes.Currency)
                {
                    var m = item.Metadata;
                    if (m.Contains("Essence") || m.Contains("CorruptMonolith"))
                    {
                        item.StashTab = GetTabForStashing(Settings.StashingCategory.Essence);
                        continue;
                    }
                    if (m.Contains("ItemisedProphecy"))
                    {
                        item.StashTab = GetTabForStashing(Settings.StashingCategory.Prophecy);
                        continue;
                    }
                    if (m.Contains("AddAtlasMod"))
                    {
                        item.StashTab = GetTabForStashing(Settings.CurrencyGroup.Sextant, true);
                        continue;
                    }
                    if (m.Contains("Breach"))
                    {
                        item.StashTab = GetTabForStashing(Settings.CurrencyGroup.Breach, true);
                        continue;
                    }
                    item.StashTab = GetTabForStashing(item.Name, true);
                    continue;
                }

                if (itemType == ItemTypes.Gem)
                {
                    item.StashTab = GetTabForStashing(Settings.StashingCategory.Gem);
                    continue;
                }

                if (itemType == ItemTypes.DivinationCard)
                {
                    item.StashTab = GetTabForStashing(Settings.StashingCategory.Card);
                    continue;
                }

                if (itemType == ItemTypes.Jewel)
                {
                    item.StashTab = GetTabForStashing(Settings.StashingCategory.Jewel);
                    continue;
                }

                if (itemType == ItemTypes.Map)
                {
                    item.StashTab = GetTabForStashing(Settings.StashingCategory.Map);
                    continue;
                }

                if (itemType == ItemTypes.MapFragment)
                {
                    item.StashTab = GetTabForStashing(Settings.StashingCategory.Fragment);
                    continue;
                }

                if (itemType == ItemTypes.Leaguestone)
                {
                    item.StashTab = GetTabForStashing(Settings.StashingCategory.Leaguestone);
                    continue;
                }

                var rarity = item.Rarity;
                if (rarity == Rarity.Rare)
                {
                    item.StashTab = GetTabForStashing(Settings.StashingCategory.Rare);
                    continue;
                }
                if (rarity == Rarity.Unique)
                {
                    item.StashTab = GetTabForStashing(Settings.StashingCategory.Unique);
                    continue;
                }

                GlobalLog.Error($"[StashTask] Cannot determine stash tab for \"{item.FullName}\" ({rarity}). It will be stashed to \"Other\" tabs.");
                item.StashTab = GetTabForStashing(Settings.StashingCategory.Other);
            }
        }

        private static string GetTabForStashing(string name, bool forCurrency = false)
        {
            var settings = Settings.Instance;

            var tabs = forCurrency
                ? settings.GetTabsForCurrency(name)
                : settings.GetTabsForCategory(name);

            var tab = tabs.Find(t => !settings.FullTabs.Contains(t));
            if (tab == null)
            {
                GlobalLog.Warn($"[StashTask] All tabs for \"{name}\" are full. Current item will be stashed to \"Other\" tabs.");
                tabs = settings.GetTabsForCategory(Settings.StashingCategory.Other);
                tab = tabs.Find(t => !settings.FullTabs.Contains(t));
                if (tab == null)
                {
                    GlobalLog.Error($"[StashTask] All tabs for \"{name}\" are full and all \"Other\" tabs are full. Now stopping the bot because it cannot continue.");
                    GlobalLog.Error("[StashTask] Please clean your tabs.");
                    ErrorManager.ReportCriticalError();
                    return null;
                }
            }
            return tab;
        }

        private static HashSet<string> GetNonexistentTabs()
        {
            var result = new HashSet<string>();
            var actualTabs = StashUi.TabControl.TabNames;

            foreach (var rule in Settings.Instance.GeneralStashingRules)
            {
                foreach (var tab in rule.TabList)
                {
                    if (!actualTabs.Contains(tab))
                    {
                        result.Add(tab);
                    }
                }
            }

            foreach (var rule in Settings.Instance.CurrencyStashingRules.Where(r => r.Enabled))
            {
                foreach (var tab in rule.TabList)
                {
                    if (!actualTabs.Contains(tab))
                    {
                        result.Add(tab);
                    }
                }
            }

            return result;
        }

        public static void RequestInvalidTabCheck()
        {
            _checkInvalidTabs = true;
        }

        public static void RequestFullTabCheck()
        {
            _checkFullTabs = true;
        }

        public void Start()
        {
            RequestFullTabCheck();
        }

        private class StashItem : CachedItem
        {
            public Vector2i Position { get; }
            public string StashTab { get; set; }

            public StashItem(Item item) : base(item)
            {
                Position = item.LocationTopLeft;
            }
        }

        #region Unused interface methods

        public MessageResult Message(Message message)
        {
            return MessageResult.Unprocessed;
        }

        public async Task<LogicResult> Logic(Logic logic)
        {
            return LogicResult.Unprovided;
        }

        public void Tick()
        {
        }

        public void Stop()
        {
        }


        public void Enable()
        {
        }

        public void Disable()
        {
        }

        public string Name => "StashTask";

        public string Description => "Task that handles item stashing.";

        public string Author => "ExVault";

        public string Version => "1.0";

        #endregion
    }
}