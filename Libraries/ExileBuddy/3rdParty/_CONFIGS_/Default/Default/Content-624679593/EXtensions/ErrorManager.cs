﻿using System;
using Loki.Bot;

namespace Default.EXtensions
{
    public static class ErrorManager
    {
        public const int MaxErrors = 10;
        public static int ErrorCount { get; private set; }

        static ErrorManager()
        {
            Events.AreaChanged += (sender, args) => Reset();
        }

        public static void Reset()
        {
            GlobalLog.Info("[ErrorManager] Error count has been reset.");
            ErrorCount = 0;
        }

        public static void ReportError()
        {
            ++ErrorCount;
            GlobalLog.Error($"[ErrorManager] Error count: {ErrorCount}/{MaxErrors}.");

            if (ErrorCount >= MaxErrors)
            {
                GlobalLog.Error("[ErrorManager] Error threshold has been reached. Now requesting bot to stop.");
                if (BotManager.Stop()) ErrorCount = 0;
                throw new Exception("MAX_ERRORS");
            }
        }

        public static void ReportCriticalError()
        {
            GlobalLog.Error("[CRITICAL ERROR] Now requesting bot to stop.");
            BotManager.Stop();
            throw new Exception("CRITICAL_ERROR");
        }
    }
}