﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Default.EXtensions.Positions;
using Loki.Bot;
using Loki.Common;
using Loki.Game;
using Loki.Game.Objects;

namespace Default.EXtensions.CommonTasks
{
    public class SortInventoryTask : ITask
    {
        private const int LastRow = 4;
        private const int LastColumn = 11;

        private static readonly Dictionary<string, Vector2i> CurrenciesToSort = new Dictionary<string, Vector2i>
        {
            {CurrencyNames.Wisdom, new Vector2i(LastColumn, LastRow)},
            {CurrencyNames.Portal, new Vector2i(LastColumn - 1, LastRow)},
            {CurrencyNames.Chance, new Vector2i(LastColumn - 2, LastRow)},
            {CurrencyNames.Alchemy, new Vector2i(LastColumn - 3, LastRow)},
        };

        public async Task<bool> Run()
        {
            var area = World.CurrentArea;
            if (!area.IsTown && !area.IsHideoutArea)
                return false;

            if (Inventories.AvailableInventorySquares == 0)
                return false;

            while (true)
            {
                var cursorItem = LokiPoe.InGameState.CursorItemOverlay.Item;
                if (cursorItem != null)
                {
                    if (!await Inventories.OpenInventory())
                    {
                        ErrorManager.ReportError();
                        return true;
                    }
                    if (!await HandleCursorItem(cursorItem))
                    {
                        ErrorManager.ReportError();
                        return true;
                    }
                    continue;
                }
                var itemForSort = GetFirstItemForSort();

                if (itemForSort == null)
                    break;

                if (!await Inventories.OpenInventory())
                {
                    ErrorManager.ReportError();
                    return true;
                }
                if (!await MoveItem(itemForSort.Item1, itemForSort.Item2))
                {
                    ErrorManager.ReportError();
                    return true;
                }
            }
            return false;
        }

        private static Tuple<Vector2i, Vector2i> GetFirstItemForSort()
        {
            foreach (var item in Inventories.InventoryItems)
            {
                var itemName = item.Name;
                var itemPos = item.LocationTopLeft;

                if (CurrenciesToSort.TryGetValue(itemName, out Vector2i destPos))
                {
                    if (itemPos != destPos)
                    {
                        var count = Inventories.InventoryItems.Count(i => i.Name == itemName);
                        if (count != 1)
                        {
                            GlobalLog.Error("[SortInventoryTask] There are {0} stacks of \"{0}\" in inventory. Cannot sort that.");
                            continue;
                        }
                        return Tuple.Create(itemPos, destPos);
                    }
                }
                else
                {
                    destPos = GetMovePosForAnyItem(item);
                    if (Position.Comparer.Instance.Compare(destPos, itemPos) < 0)
                        return Tuple.Create(itemPos, destPos);
                }
            }
            return null;
        }

        private static async Task<bool> HandleCursorItem(Item item)
        {
            if (!CurrenciesToSort.TryGetValue(item.Name, out Vector2i destPos))
                destPos = GetMovePosForAnyItem(item);

            return await LokiPoe.InGameState.InventoryUi.InventoryControl_Main.PlaceItemFromCursor(destPos);
        }

        private static async Task<bool> MoveItem(Vector2i from, Vector2i to)
        {
            if (!await LokiPoe.InGameState.InventoryUi.InventoryControl_Main.PickItemToCursor(from))
                return false;

            if (!await LokiPoe.InGameState.InventoryUi.InventoryControl_Main.PlaceItemFromCursor(to))
                return false;

            return true;
        }

        private static Vector2i GetMovePosForAnyItem(Item item)
        {
            if (!LokiPoe.InGameState.InventoryUi.InventoryControl_Main.Inventory.CanFitItem(item.Size, out int x, out int y))
            {
                GlobalLog.Error("[GetMovePosForAnyItem] Unexpected error. Cannot fit item anywhere in main inventory.");
                ErrorManager.ReportCriticalError();
                return Vector2i.Zero;
            }
            return new Vector2i(x, y);
        }

        #region Unused interface methods

        public MessageResult Message(Message message)
        {
            return MessageResult.Unprocessed;
        }

        public async Task<LogicResult> Logic(Logic logic)
        {
            return LogicResult.Unprovided;
        }

        public void Tick()
        {
        }

        public void Start()
        {
        }

        public void Stop()
        {
        }

        public string Name => "SortInventoryTask";
        public string Author => "ExVault";
        public string Description => "Task for organizing items in inventory";
        public string Version => "1.0";

        #endregion
    }
}