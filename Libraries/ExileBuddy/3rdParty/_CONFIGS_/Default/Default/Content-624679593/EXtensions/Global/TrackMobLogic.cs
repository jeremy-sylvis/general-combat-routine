﻿using System.Linq;
using System.Threading.Tasks;
using Buddy.Coroutines;
using Default.EXtensions.CachedObjects;
using Loki.Bot;
using Loki.Game.Objects;

namespace Default.EXtensions.Global
{
    public static class TrackMobLogic
    {
        private const int MaxKillAttempts = 20;
        private static readonly Interval LogInterval = new Interval(1000);

        public static CachedObject CurrentTarget;

        static TrackMobLogic()
        {
            Events.AreaChanged += (sender, args) => CurrentTarget = null;
        }

        public static async Task<bool> Execute(int range = -1)
        {
            var cachedMonsters = CombatAreaCache.Current.Monsters;

            if (CurrentTarget == null)
            {
                CurrentTarget = range == -1
                    ? cachedMonsters.ClosestValid()
                    : cachedMonsters.Where(m => m.Position.Distance <= range).ClosestValid();

                if (CurrentTarget == null) return false;
            }

            var pos = CurrentTarget.Position;
            if (pos.IsFar)
            {
                if (LogInterval.Elapsed)
                {
                    GlobalLog.Debug($"[TrackMobTask] Cached monster locations: {cachedMonsters.Valid().Count()}");
                    GlobalLog.Debug($"[TrackMobTask] Moving to {pos}");
                }
                if (!PlayerMover.MoveTowards(pos))
                {
                    GlobalLog.Error($"[TrackMobTask] Fail to move to {pos}. Marking this monster as unwalkable.");
                    CurrentTarget.Unwalkable = true;
                    CurrentTarget = null;
                }
                return true;
            }

            var monsterObj = CurrentTarget.Object as Monster;
            if (monsterObj == null || monsterObj.IsDead)
            {
                cachedMonsters.Remove(CurrentTarget);
                CurrentTarget = null;
            }
            else
            {
                var attempts = ++CurrentTarget.InteractionAttempts;
                if (attempts > MaxKillAttempts)
                {
                    GlobalLog.Error("[TrackMobTask] All attempts to kill current monster have been spent. Now ignoring it.");
                    CurrentTarget.Ignored = true;
                    CurrentTarget = null;
                    return true;
                }
                GlobalLog.Debug($"[TrackMobTask] Alive monster is nearby, this is our {attempts}/{MaxKillAttempts} attempt to kill it.");
                await Coroutine.Sleep(200);
            }
            return true;
        }
    }
}