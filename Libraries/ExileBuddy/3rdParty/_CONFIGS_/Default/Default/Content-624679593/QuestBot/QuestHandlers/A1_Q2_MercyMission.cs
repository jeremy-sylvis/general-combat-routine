﻿using System.Threading.Tasks;
using Default.EXtensions;
using Default.EXtensions.Global;
using Default.EXtensions.Positions;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace Default.QuestBot.QuestHandlers
{
    public static class A1_Q2_MercyMission
    {
        private static readonly TgtPosition MedicineChestTgt = new TgtPosition("Medicine Chest location", "kyrenia_boat_medicinequest_v01_01_c3r2.tgt");

        private static Monster Hailrake => LokiPoe.ObjectManager.GetObjects(LokiPoe.ObjectManager.PoeObjectEnum.Hailrake)
            .FirstOrDefault<Monster>(m => m.Rarity == Rarity.Unique);

        private static WalkablePosition CachedHailrakePos
        {
            get => CombatAreaCache.Current.Storage["HailrakePosition"] as WalkablePosition;
            set => CombatAreaCache.Current.Storage["HailrakePosition"] = value;
        }

        public static void Tick()
        {
            if (!World.Act1.TidalIsland.IsCurrentArea)
                return;

            var hailrake = Hailrake;
            if (hailrake != null)
            {
                CachedHailrakePos = hailrake.WalkablePosition();
            }
        }

        public static async Task<bool> KillHailrake()
        {
            if (Helpers.PlayerHasQuestItem(QuestItemMetadata.MedicineChest))
                return false;

            if (World.Act1.TidalIsland.IsCurrentArea)
            {
                var hailrakePos = CachedHailrakePos;
                if (hailrakePos != null)
                {
                    await Helpers.MoveAndWait(hailrakePos);
                }
                else
                {
                    MedicineChestTgt.Come();
                }
                return true;
            }
            await Travel.To(World.Act1.TidalIsland);
            return true;
        }

        public static async Task<bool> TakeReward()
        {
            return await Helpers.TakeQuestReward(
                World.Act1.LioneyeWatch,
                TownNpcs.Nessa,
                "Medicine Chest Reward",
                Quests.MercyMission.Id);
        }
    }
}