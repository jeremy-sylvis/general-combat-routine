﻿using Loki;
using Loki.Common;

namespace Legacy.ItemFilterEditor
{
	/// <summary>Settings for the ItemFilterEditor plugin. </summary>
	public class ItemFilterEditorSettings : JsonSettings
	{
		private static ItemFilterEditorSettings _instance;

		/// <summary>The current instance for this class. </summary>
		public static ItemFilterEditorSettings Instance => _instance ?? (_instance = new ItemFilterEditorSettings());

		/// <summary>The default ctor. Will use the settings path "ItemFilterEditor".</summary>
		public ItemFilterEditorSettings()
			: base(GetSettingsFilePath(Configuration.Instance.Name, string.Format("{0}.json", "ItemFilterEditor")))
		{
		}
	}
}