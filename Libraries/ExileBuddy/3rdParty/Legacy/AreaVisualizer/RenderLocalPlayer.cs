﻿using System.Windows.Controls;
using System.Windows.Media.Media3D;
using HelixToolkit.Wpf;

namespace Legacy.AreaVisualizer
{
	class RenderLocalPlayer : RenderGroup
	{
		private TextBlock _playerLocationText;
		private AreaVisualizerData _curData;
		private GeometryModel3D _model;
		private bool _lockCamera;

		public RenderLocalPlayer(HelixViewport3D viewport, TextBlock playerLocationText) : base(viewport)
		{
			MeshBuilder mb = new MeshBuilder();
			mb.AddSphere(new Point3D(), 1.5);

			_model = new GeometryModel3D(mb.ToMesh(true), Materials.Red);
			Visual = new MeshVisual3D
			{
				Content = _model
			};

			_playerLocationText = playerLocationText;
		}

		public bool LockCamera
		{
			get
			{
				return _lockCamera;
			}
			set
			{
				if (_lockCamera != value)
				{
					if (!value)
					{
						View.Camera.Reset();
						View.ZoomExtents();
					}
					_lockCamera = value;
				}
			}
		}

		#region Overrides of RenderGroup

		public override void Render(AreaVisualizerData data)
		{
			_curData = data;

			var mpos = _curData.MyPos;
			_model.Transform = new TranslateTransform3D(new Vector3D(mpos.X, mpos.Y, 0));

			// While we're here, lets lock the camera. :)
			if (LockCamera)
			{
				var cam = View.Camera as PerspectiveCamera;
				cam.Position = new Point3D(mpos.X - 300, mpos.Y - 300, 300);
				var target = new Point3D(mpos.X, mpos.Y, 0);
				Vector3D dir = target - cam.Position;
				dir.Normalize();
				cam.LookDirection = dir;
			}

			_playerLocationText.Dispatcher.BeginInvoke(new System.Action(() =>
			{
				_playerLocationText.Text = "Player Location: " + mpos.X + ", " + mpos.Y;
			}));
		}

		#endregion
	}
}