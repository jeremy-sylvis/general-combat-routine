﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Controls;
using log4net;
using Loki.Bot;
using Loki.Common;
using Loki.Game;
using Loki.Game.GameData;
using Loki.Game.Objects;

namespace Legacy.AutoFlask
{
	internal class AutoFlask : IPlugin, ITickEvents
	{
		private static readonly ILog Log = Logger.GetLoggerInstanceForType();

		private Gui _instance;

		private readonly Stopwatch _lifeFlaskCd = new Stopwatch();
		private readonly Stopwatch _manaFlaskCd = new Stopwatch();
		private readonly Stopwatch _speedFlaskCd = new Stopwatch();

		#region Implementation of IAuthored

		/// <summary> The name of the plugin. </summary>
		public string Name => "AutoFlask";

		/// <summary>The author of the plugin.</summary>
		public string Author => "Bossland GmbH";

		/// <summary> The description of the plugin. </summary>
		public string Description => "A plugin that provides basic auto-flask use.";

		/// <summary>The version of the plugin.</summary>
		public string Version => "0.0.1.1";

		#endregion

		#region Implementation of IBase

		/// <summary>Initializes this plugin.</summary>
		public void Initialize()
		{
		}

		/// <summary>Deinitializes this object. This is called when the object is being unloaded from the bot.</summary>
		public void Deinitialize()
		{
		}

		#endregion

		#region Implementation of ITickEvents

		/// <summary> The plugin tick callback. Do any update logic here. </summary>
		public void Tick()
		{
			if (!LokiPoe.IsInGame || LokiPoe.Me.IsInTown || LokiPoe.Me.IsDead)
				return;

			// Life
			if (LokiPoe.Me.HealthPercent < AutoFlaskSettings.Instance.HpFlaskPercentTrigger &&
				!LokiPoe.Me.IsUsingHealthFlask)
			{
				if (FlaskHelper(_lifeFlaskCd, AutoFlaskSettings.Instance.HpFlaskCooldownMs, LifeFlasks))
				{
					return;
				}
			}

			// Mana
			if (LokiPoe.Me.ManaPercent < AutoFlaskSettings.Instance.MpFlaskPercentTrigger &&
				!LokiPoe.Me.IsUsingManaFlask)
			{
				if (FlaskHelper(_manaFlaskCd, AutoFlaskSettings.Instance.MpFlaskCooldownMs, ManaFlasks))
				{
					return;
				}
			}

			// Quicksilver
			if (AutoFlaskSettings.Instance.UseQuickSilver && !LokiPoe.Me.IsUsingQuicksilverFlask)
			{
				if (LokiPoe.ObjectManager.Objects.OfType<Monster>()
						.Where(m => m.DistanceSqr < 50 * 50)
						.Count(m => m.IsAliveHostile) == 0)
				{
					var la = LokiPoe.InstanceInfo.LastAction;
					if (la != null && la.InternalId.Equals("Move"))
					{
						var flasks = QuicksilverFlasks.ToList();
						//Log.Info("QuicksilverFlasks: " + flasks.Count);
						if (FlaskHelper(_speedFlaskCd, AutoFlaskSettings.Instance.QuickSilverFlaskCooldownMs, flasks))
						{
							// ReSharper disable once RedundantJumpStatement
							return;
						}
						else
						{
							//Log.Info("!FlaskHelper");
						}
					}
				}
				else
				{
					//Log.Info("Mobs");
				}
			}
			else
			{
				//Log.Info("IsUsingQuicksilverFlask");
			}
		}

		#endregion

		#region Implementation of IConfigurable

		/// <summary>The settings object. This will be registered in the current configuration.</summary>
		public JsonSettings Settings => AutoFlaskSettings.Instance;

		/// <summary> The plugin's settings control. This will be added to the Exilebuddy Settings tab.</summary>
		public UserControl Control => (_instance ?? (_instance = new Gui()));

		#endregion

		#region Implementation of ILogicHandler

		/// <summary>
		/// Implements the ability to handle a logic passed through the system.
		/// </summary>
		/// <param name="logic">The logic to be processed.</param>
		/// <returns>A LogicResult that describes the result..</returns>
		public async Task<LogicResult> Logic(Logic logic)
		{
			return LogicResult.Unprovided;
		}

		#endregion

		#region Implementation of IMessageHandler

		/// <summary>
		/// Implements logic to handle a message passed through the system.
		/// </summary>
		/// <param name="message">The message to be processed.</param>
		/// <returns>A tuple of a MessageResult and object.</returns>
		public MessageResult Message(Message message)
		{
			return MessageResult.Unprocessed;
		}

		#endregion

		#region Implementation of IEnableable

		/// <summary> The plugin is being enabled.</summary>
		public void Enable()
		{
		}

		/// <summary> The plugin is being disabled.</summary>
		public void Disable()
		{
		}

		#endregion

		#region Override of Object

		/// <summary>Returns a string that represents the current object.</summary>
		/// <returns>A string that represents the current object.</returns>
		public override string ToString()
		{
			return Name + ": " + Description;
		}

		#endregion

		private bool FlaskHelper(Stopwatch sw, int flaskCdMs, IEnumerable<Item> flasks)
		{
			var useFlask = false;
			if (!sw.IsRunning)
			{
				sw.Start();
				useFlask = true;
			}
			else if (sw.ElapsedMilliseconds > flaskCdMs && sw.ElapsedMilliseconds > LatencyTracker.Average)
			{
				sw.Restart();
				useFlask = true;
			}

			if (useFlask)
			{
				var flask = flasks.FirstOrDefault();
				if (flask != null)
				{
					var err = LokiPoe.InGameState.QuickFlaskHud.UseFlaskInSlot(flask.LocationTopLeft.X + 1);
					if (!err)
					{
						Log.ErrorFormat("[FlaskHelper] QuickFlaskHud.UseFlaskInSlot returned {0}.", err);
					}
					return true;
				}
			}

			return false;
		}

		/// <summary>All life restoring flasks that are non-unique.</summary>
		public static IEnumerable<Item> LifeFlasks
		{
			get
			{
				var items = LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Flasks);
				return from item in items
					where item != null && item.Rarity != Rarity.Unique && item.HealthRecover > 0 && item.CanUse
					orderby item.IsInstantRecovery ? item.HealthRecover : item.HealthRecoveredPerSecond descending
					select item;
			}
		}

		/// <summary>All mana restoring flasks that are non-unique.</summary>
		public static IEnumerable<Item> ManaFlasks
		{
			get
			{
				var items = LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Flasks);
				return from item in items
					where item != null && item.Rarity != Rarity.Unique && item.ManaRecover > 0 && item.CanUse
					orderby item.IsInstantRecovery ? item.ManaRecover : item.ManaRecoveredPerSecond descending
					select item;
			}
		}

		/// <summary>All quicksilver flasks that are non-unique.</summary>
		public static IEnumerable<Item> QuicksilverFlasks
		{
			get
			{
				var items = LokiPoe.InstanceInfo.GetPlayerInventoryItemsBySlot(InventorySlot.Flasks);
				foreach (var item in items)
				{
					if(item.Rarity == Rarity.Unique)
						continue;

					if(!item.HasMetadataFlags(MetadataFlags.FlaskUtility6))
						continue;

					if(!item.CanUse)
						continue;

					yield return item;
				}
			}
		}
	}
}